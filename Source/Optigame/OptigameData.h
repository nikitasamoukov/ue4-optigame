// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataAsset.h"
#include "Galaxy/BreedRegistry/LBreedRegistry.h"
#include "Materials/MaterialInterface.h"
#include "Optigame/OptigameParameters.h"
#include "OptigameData.generated.h"

class UTexture2D;

USTRUCT(BlueprintType)
struct FEmpireIcons
{
	GENERATED_BODY()

	void Init();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	TArray<UTexture2D*> Icons;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Runtime")
	TMap<FName, UTexture2D*> IconsMap;
};

UCLASS()
class OPTIGAME_API UOptigameDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	void Init();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	TMap<FName, UMaterialInterface*> PlanetMaterialsMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	TMap<FName, UMaterialInterface*> RingMaterialsMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	TMap<FName, UStaticMesh*> MeshesMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	TArray<UObjectsDataAsset*> BuildingsData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	TArray<UObjectsDataAsset*> ResourcesData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	TArray<UObjectsDataAsset*> EmpireSpawnData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	TArray<UObjectsDataAsset*> ShipModulesData;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	FOptigameVars OptigameVars;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	FEmpireIcons EmpireIcons;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	TMap<FName, UTexture2D*> TexturesMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Static")
	TMap<FName, FString> StringsMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "OG Runtime")
	bool IsLoaded = false;

	UFUNCTION(BlueprintCallable)
	FText GetFTextFromId(FName Id);

	UFUNCTION(BlueprintCallable)
	FString GetFStringFromId(FName Id);

	UFUNCTION(BlueprintCallable)
	FEnttHandle GetBreedFromId(FName Id);

	LBreedRegistry BreedRegistry;
};

UCLASS(config = Game, defaultconfig)
class OPTIGAME_API UOptigameProjectSettings : public UDeveloperSettings
{
	GENERATED_BODY()
public:
	UPROPERTY(Config, EditAnywhere)
	TSoftObjectPtr<UOptigameDataAsset> OptigameDataAsset;

	UFUNCTION(BlueprintCallable, BlueprintPure)
	static UOptigameDataAsset* GetOptigameData();
};