#pragma once

#include "OptigameParameters.h"

#include "Galaxy/Components/ComponentsShip.h"
FOptigameVars GOptigameVars;

void FOptigameVars::Assign(FOptigameVars const& Vars)
{
	*this = Vars;
	Ups = FMath::Max(0.1f, Ups);
	FrameTime = 1 / Ups;

	GMapWeaponSize = CalcGMapWeaponSize();
}