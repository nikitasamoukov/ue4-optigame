#pragma once

class AOptigameCore;
class FGalaxy;
class FGalaxyImGuiWindowImpl;

class FGalaxyImGuiWindow
{
public:
	FGalaxyImGuiWindow();
	~FGalaxyImGuiWindow();

	void Draw(AOptigameCore* OptigameCore);

protected:
	TUniquePtr<FGalaxyImGuiWindowImpl> Impl;
};