#pragma once

class LRegistryImGuiWindowImpl;

struct FImGuiComponentDrawContext
{
	LRegistryImGuiWindowImpl* RegWindow;
	FRegistry const*		  Reg;
	FEntity					  Entity;
	int						  StackLevel;

	void ChildDraw(FEntity E, FString const& Name) const;
};

class FRegistryImGuiWindow
{
public:
	FRegistryImGuiWindow();
	~FRegistryImGuiWindow();

	void DrawRegistry(FRegistry const& Reg);
	void DrawEntity(FRegistry const& Reg, FEntity Entity) { DrawEntity({ Impl.Get(), &Reg, Entity, 0 }); }
	void DrawEntity(FImGuiComponentDrawContext const& Ctx);

protected:
	TUniquePtr<LRegistryImGuiWindowImpl> Impl;
};