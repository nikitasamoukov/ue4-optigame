#include "FComponentsDrawFunctions.h"
#include <string>

#include "FRegistryImGuiWindow.h"
#include "imgui.h"
#include "Optigame/Common/common.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsBuilding.h"
#include "Optigame/Galaxy/Components/ComponentsBuildQueue.h"
#include "Optigame/Galaxy/Components/ComponentsEmpire.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"
#include "Optigame/Galaxy/Components/ComponentsPr.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"

using std::function;
using std::string;
using std::to_string;
using std::unordered_map;
using std::vector;

FString ToFString(FCompPlanet::EType Val)
{
	switch (Val)
	{
		case FCompPlanet::EType::Desert:
			return "Desert";
		case FCompPlanet::EType::Earth:
			return "Earth";
		case FCompPlanet::EType::Ice:
			return "Ice";
		case FCompPlanet::EType::Lava:
			return "Lava";
		case FCompPlanet::EType::Rock:
			return "Rock";
		case FCompPlanet::EType::Water:
			return "Water";
		default:
			return "ERR";
	}
}

FString ToFString(int Val)
{
	return FString::FromInt(Val);
}

FString ToFString(double Val)
{
	return FString::SanitizeFloat(Val);
}

FString ToFString(FColor Val)
{
	return Val.ToHex();
}

FString ToFString(FName Val)
{
	return Val.ToString();
}

FString ToFString(FHitStat Val)
{
	return ToFString(Val.Val) + "\\" + ToFString(Val.Max) + "(" + ToFString(Val.Regen) + ")";
}

#define IMGUI_DRAW_C_FIELD(field) ImGui::Text(#field ": %ls", *ToFString(Component.field));

void ImGuiCDrawNoReflectionAndDraw(FImGuiComponentDrawContext const& Ctx, entt::type_info Ti)
{
	ImGui::Text("Component no reflection and draw: %s", std::string(Ti.name()).c_str());
}

void ImGuiDraw(FShipTemplateModule const& Val)
{
	ImGui::Text("TemplateId: %ls", *ToFString(Val.BreedId));
	ImGui::Text("Size: %ls", *ToFString(Val.Size));
	ImGui::Text("Tier: %ls", *ToFString(Val.Tier));
}

void ImGuiCDrawPropImpl(FImGuiComponentDrawContext const& Ctx, FProperty* PropBase, void const* PropPtr, int Depth, TOptional<FString> VarName = {})
{
	if (Depth > 10)
		return;

	FString Spaces;
	for (int i = 0; i < Depth; i++)
		Spaces += "  ";
	FString SimpleVarName = "##4EC9B0FF" + PropBase->GetCPPType() + " ##9CDCFEFF" + PropBase->GetNameCPP() + "##FFFFFFFF";
	if (VarName)
		SimpleVarName = *VarName;
	FString VarVal;
	bool	IsSimpleVal = false;

	// Values simple
	if (auto Prop = CastField<FFloatProperty>(PropBase))
	{
		double Val = *(float const*)PropPtr;
		VarVal = FString::SanitizeFloat(Val);
		IsSimpleVal = true;
	}
	if (auto Prop = CastField<FDoubleProperty>(PropBase))
	{
		double Val = *(double const*)PropPtr;
		VarVal = FString::SanitizeFloat(Val);
		IsSimpleVal = true;
	}
	if (auto Prop = CastField<FIntProperty>(PropBase))
	{
		int Val = *(int const*)PropPtr;
		VarVal = FString::FromInt(Val);
		IsSimpleVal = true;
	}
	if (auto Prop = CastField<FNameProperty>(PropBase))
	{
		FName Val = *(FName const*)PropPtr;
		VarVal = Val.ToString();
		IsSimpleVal = true;
	}
	if (PropBase->GetCPPType() == "FVector")
	{
		FVector Val = *(FVector const*)PropPtr;
		VarVal = FString::Printf(TEXT("(%3.3f, %3.3f, %3.3f)"), Val.X, Val.Y, Val.Z);
		IsSimpleVal = true;
	}

	if (IsSimpleVal)
	{
		ImGui::Text("%ls%ls:%ls", *Spaces, *SimpleVarName, *VarVal);
		return;
	}

	// Values special
	if (PropBase->GetCPPType() == "FEntity")
	{
		FString TypeName = "##4EC9B0FF" + PropBase->GetCPPType() + "##FFFFFFFF";
		FString VariableName = "##9CDCFEFF" + PropBase->GetNameCPP() + "##FFFFFFFF";

		auto E = *(FEntity const*)PropPtr;

		if (E == entt::null)
		{
			ImGui::Text("%ls%ls", *Spaces, *(TypeName + " " + VariableName + ": null"));
			return;
		}
		Ctx.ChildDraw(E, "Entity: " + FString::FromInt((int)E));
		return;
	}

	if (auto Prop = CastField<FStructProperty>(PropBase))
	{
		UScriptStruct* NRef = Prop->Struct;
		ImGui::Text("%ls%ls:", *Spaces, *SimpleVarName);
		ImGuiCDrawRefImpl(Ctx, NRef, PropPtr, Depth + 1);
		return;
	}

	if (auto Prop = CastField<FArrayProperty>(PropBase))
	{
		FProperty* TemplateProperty = Prop->Inner;

		FString TypeName = "##4EC9B0FF" + PropBase->GetCPPType() + "##FFFFFFFF";
		FString TypeTemplateName = "##4EC9B0FF" + Prop->Inner->GetCPPType() + "##FFFFFFFF";
		FString VariableName = "##9CDCFEFF" + PropBase->GetNameCPP() + "##FFFFFFFF";
		//" ##9CDCFEFF" + PropBase->GetNameCPP();
		ImGui::Text("%ls%ls<%ls> %ls:", *Spaces, *TypeName, *TypeTemplateName, *VariableName);

		FScriptArrayHelper ArrayHelper(Prop, PropPtr);

		int32_t		arrayValueCount = ArrayHelper.Num();
		std::string nodeName = "Elements:##" + to_string((u64)PropPtr);
		if (ImGui::TreeNode(nodeName.c_str()))
		{
			for (size_t i = 0; i < arrayValueCount; i++)
			{
				uint8* ValuePtr = ArrayHelper.GetRawPtr(i);
				ImGuiCDrawPropImpl(Ctx, TemplateProperty, ValuePtr, Depth + 1, "[" + FString::FromInt(i) + "]");
			}
			ImGui::TreePop();
		}
		return;
	}

	if (auto Prop = CastField<FMapProperty>(PropBase))
	{
		FProperty* TemplateKeyProperty = Prop->KeyProp;
		FProperty* TemplateValProperty = Prop->ValueProp;

		FString TypeName = "##4EC9B0FF" + PropBase->GetCPPType() + "##FFFFFFFF";
		FString TypeTemplateKeyName = "##4EC9B0FF" + TemplateKeyProperty->GetCPPType() + "##FFFFFFFF";
		FString TypeTemplateValueName = "##4EC9B0FF" + TemplateValProperty->GetCPPType() + "##FFFFFFFF";
		FString VariableName = "##9CDCFEFF" + PropBase->GetNameCPP() + "##FFFFFFFF";
		//" ##9CDCFEFF" + PropBase->GetNameCPP();
		ImGui::Text("%ls%ls<%ls, %ls> %ls:", *Spaces, *TypeName, *TypeTemplateKeyName, *TypeTemplateValueName, *VariableName);

		FScriptMapHelper MapHelper(Prop, PropPtr);

		int32 ItemsLeft = MapHelper.Num();
		for (int32 Index = 0; ItemsLeft > 0; ++Index)
		{
			if (MapHelper.IsValidIndex(Index))
			{
				--ItemsLeft;

				uint8* Data = MapHelper.GetPairPtr(Index);

				ImGuiCDrawPropImpl(Ctx, TemplateKeyProperty, TemplateKeyProperty->ContainerPtrToValuePtr<uint8>(Data), Depth + 1);
				ImGuiCDrawPropImpl(Ctx, TemplateValProperty, TemplateValProperty->ContainerPtrToValuePtr<uint8>(Data), Depth + 1);
			}
			else
			{
				LOG();
			}
		}
	}
}

void ImGuiCDrawRefImpl(FImGuiComponentDrawContext const& Ctx, UScriptStruct* Ref, void const* Component, int Depth)
{
	if (Depth > 10)
		return;

	for (TFieldIterator<FProperty> It(Ref); It; ++It)
	{
		FString Spaces;
		for (int i = 0; i < Depth; i++)
			Spaces += "  ";
		FString VarName = It->GetCPPType() + " " + It->GetNameCPP();
		FString VarVal;

		void const* NStr = It->ContainerPtrToValuePtr<void>(Component);
		ImGuiCDrawPropImpl(Ctx, *It, NStr, Depth);
	}
}

/*
void ImGuiCDraw(FImGuiComponentDrawContext const& Ctx, FCompColony const& Component)
{
	ImGui::Text("%ls", ToCStr("Population:" + ToFString(Component.Population)));
	ImGui::Text("%ls", ToCStr("PopulationProgress:" + ToFString(Component.PopulationProgress)));
	ImGui::Text("%ls", ToCStr("PopulationProgressIncome:" + ToFString(Component.PopulationProgressIncome)));
	ImGui::Text("%ls", ToCStr("House:" + ToFString(Component.House)));
	ImGui::Text("%ls", ToCStr("WorkerPlaces:" + ToFString(Component.WorkerPlaces)));
	if (ImGui::TreeNode("Buildings:"))
	{
		for (auto& Connect : Component.Buildings)
		{
			if (Ctx.Reg->valid(Connect))
			{
				Ctx.ChildDraw(Connect, "Building ");
			}
			else
			{
				ImGui::Text("<Empty link>");
			}
		}
		ImGui::TreePop();
	}
	if (ImGui::TreeNode("Res prod:"))
	{
		for (auto Pair : Component.ResourcesProduce)
			ImGui::Text("%ls : %f", *Pair.Key.ToString(), (double)Pair.Value);
		ImGui::TreePop();
	}
	if (ImGui::TreeNode("Res cons:"))
	{
		for (auto Pair : Component.ResourcesConsume)
			ImGui::Text("%ls : %f", *Pair.Key.ToString(), (double)Pair.Value);
		ImGui::TreePop();
	}
}

void ImGuiCDraw(FImGuiComponentDrawContext const& Ctx, FCompEmpire const& Component)
{
	ImGui::Text("Name: %ls", *Component.Name);
	ImGui::Text("Icon.Texture: %ls", *Component.Icon.Texture.ToString());
	ImGui::Text("Icon.ColorMain: %ls", *ToFString(Component.Icon.ColorMain));
	ImGui::Text("Icon.ColorSecond: %ls", *ToFString(Component.Icon.ColorSecond));
	if (ImGui::TreeNode("Res:"))
	{
		ImGui::Text("Res:");
		for (auto& E : Component.Resources)
		{
			ImGui::Text("%ls", *("Count:" + FString::Sanitizedouble(E.Value) + " ResId:" + E.Key.ToString()));
		}
		ImGui::Text("ResCh:");
		for (auto& E : Component.ResourcesChange)
		{
			ImGui::Text("%ls", *("Count:" + FString::Sanitizedouble(E.Value) + " ResId:" + E.Key.ToString()));
		}
		ImGui::TreePop();
	}
	if (ImGui::TreeNode("Ships templates:"))
	{
		for (auto& E : Component.ShipTemplates)
		{
			Ctx.ChildDraw(E, "Ship:");
		}
		Ctx.ChildDraw(Component.ShipTemplateEditing, "ShipEdit:");
		ImGui::TreePop();
	}
}

void ImGuiCDraw(FImGuiComponentDrawContext const& Ctx, FCompBuildQueue const& Component)
{
	TVariant<FBuildResultBuilding, FBuildResultShip, FBuildResultStationModule, FBuildResultUnique> Val;

	if (Component.Queue.Num() == 0)
	{
		ImGui::Text("<no orders>");
	}

	for (auto& E : Component.Queue)
	{
		if (E.BuildResult.Val.TryGet<FBuildResultBuilding>())
		{
			ImGui::Text("FBuildResultBuilding");
		}
		if (E.BuildResult.Val.TryGet<FBuildResultShip>())
		{
			ImGui::Text("FBuildResultShip");
		}
		if (E.BuildResult.Val.TryGet<FBuildResultStationModule>())
		{
			ImGui::Text("FBuildResultStationModule");
		}
		if (E.BuildResult.Val.TryGet<FBuildResultUnique>())
		{
			ImGui::Text("FBuildResultUnique");
		}
	}
}
*/

TMap<entt::id_type, double> GenPriMap()
{
	TArray<entt::type_info> Arr = {
		{ entt::type_id<FCompPrStar>() },
		{ entt::type_id<FCompPrSun>() },
		{ entt::type_id<FCompPrPlanet>() },
		{ entt::type_id<FCompBreedId>() },
		{ entt::type_id<FCompTransform>() },
		{ entt::type_id<FCompCurrentTimePos>() },
		{ entt::type_id<FCompOwner>() },
		{ entt::type_id<FCompStar>() },
		{ entt::type_id<FCompPlanet>() },
		{ entt::type_id<FCompColony>() },
		{ entt::type_id<FCompBuilding>() },
		{ entt::type_id<FCompBuildingType>() },
		{ entt::type_id<FCompBuildingHouse>() },
		{ entt::type_id<FCompEmpire>() },
		{ entt::type_id<FCompShipTemplate>() },
		{ entt::type_id<FCompSelectTag>() },
		{ entt::type_id<FCompShip>() },
		{ entt::type_id<FCompSpaceObject>() },
		{ entt::type_id<FCompResourceType>() },
		{ entt::type_id<FCompEmpireSpawnInfo>() },
		{ entt::type_id<FCompShipModule>() },
		{ entt::type_id<FCompShipModuleHull>() },
		{ entt::type_id<FCompShipModuleShield>() },
		{ entt::type_id<FCompShipModuleArmor>() },
		{ entt::type_id<FCompShipModuleWeapon>() },
		{ entt::type_id<FCompBuildQueue>() },
		{ entt::type_id<FCompMesh>() },
	};

	TMap<entt::id_type, double> Map;

	double Pri = 1;
	for (auto Ti : Arr)
	{
		Map.Add(Ti.hash(), Pri);
		Pri++;
	}
	return Map;
}

double ImGuiCDrawGetPri(entt::type_info Ti)
{
	static TMap<entt::id_type, double> PriMap = GenPriMap();

	if (PriMap.Contains(Ti.hash()))
	{
		return PriMap[Ti.hash()];
	}
	return 10000 + Ti.hash() * 0.1;
}