#pragma once
#include "FRegistryImGuiWindow.h"

void ImGuiCDrawNoReflectionAndDraw(FImGuiComponentDrawContext const& Ctx, entt::type_info Ti);

template <typename T>
concept CComponentHaveStaticStruct = requires(T) {
	{
		T::StaticStruct()
	} -> std::same_as<UScriptStruct*>;
};

template <typename C>
concept CCompIsRef = CComponentHaveStaticStruct<C>;

template <typename C>
void ImGuiCDrawRef(FImGuiComponentDrawContext const& Ctx, C const& Component)
{
	auto Ref = C::StaticStruct();
	ImGuiCDrawRefImpl(Ctx, Ref, (void*)&Component);
}

void ImGuiCDrawRefImpl(FImGuiComponentDrawContext const& Ctx, UScriptStruct* Ref, void const* Component, int Depth = 0);

// void ImGuiCDraw(FImGuiComponentDrawContext const& Ctx, FCompColony const& Component);
// void ImGuiCDraw(FImGuiComponentDrawContext const& Ctx, FCompEmpire const& Component);
// void ImGuiCDraw(FImGuiComponentDrawContext const& Ctx, FCompBuildQueue const& Component);

double ImGuiCDrawGetPri(entt::type_info Ti);

template <typename C>
double ImGuiCDrawGetPri()
{
	return ImGuiCDrawGetPri(entt::type_id<C>());
}