// Fill out your copyright notice in the Description page of Project Settings.

#include "FRegistryImGuiWindow.h"
#include "imgui.h"
#include "Optigame/OptigameCore.h"
#include "Optigame/OptigameData.h"

class LImGuiWindowTemplatesExtra
{
public:
	FEntity					Selected = entt::null;
	TStaticArray<char, 256> Filter{};

	void DrawReg(LBreedRegistry const& TemplatesReg)
	{

		// Left
		{
			ImGui::BeginChild("left pane", ImVec2(300, 0), true);
			ImGui::InputText("##FilterTemplates", Filter.GetData(), Filter.Num());
			auto& NamesMap = TemplatesReg._GetNamesMap();

			for (auto& Pair : NamesMap)
				if (Pair.Key.ToString().Contains(Filter.GetData()) || Filter[0] == '\0')
					if (ImGui::Selectable(TCHAR_TO_UTF8(*Pair.Key.ToString()), Selected == Pair.Value))
						Selected = Pair.Value;

			ImGui::EndChild();
		}
		ImGui::SameLine();

		// Right
		{
			ImGui::BeginGroup();
			ImGui::BeginChild("item view", ImVec2(0, 0)); // Leave room for 1 line below us

			auto& Reg = TemplatesReg.GetReg();

			if (Reg.valid(Selected))
			{
				static FRegistryImGuiWindow Drawer;
				Drawer.DrawEntity(Reg, Selected);
			}
			else
			{
				ImGui::Text("Incorrect entity");
			}

			ImGui::EndChild();
			ImGui::EndGroup();
		}
	}
};

class LImGuiDrawerMapKeys
{
public:
	TStaticArray<char, 256> Filter{};

	template <typename TVal>
	void Draw(TMap<FName, TVal> Map)
	{
		ImGui::InputText("##Filter", Filter.GetData(), Filter.Num());

		for (auto& Pair : Map)
			if (Pair.Key.ToString().Contains(Filter.GetData()) || Filter[0] == '\0')
			{
				ImGui::Text(TCHAR_TO_UTF8(*Pair.Key.ToString()));
			}
	}
};

class LImGuiDrawerStringsMap
{
public:
	TStaticArray<char, 256> Filter{};

	void Draw(TMap<FName, FString> Map)
	{
		ImGui::InputText("##Filter", Filter.GetData(), Filter.Num());

		for (auto& Pair : Map)
			if (Pair.Key.ToString().Contains(Filter.GetData()) || Filter[0] == '\0')
			{
				ImGui::Text("%s : %s", TCHAR_TO_UTF8(*Pair.Key.ToString()), TCHAR_TO_UTF8(*Pair.Value));
			}
	}
};

void ImGuiWindowGameData(AOptigameCore* OptigameCore)
{
	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto& TemplatesReg = OptigameData->BreedRegistry;

	ImGui::Begin("Game data");
	if (ImGui::BeginTabBar("##Tabs", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("Templates"))
		{
			static LImGuiWindowTemplatesExtra WindowTemplates;
			WindowTemplates.DrawReg(TemplatesReg);
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Textures"))
		{
			static LImGuiDrawerMapKeys Drawer;
			Drawer.Draw(OptigameData->TexturesMap);
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Strings"))
		{
			static LImGuiDrawerStringsMap Drawer;
			Drawer.Draw(OptigameData->StringsMap);
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("T Raw"))
		{
			static FRegistryImGuiWindow Drawer;
			Drawer.DrawRegistry(TemplatesReg.GetReg());
			ImGui::EndTabItem();
		}
		ImGui::EndTabBar();
	}

	ImGui::End();
}