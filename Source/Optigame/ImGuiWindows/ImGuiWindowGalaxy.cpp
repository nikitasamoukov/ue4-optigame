// Fill out your copyright notice in the Description page of Project Settings.

#include "FGalaxyImGuiWindow.h"
#include "FRegistryImGuiWindow.h"
#include "imgui.h"
#include "Optigame/OptigameCore.h"

void ImGuiWindowGalaxy(AOptigameCore* OptigameCore)
{
	ImGui::Begin("Galaxy Registry");
	if (ImGui::BeginTabBar("##Tabs", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("Raw"))
		{
			static FRegistryImGuiWindow Drawer;
			Drawer.DrawRegistry(OptigameCore->Galaxy->Reg);
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Galaxy view"))
		{
			static FGalaxyImGuiWindow Drawer;
			Drawer.Draw(OptigameCore);
			ImGui::EndTabItem();
		}
		ImGui::EndTabBar();
	}
	ImGui::End();
}