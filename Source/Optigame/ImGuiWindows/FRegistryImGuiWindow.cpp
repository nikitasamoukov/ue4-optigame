#include "FRegistryImGuiWindow.h"
#include <string>
#include "imgui.h"
#include "Optigame/Common/common.h"
#include "Optigame/Galaxy/Components/FFactoryComponents.h"

using std::function;
using std::string;
using std::to_string;
using std::unordered_map;
using std::vector;

namespace std
{
	template <>
	struct hash<entt::type_info>
	{
		std::size_t operator()(entt::type_info const& s) const noexcept
		{
			return s.hash();
		}
	};
} // namespace std

class LRegistryImGuiWindowImpl
{
public:
	LRegistryImGuiWindowImpl();

	void		DrawReg(FRegistry const& reg);
	static void DrawEntity(FImGuiComponentDrawContext const& Ctx);

	FEntity selected = entt::null;

private:
	void DrawEntityInfo(FRegistry const& reg, FEntity entity);

	FFactoryComponents		  FactoryComponents;
	TMap<entt::id_type, bool> Filters;
};

void LRegistryImGuiWindowImpl::DrawEntityInfo(FRegistry const& reg, FEntity entity)
{
	string s = to_string((u64)entity);
	ImGui::Text("MyObject: %s", s.c_str());
	ImGui::Separator();
	if (ImGui::BeginTabBar("##Tabs", ImGuiTabBarFlags_None))
	{
		if (ImGui::BeginTabItem("Raw"))
		{
			DrawEntity({ this, &reg, selected, 0 });
			ImGui::EndTabItem();
		}
		if (ImGui::BeginTabItem("Not soon"))
		{
			ImGui::Text("Unknown");
			ImGui::EndTabItem();
		}
		ImGui::EndTabBar();
	}
}

void LRegistryImGuiWindowImpl::DrawReg(FRegistry const& reg)
{
	if (ImGui::TreeNode("Filters"))
	{
		if (ImGui::BeginTable("TableFilters", 3))
		{
			for (auto& CellPair : FactoryComponents.GetAll())
			{
				ImGui::TableNextColumn();
				auto& Cell = CellPair.Value;
				if (!Filters.Contains(CellPair.Key))
					continue;

				ImGui::Checkbox(TCHAR_TO_ANSI(*Cell.Name), &Filters[CellPair.Key]);
			}
			ImGui::EndTable();
		}
		ImGui::TreePop();
	}

	TArray<FFactoryComponentsCCell::FFuncHaveComp> FilterFunctions;
	for (auto& FiltersPair : Filters)
	{
		auto Cell = FactoryComponents.GetTypeCell(FiltersPair.Key);
		if (!Cell)
			LOG();
		if (FiltersPair.Value)
		{
			FilterFunctions.Add(Cell->FuncHaveComp);
		}
	}

	auto filter_func = [&](FEntity entity) {
		for (auto& func : FilterFunctions)
		{
			if (!func(reg, entity))
				return false;
		}
		return true;
	};

	// Left
	{
		ImGui::BeginChild("left pane", ImVec2(250, 0), true);

		reg.each([&](FEntity entity) {
			u64	   idx = static_cast<u64>(entity);
			string entity_str = to_string(idx);
			if (filter_func(entity))
				if (ImGui::Selectable(entity_str.c_str(), selected == entity))
					selected = entity;
		});

		ImGui::EndChild();
	}
	ImGui::SameLine();

	// Right
	{
		ImGui::BeginGroup();
		ImGui::BeginChild("item view", ImVec2(0, 0)); // Leave room for 1 line below us

		if (reg.valid(selected))
		{
			DrawEntityInfo(reg, selected);
		}
		else
		{
			ImGui::Text("Incorrect entity");
		}

		ImGui::EndChild();
		ImGui::EndGroup();
	}

	/*
	auto func_draw_component = [&](FEntity entity, entt::id_type id_type) {

	};

	reg.each([&](FEntity entity) {
		u64 idx = static_cast<u64>(entity);
		string entity_str = "Entity:" + to_string(idx);
		ImGui::Selectable(entity_str.c_str());
		reg.visit(entity, [&](entt::id_type id_type) {
			func_draw_component(entity, id_type);
			});
		});
		*/
}

void LRegistryImGuiWindowImpl::DrawEntity(FImGuiComponentDrawContext const& Ctx)
{
	if (!Ctx.Reg->valid(Ctx.Entity))
	{
		ImGui::Text("Invalid");
		return;
	}
	TArray<TPair<int, TFunction<void()>>> DrawFuncArray;

	auto func_draw_component = [&](entt::id_type Id) {
		if (auto Cell = Ctx.RegWindow->FactoryComponents.GetTypeCell(Id))
		{
			auto	DrawFunc = Cell->FuncImguiDraw;
			auto	DrawFuncPri = Cell->ImguiDrawPriority;
			FString CompName = Cell->Name;

			auto func = [&, DrawFunc, CompName]() {
				ImGui::Text("%ls", *CompName);
				DrawFunc(Ctx);
			};
			DrawFuncArray.Add(TPair<int, TFunction<void()>>{ DrawFuncPri, func });
		}
		else
		{
			auto func = [Id]() {
				ImGui::Text("Unknown unregistred component: %s", TCHAR_TO_UTF8(*FString::FromInt(Id)));
			};
			DrawFuncArray.Add(TPair<int, TFunction<void()>>{ 10000, func });
		}
	};

	for (auto Pair : Ctx.Reg->storage())
	{
		entt::id_type Id = Pair.first;
		if (Pair.second.contains(Ctx.Entity))
			func_draw_component(Id);
	}

	DrawFuncArray.Sort([](auto& A, auto& B) { return A.Key < B.Key; });

	ImGui::Separator();
	for (auto& E : DrawFuncArray)
	{
		E.Value();
		ImGui::Separator();
	}
}

void DrawEntityLink(FRegistry const& reg, FEntity entity, LRegistryImGuiWindowImpl& reg_window, int stack_level, string const& popup_name)
{
}

LRegistryImGuiWindowImpl::LRegistryImGuiWindowImpl()
{
	for (auto& CellPair : FactoryComponents.GetAll())
	{
		Filters.Add(CellPair.Key, false);
	}
}

void FImGuiComponentDrawContext::ChildDraw(FEntity E, FString const& Name) const
{
	string popup_name_full = std::string(TCHAR_TO_UTF8(*Name)) + to_string((u64)E) + "##" + to_string(StackLevel);
	if (ImGui::TreeNode(popup_name_full.c_str()))
	{
		RegWindow->DrawEntity(FImGuiComponentDrawContext{ RegWindow, Reg, E, StackLevel + 1 });
		ImGui::TreePop();
	}
}

FRegistryImGuiWindow::FRegistryImGuiWindow()
{
	Impl = MakeUnique<LRegistryImGuiWindowImpl>();
}

FRegistryImGuiWindow::~FRegistryImGuiWindow() = default;

void FRegistryImGuiWindow::DrawRegistry(FRegistry const& Reg)
{
	Impl->DrawReg(Reg);
}

void FRegistryImGuiWindow::DrawEntity(FImGuiComponentDrawContext const& Ctx)
{
	Impl->DrawEntity(Ctx);
}