#include "FGalaxyImGuiWindow.h"

#include "FRegistryImGuiWindow.h"
#include "Optigame/OptigameCore.h"
#include "imgui.h"

class FGalaxyImGuiWindowImpl
{
public:
	void Draw(AOptigameCore* OptigameCore);

	FRegistryImGuiWindow Drawer;
};

void FGalaxyImGuiWindowImpl::Draw(AOptigameCore* OptigameCore)
{
	ImGui::Text("Current star:");
	Drawer.DrawEntity(OptigameCore->Galaxy->Reg, OptigameCore->GalaxyProjector->CurrentViewStar.entity());
}

FGalaxyImGuiWindow::FGalaxyImGuiWindow()
{
	Impl = MakeUnique<FGalaxyImGuiWindowImpl>();
}

FGalaxyImGuiWindow::~FGalaxyImGuiWindow()
{
}

void FGalaxyImGuiWindow::Draw(AOptigameCore* OptigameCore)
{
	Impl->Draw(OptigameCore);
}