#pragma once

float	SpheresFitOnSphere(float PointR, float R);
FVector GetSphereFibPoint(int N, int I);

struct FPointsGenerator
{
	FPointsGenerator();
	FVector GetPoint();
	void	SkipPoints(int Count);

protected:
	int CurrentLevel = 0;
	int CurrentLevelN = 1;
	int CurrentI = 0;
};