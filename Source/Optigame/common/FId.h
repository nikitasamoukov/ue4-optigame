#pragma once

template <typename T, typename TagT>
struct FId
{
	T	 id = { T(-1) };
	auto operator<(const FId& a) const
	{
		return id < a.id;
	}
	auto operator==(const FId& a) const
	{
		return id == a.id;
	}
	auto operator!=(const FId& a) const
	{
		return id != a.id;
	}
};

template <typename T, typename TagT>
std::size_t GetTypeHash(FId<T, TagT> const& s) noexcept
{
	return std::hash<T>()(s.id);
}