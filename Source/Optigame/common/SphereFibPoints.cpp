#include "SphereFibPoints.h"

double SpheresFitOnSphere(double PointR, float R)
{
	return 4 * PI * (R / PointR) * (R / PointR) / 6;
}

FVector GetSphereFibPoint(int N, int I)
{
	constexpr double GoldenRatio = 1.6180339887498948482;
	double Theta = 2 * PI * (double)I / GoldenRatio;
	double Phi = FMath::Acos(1 - 2 * ((double)I + 0.5f) / (double)N);
	FVector Res(
		FMath::Cos(Theta) * FMath::Sin(Phi),
		FMath::Sin(Theta) * FMath::Sin(Phi),
		FMath::Cos(Phi));
	return Res;
}

FPointsGenerator::FPointsGenerator()
{
	SkipPoints(0);
}

FVector FPointsGenerator::GetPoint()
{
	if (CurrentLevel == 0)
		return FVector::ZeroVector;
	return GetSphereFibPoint(CurrentLevelN, CurrentI) * (double)CurrentLevel;
}

void FPointsGenerator::SkipPoints(int Count)
{
	CurrentI += Count;
	for (; CurrentLevel < 100 && CurrentI >= CurrentLevelN; CurrentLevel++)
	{
		CurrentI -= CurrentLevelN;
		CurrentLevelN = (int)SpheresFitOnSphere(0.5, (double)CurrentLevel + 1);
	}
}