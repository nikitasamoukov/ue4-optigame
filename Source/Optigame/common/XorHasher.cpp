#include "XorHasher.h"
#include <bit>

uint64 XorHash(FString const& String)
{
	std::string StrUTF8(TCHAR_TO_UTF8(*String));
	unsigned long long Hash = 0;
	for (auto& Ch : StrUTF8)
	{
		Hash = std::rotl(Hash, 7);
		Hash ^= Ch;
	}

	return Hash;
}