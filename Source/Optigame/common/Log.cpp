﻿DEFINE_LOG_CATEGORY(LogOptigame);

void LOG_IMPL(const char* File, int Line, const char* Function, FString const& Text)
{
	UE_LOG(LogOptigame, Warning, TEXT("%s (file:%s line:%i func:%s"), *Text, *FString(File), Line, *FString(Function));
}