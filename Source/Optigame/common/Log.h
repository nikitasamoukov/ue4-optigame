﻿DECLARE_LOG_CATEGORY_EXTERN(LogOptigame, Log, All);

void LOG_IMPL(const char* File, int Line, const char* Function, FString const& Text = "OOps");
#define __FILENAME__ (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#define LOG(...) (LOG_IMPL(__FILENAME__, __LINE__, __FUNCTION__, __VA_ARGS__))