#pragma once

#include "CommonMath.h"

FVector GeneratePointInSphere(i32 Seed)
{
	FRandomStream Rg(Seed);
	double Phi = Rg.FRandRange(0, 2 * PI);
	double CosTheta = Rg.FRandRange(-1, 1);
	double U = Rg.FRandRange(0, 1);

	double Theta = FMath::Acos(CosTheta);
	double R = FMath::Pow(U, 1.0f / 3);

	FVector Res;
	Res.X = R * FMath::Sin(Theta) * FMath::Cos(Phi);
	Res.Y = R * FMath::Sin(Theta) * FMath::Sin(Phi);
	Res.Z = R * FMath::Cos(Theta);

	return Res;
}