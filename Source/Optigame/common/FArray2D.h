#pragma once
/*
template <typename T>
class FArray2D
{
public:
	void resize(size_t size_x, size_t size_y);

	void clear();

	size_t size_x() const;
	size_t size_y() const;
	size_t size() const;

	auto begin();
	auto end();
	auto data();
	auto data() const;

	class FArray2D_index_proxy
	{
	public:
		T& operator [](size_t y);
	private:
		FArray2D_index_proxy(FArray2D<T>& m, size_t x);

		FArray2D<T>& _m;
		size_t _x;
		friend class FArray2D;
	};

	class FArray2D_index_proxy_const
	{
	public:
		const T& operator [](size_t y) const;
	private:
		FArray2D_index_proxy_const(const FArray2D<T>& m, size_t x);

		const FArray2D<T>& _m;
		size_t _x;
		friend class FArray2D;
	};

	FArray2D_index_proxy operator [](size_t x);
	FArray2D_index_proxy_const operator [](size_t x) const;

private:
	size_t _size_x = 0;
	vector<T> _data;
};

template <typename T>
void FArray2D<T>::resize(size_t size_x, size_t size_y)
{
	_data.clear();
	_data.resize(size_x * size_y);
	_size_x = size_x;
}

template <typename T>
void FArray2D<T>::clear()
{
	_data.clear();
	_size_x = 0;
}

template <typename T>
size_t FArray2D<T>::size_x() const
{
	return _size_x;
}

template <typename T>
size_t FArray2D<T>::size_y() const
{
	return _data.size() / _size_x;
}

template <typename T>
size_t FArray2D<T>::size() const
{
	return _data.size();
}

template <typename T>
auto FArray2D<T>::begin()
{
	return _data.begin();
}

template <typename T>
auto FArray2D<T>::end()
{
	return _data.end();
}

template <typename T>
auto FArray2D<T>::data()
{
	return _data.data();
}

template <typename T>
auto FArray2D<T>::data() const
{
	return _data.data();
}

template <typename T>
T& FArray2D<T>::FArray2D_index_proxy::operator[](size_t y)
{
	Assert(y < _m.size_y());
	return _m._data[y * _m._size_x + _x];
}

template <typename T>
FArray2D<T>::FArray2D_index_proxy::FArray2D_index_proxy(FArray2D<T>& m, size_t x) :
	_m(m),
	_x(x)
{
	Assert(x < _m._size_x);
}

template <typename T>
const T& FArray2D<T>::FArray2D_index_proxy_const::operator[](size_t y) const
{
	Assert(y < _m.size_y());
	return _m._data[y * _m._size_x + _x];
}

template <typename T>
FArray2D<T>::FArray2D_index_proxy_const::FArray2D_index_proxy_const(const FArray2D<T>& m, size_t x) :
	_m(m),
	_x(x)
{
	Assert(x < _m._size_x);
}

template <typename T>
typename FArray2D<T>::FArray2D_index_proxy FArray2D<T>::operator[](size_t x)
{
	return FArray2D_index_proxy(*this, x);
}

template <typename T>
typename FArray2D<T>::FArray2D_index_proxy_const FArray2D<T>::operator[](size_t x) const
{
	return FArray2D_index_proxy_const(*this, x);
}
*/