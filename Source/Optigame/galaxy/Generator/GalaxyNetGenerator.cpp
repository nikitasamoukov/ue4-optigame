// Fill out your copyright notice in the Description page of Project Settings.

#include "GalaxyNetGenerator.h"
#include <delaunator/delaunator.hpp>
#include <random>
#include "Optigame/common/FId.h"

class GraphNetGen
{
public:
	struct NodeTag
	{
	};
	using NodeId = FId<size_t, NodeTag>;
	struct Node
	{
		FVector2D	   pos{ 0 };
		FVector2D	   force{ 0 };
		TArray<NodeId> connections;

		void AddConnection(NodeId a)
		{
			if (connections.Find(a) == INDEX_NONE)
			{
				connections.Add(a);
			}
		}
		void DelConnection(NodeId a)
		{
			if (auto i = connections.Find(a); i != INDEX_NONE)
			{
				connections.RemoveAtSwap(i);
			}
		}
	};

	void Init(int count, double connections_percent, uint64 seed)
	{
		_connections_del_percent = 1 - connections_percent;
		_galaxy_size = sqrt(double(count)) * 65;
		_rg = std::mt19937_64{ seed };

		for (size_t i = 0; i < count; i++)
		{
			AddNode(GenPointInCircle(_rg, NodeLim({ i }, count)));
		}
	}

	void Generate()
	{
		for (size_t i = 0; i < 300; i++)
			Step();
		GenAllLinks();
		RemoveLongLinks();
		RemoveLinksPercentage();
	}

	double GalaxySizeDiff()
	{
		return _galaxy_size * _galaxy_size_diff;
	}

	double NodeLim(NodeId id, size_t count)
	{
		return _galaxy_size + id.id * (GalaxySizeDiff()) / count - GalaxySizeDiff() / 2.0f;
	}

	double NodeLim(NodeId id)
	{
		return NodeLim(id, _nodes.Num());
	}

	bool IsCorrect()
	{
		return _is_correct;
	}

	void Step()
	{
		_is_correct = true;

		for (auto& node : _nodes)
		{
			node.pos += GenPointInCircle(_rg, _rand_force);
		}

		for (size_t i = 0; i < _nodes.Num(); i++)
		{
			auto& node = _nodes[i];
			double dist = node.pos.Size();
			double node_lim = NodeLim({ i });
			if (dist > node_lim)
			{
				node.pos += (dist - node_lim) * (-node.pos).GetSafeNormal();
			}
		}

		_grid.Reset();
		for (size_t i = 0; i < _nodes.Num(); i++)
		{
			GridAdd({ i });
		}

		for (auto& node : _nodes)
		{
			node.force = { 0, 0 };
		}

		for (size_t i = 0; i < _nodes.Num(); i++)
		{
			auto& node = _nodes[i];

			GridQuery(node.pos, _star_push_dist, [&](NodeId node_id) {
				double	  dist = FVector2D::Distance(node.pos, _nodes[node_id.id].pos);
				FVector2D dir = (node.pos - _nodes[node_id.id].pos).GetSafeNormal();
				if (node_id.id == i)
					return;		 // self
				if (dist < 0.01) // singularity
				{
					_is_correct = false;
					return;
				}
				if (dist < _star_minimal_dist) // smash
				{
					node.force += dir * (_star_minimal_dist - dist) / 2.0f;
					_is_correct = false;
					return;
				}
				if (dist < _star_push_dist) // so near
				{
					double force_percent = 1 - (_star_minimal_dist - dist) / _star_push_dist;
					node.force += dir * (force_percent * _push_force) / 2.0f;
				}
			});
		}
		for (auto& node : _nodes)
		{
			node.pos += node.force;
		}
	}

	void GenAllLinks()
	{
		std::vector<double> coords;
		for (auto& node : _nodes)
		{
			// UE_LOG(LogTemp, Warning, TEXT("%f %f"), node.pos.X, node.pos.Y);
			coords.push_back(node.pos.X);
			coords.push_back(node.pos.Y);
		}

		delaunator::Delaunator dela(coords);

		for (std::size_t i = 0; i < dela.triangles.size() - 2; i += 3)
		{
			AddEdge({ dela.triangles[i + 0] }, { dela.triangles[i + 1] });
			AddEdge({ dela.triangles[i + 1] }, { dela.triangles[i + 2] });
			AddEdge({ dela.triangles[i + 2] }, { dela.triangles[i + 0] });
		}
	}

	void RemoveLongLinks()
	{
		TArray<TPair<TPair<NodeId, NodeId>, double>> conn_to_remove;

		for (size_t i = 0; i < _nodes.Num(); i++)
		{
			auto& node = _nodes[i];

			for (auto connection : node.connections)
			{
				if (connection.id < i)
					continue; // duplicate

				double dist = FVector2D::Distance(node.pos, _nodes[connection.id].pos);

				if (dist > _link_length_limit)
				{
					conn_to_remove.Add(TPair<TPair<NodeId, NodeId>, double>{ TPair<NodeId, NodeId>(NodeId{ i }, connection), dist });
				}
			}
		}

		conn_to_remove.Sort([](TPair<TPair<NodeId, NodeId>, double> const& a, TPair<TPair<NodeId, NodeId>, double> const& b) { return a.Value > b.Value; });

		for (auto edge : conn_to_remove)
		{
			TryDelEdge(edge.Key.Key, edge.Key.Value);
		}
	}

	void RemoveLinksPercentage()
	{
		int current_connections = 0;
		for (size_t i = 0; i < _nodes.Num(); i++)
			for (auto connection : _nodes[i].connections)
			{
				if (connection.id < i)
					continue; // duplicate
				current_connections++;
			}
		int min_connections = _nodes.Num() - 1;

		int can_removed_count = current_connections - min_connections;
		int remove_count = int(can_removed_count * _connections_del_percent);
		// clog("can_removed_count" + to_string(can_removed_count));
		// clog("remove_count" + to_string(remove_count));
		std::uniform_int_distribution<int> dist(0, _nodes.Num() - 1);

		for (int i = 0; i < remove_count; i++)
		{
			for (int r = 0; r < _remove_retry_count; r++)
			{
				size_t idx = dist(_rg);
				size_t conn_idx = 0;
				if (_nodes[idx].connections.Num() > 1)
				{
					std::uniform_int_distribution<int> dist_conn(0, _nodes[idx].connections.Num() - 1);
					conn_idx = dist_conn(_rg);
				}
				bool is_del = TryDelEdge({ idx }, _nodes[idx].connections[conn_idx]);
				if (is_del)
					break;
			}
		}
	}

	[[maybe_unused]] bool TryDelEdge(NodeId a, NodeId b)
	{
		DelEdge(a, b);
		bool is_connected = TestConnectivity(a, b, 10);
		if (!is_connected)
		{
			AddEdge(a, b);
		}
		return is_connected;
	}

	enum class FillTypes
	{
		Zero,
		A,
		B,
	};

	TArray<TPair<std::vector<NodeId>, FillTypes>> fronts;
	std::vector<NodeId>							  front_tmp;
	std::vector<FillTypes>						  node_fill;
	bool										  TestConnectivity(NodeId a, NodeId b, int steps)
	{
		node_fill.resize(_nodes.Num());
		fill(node_fill.begin(), node_fill.end(), FillTypes::Zero);

		fronts.SetNum(2);

		fronts[0].Key.clear();
		fronts[0].Key.push_back(a);
		fronts[0].Value = FillTypes::A;

		fronts[1].Key.clear();
		fronts[1].Key.push_back(b);
		fronts[1].Value = FillTypes::B;

		for (int i = 0; i < steps; i++)
		{
			for (auto& front_pair : fronts)
			{
				auto& front = front_pair.Key;
				auto& front_t = front_pair.Value;

				front_tmp.clear();

				for (auto& id : front)
				{
					if (node_fill[id.id] == front_t)
						continue;	 // duplicate
					if (node_fill[id.id] != FillTypes::Zero)
						return true; // connected
					node_fill[id.id] = front_t;
					for (auto& connection : _nodes[id.id].connections)
					{
						front_tmp.push_back(connection);
					}
				}
				std::swap(front_tmp, front);
				if (front.empty())
					return false; // end of move
			}
		}

		return false; // out of steps
	}

	void DelEdge(NodeId a, NodeId b)
	{
		_nodes[a.id].DelConnection(b);
		_nodes[b.id].DelConnection(a);
	}

	void AddEdge(NodeId a, NodeId b)
	{
		_nodes[a.id].AddConnection(b);
		_nodes[b.id].AddConnection(a);
	}

	TArray<FVector2D> GetPositions()
	{
		TArray<FVector2D> vec;
		for (auto& node : _nodes)
		{
			vec.Add({ node.pos });
		}

		return vec;
	}

	// No duplicates in array
	TArray<TPair<int, int>> GetConnections()
	{
		TArray<TPair<int, int>> vec;
		for (size_t i = 0; i < _nodes.Num(); i++)
		{
			auto& node = _nodes[i];
			for (auto connection : node.connections)
				if (i < connection.id)
					vec.Add(TPair<int, int>(i, connection.id));
		}

		return vec;
	}

private:
	static FVector2D GenPointInCircle(std::mt19937_64& rg, double range)
	{
		std::uniform_real_distribution<double> dis_range(0, range);
		std::uniform_real_distribution<double> dis_2_pi(0, PI * 2);

		double a = dis_2_pi(rg);
		double d = dis_range(rg) + dis_range(rg);
		d = d > range ? 2 * range - d : d;
		return { d * cos(a), d * sin(a) };
	}

	NodeId AddNode(FVector2D pos)
	{
		NodeId node_id = { _nodes.Num() };

		Node node;
		node.pos = pos;

		_nodes.Add(node);
		GridAdd(node_id);

		return node_id;
	}

	void MoveNode(NodeId node_id, FVector2D pos)
	{
		GridDel(node_id);

		Node& node = _nodes[node_id.id];
		node.pos = pos;

		GridAdd(node_id);
	}

	void GridAdd(NodeId node_id)
	{
		Node&	  node = _nodes[node_id.id];
		FIntPoint grid_index = PosToGrid(node.pos);
		if (!_grid.Find(grid_index))
			_grid.Add(grid_index, {});
		_grid[grid_index].Add(node_id);
	}

	void GridDel(NodeId node_id)
	{
		Node&	  node = _nodes[node_id.id];
		FIntPoint grid_index = PosToGrid(node.pos);
		_grid[grid_index].Remove(node_id);
		if (_grid[grid_index].Num() == 0)
		{
			_grid.Remove(grid_index);
		}
	}

	void GridQuery(FVector2D pos, double range, TFunction<void(NodeId)> const& func)
	{
		FIntPoint grid_pos_min = PosToGrid(pos - FVector2D(range));
		FIntPoint grid_pos_max = PosToGrid(pos + FVector2D(range));

		for (int x = grid_pos_min.X; x <= grid_pos_max.X; x++)
			for (int y = grid_pos_min.Y; y <= grid_pos_max.Y; y++)
				if (auto it = _grid.Find(FIntPoint(x, y)); it)
					for (auto id : *it)
						func(id);
	}

	FIntPoint PosToGrid(FVector2D pos)
	{
		auto doublePoint = pos / _star_push_dist;
		return { (int)FMath::FloorToInt(doublePoint.X), (int)FMath::FloorToInt(doublePoint.Y) };
	}

	double						  _galaxy_size = 0;
	double						  _connections_del_percent = 0;
	std::mt19937_64				  _rg;
	TArray<Node>				  _nodes;
	TMap<FIntPoint, TSet<NodeId>> _grid;

	bool _is_correct = false;

	const double _galaxy_size_diff = 0.3;
	const double _star_minimal_dist = 50;
	const double _star_push_dist = 100;
	const double _push_force = 3;
	const double _rand_force = 10;
	const double _link_length_limit = 200;
	const int	_remove_retry_count = 10;
};

FGalaxyNetGenerator::GeneratedGraphNet FGalaxyNetGenerator::GenerateNet(int Count, double ConnectionsPercent, uint64 Seed)
{
	GraphNetGen gen_net;

	gen_net.Init(Count, ConnectionsPercent, Seed);
	gen_net.Generate();

	GeneratedGraphNet net;

	net.Points = gen_net.GetPositions();
	net.Connects = gen_net.GetConnections();

	// for (int i = 0; i < net.Points.Num(); i++)
	//{
	//	auto& p = net.Points[i];
	// p.X = 1000 * sin((double)i / net.Points.Num() * PI * 2);
	// p.Y = 1000 * cos((double)i / net.Points.Num() * PI * 2);
	//}

	return net;
}