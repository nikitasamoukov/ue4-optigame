// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 *
 */
class FGalaxyNetGenerator
{
public:
	struct GeneratedGraphNet
	{
		TArray<FVector2D>		Points;
		TArray<TPair<int, int>> Connects; // No duplicates
	};

	GeneratedGraphNet GenerateNet(int Count, double ConnectionsPercent, uint64 Seed);
};