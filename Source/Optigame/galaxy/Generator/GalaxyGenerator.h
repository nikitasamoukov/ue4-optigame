// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "CoreMinimal.h"

struct FCompOrbit;

class FGalaxyGenerator
{
public:
	explicit FGalaxyGenerator(FRegistry& Reg);

	struct GeneratedGraphNet
	{
		TArray<FVector2D>		points;
		TArray<TPair<int, int>> connects;
	};

	void Generate(int Count, double ConnectionsPercent, uint64 Seed, int AiEmpiresCount);

protected:
	void	   GenerateStar(FRandomStream& Rg, FEntity StarEntity);
	FCompOrbit GenerateOrbit(FRandomStream& Rg, double Range);
	FEntity	   GeneratePlanet(FRandomStream& Rg, FEntity StarEntity, FCompOrbit const& COrbit);
	FEntity	   GenerateEmpire(FRandomStream& Rg);
	FEntity	   CreateWarpZone(FVector Pos, FEntity StarToWarp);

	void SpawnEmpires(FRandomStream& Rg);
	void SpawnEmpireAt(FRandomStream& Rg, FEntity Empire, FEntity Star);

	FRegistry& Reg;
};