// Fill out your copyright notice in the Description page of Project Settings.

#include "GalaxyGenerator.h"
#include "GalaxyNetGenerator.h"
#include "Algo/MaxElement.h"
#include "Algo/RemoveIf.h"
#include "Engine/Texture2D.h"
#include "Kismet/KismetMathLibrary.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Common/ColorSpace/ColorSpaceComparison.h"
#include "Optigame/Galaxy/BreedRegistry/LBreedRegistry.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsBuildQueue.h"
#include "Optigame/Galaxy/Components/ComponentsEmpire.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"
#include "Optigame/Galaxy/Handles/FEnttStarHandle.h"
#include "Optigame/Galaxy/Helpers/HelperBuilding.h"
#include "Optigame/Galaxy/Helpers/HelperColony.h"
#include "Optigame/Galaxy/Helpers/HelperShip.h"
#include "Optigame/Galaxy/Helpers/HelperStation.h"

FEntity CreateStar(FRegistry& Reg, FVector const& Pos)
{
	FEntity Entity = Reg.create();

	FCompTransform C_Transform = { Pos, {}, Pos, {} };
	Reg.emplace<FCompTransform>(Entity, C_Transform);

	FCompStar C_Star;
	Reg.emplace<FCompStar>(Entity, C_Star);

	return Entity;
}

struct LPlanetSpawnRange
{
	FCompPlanet::EType Type = FCompPlanet::EType::Earth;
	double Min = 0;
	double Max = 1;
};

TArray<LPlanetSpawnRange> GPlanetSpawnRanges = {
	{ FCompPlanet::EType::Earth, 0.4, 0.6 },
	{ FCompPlanet::EType::Ice, 0.6, 1.0 },
	{ FCompPlanet::EType::Lava, 0.0, 0.4 },
	{ FCompPlanet::EType::Rock, 0.2, 1.0 },
	{ FCompPlanet::EType::Desert, 0.2, 0.6 },
	{ FCompPlanet::EType::Water, 0.4, 0.6 },
};

LPlanetSpawnRange GetRange(FCompPlanet::EType Type)
{
	auto RangePtr = GPlanetSpawnRanges.FindByPredicate([&](auto& E) {
		return E.Type == Type;
	});
	if (RangePtr)
		return *RangePtr;
	return {};
}

FEntity FGalaxyGenerator::GeneratePlanet(FRandomStream& Rg, FEntity StarEntity, FCompOrbit const& COrbit)
{
	FEntity Planet = Reg.create();

	FCompPlanet CPlanet;

	FCompPlanet::EType Type = FCompPlanet::EType::Rock;
	double OrbitRangePercent = (COrbit.Radius - GOptigameVars.PlanetOrbitMin) / FMath::Max(GOptigameVars.PlanetOrbitMax - GOptigameVars.PlanetOrbitMin, 0.01f);
	OrbitRangePercent = FMath::Clamp<double>(OrbitRangePercent, 0, 1);

	TArray<FCompPlanet::EType> PossibleTypes;
	for (auto& TypeInfo : GPlanetSpawnRanges)
	{
		if (OrbitRangePercent >= TypeInfo.Min && OrbitRangePercent <= TypeInfo.Max)
			PossibleTypes.Add(TypeInfo.Type);
	}

	if (PossibleTypes.Num() > 0)
	{
		Type = PossibleTypes[Rg.RandRange(0, PossibleTypes.Num() - 1)];
	}

	CPlanet.Type = Type;

	CPlanet.Name = "Planet" + FString::FromInt(Rg.RandRange(1, 999));

	Reg.emplace<FCompPlanet>(Planet, CPlanet);
	Reg.emplace<FCompOrbit>(Planet, COrbit);

	int ColonySlotsCount = Rg.RandRange(5, 20);

	FCompColony CColony;
	for (int i = 0; i < ColonySlotsCount; i++)
		CColony.Buildings.Add(entt::null);

	// CColony.Buildings[0] = HelperBuilding::Create(Reg, *BreedRegistry, "b_city_2");
	// CColony.Buildings[1] = HelperBuilding::Create(Reg, *BreedRegistry, "b_city_1");
	// CColony.Buildings[2] = HelperBuilding::Create(Reg, *BreedRegistry, "b_farm_1");

	Reg.emplace<FCompColony>(Planet, CColony);

	Reg.emplace<FCompBuildQueue>(Planet);

	FCompTransform CTransform;
	CTransform.Pos = COrbit.CalculatePosition();
	CTransform.CopyToPrev();
	Reg.emplace<FCompTransform>(Planet, CTransform);
	Reg.emplace<FCompPosStar>(Planet, StarEntity);

	Reg.get<FCompStar>(StarEntity).Objects.Add(Planet);

	return Planet;
}

void FGalaxyGenerator::GenerateStar(FRandomStream& Rg, FEntity StarEntity)
{
	Reg.get<FCompStar>(StarEntity).Name = "Star" + FString::FromInt(Rg.RandRange(1, 999));

	size_t Planets = Rg.RandRange(2, 5);

	TArray<double> Distances;
	double DistancesSum = 0;
	for (size_t i = 0; i < Planets + 1; i++)
	{
		double RandDist = Rg.GetFraction();
		Distances.Add(RandDist);
		DistancesSum++;
	}

	double TargetRandSum = FMath::Max(GOptigameVars.PlanetOrbitMax - GOptigameVars.PlanetDistMin - GOptigameVars.PlanetDistMin * (Planets - 1), 0.01f);

	for (auto& D : Distances)
		D = D / FMath::Max(DistancesSum, 0.01f) * TargetRandSum;

	double PenPos = GOptigameVars.PlanetOrbitMin;

	for (auto& D : Distances)
	{
		PenPos += D;

		FCompOrbit COrbit = GenerateOrbit(Rg, PenPos);

		GeneratePlanet(Rg, StarEntity, COrbit);

		PenPos += GOptigameVars.PlanetDistMin;
	}
}

FCompOrbit FGalaxyGenerator::GenerateOrbit(FRandomStream& Rg, double Range)
{
	FCompOrbit COrbit;
	COrbit.Radius = Range;
	COrbit.CurrentAngle = Rg.FRandRange(0, PI * 2);
	COrbit.CalcAngularSpeed();
	return COrbit;
}

// Alpha ignored
double GetColorDist(FColor C1, FColor C2)
{
	FLinearColor Cl1 = FLinearColor(C1);
	FLinearColor Cl2 = FLinearColor(C2);

	ColorSpace::Rgb Rgb1(Cl1.R, Cl1.G, Cl1.B);
	ColorSpace::Rgb Rgb2(Cl2.R, Cl2.G, Cl2.B);
	return ColorSpace::Cie2000Comparison::Compare(&Rgb1, &Rgb2);
}

FLinearColor GenerateColor(FRandomStream& Rg)
{
	return UKismetMathLibrary::HSVToRGB(Rg.FRandRange(0, 360), FMath::Sqrt(Rg.FRandRange(0.5, 1)), Rg.FRandRange(0.5, 1), 1);
}

void GenerateEmpireColors(FRegistry& Reg, FRandomStream& Rg, FCompEmpire& CEmpire)
{
	CEmpire.Icon.ColorMain = GenerateColor(Rg).ToFColor(true);

	for (int i = 0; i < 30; i++)
	{
		CEmpire.Icon.ColorSecond = GenerateColor(Rg).ToFColor(true);
		// LOG(FString::Sanitizedouble(GetColorDist(CEmpire.Icon.ColorMain, CEmpire.Icon.ColorSecond)));
		if (GetColorDist(CEmpire.Icon.ColorMain, CEmpire.Icon.ColorSecond) > 0.3)
			break;
	}
	// LOG("Res:" + FString::Sanitizedouble(GetColorDist(CEmpire.Icon.ColorMain, CEmpire.Icon.ColorSecond)));
}

FEntity FGalaxyGenerator::GenerateEmpire(FRandomStream& Rg)
{
	FEntity Entity = Reg.create();
	FEnttHandle Empire(Reg, Entity);

	FCompEmpire& CEmpire = Empire.emplace_or_replace<FCompEmpire>();

	CEmpire.Name = "<Empire name>";
	GenerateEmpireColors(Reg, Rg, CEmpire);

	auto OptigameData = UOptigameProjectSettings::GetOptigameData();

	auto& EmpireIcons = OptigameData->EmpireIcons;

	if (EmpireIcons.Icons.Num() == 0)
	{
		LOG();
		return {};
	}

	CEmpire.Icon.Texture = FName(EmpireIcons.Icons[Rg.RandHelper(EmpireIcons.Icons.Num())]->GetName());

	for (auto& Pair : Reg.ctx().get<LBreedRegistry*>()->GetBreeds(LBreedRegistry::EBreedType::Resource))
	{
		auto& ResHandle = Pair.Value;
		if (!ResHandle.all_of<FCompResourceType>())
		{
			LOG("LBreedRegistry res no LCResourceType");
			continue;
		}
		CEmpire.Resources.Add(Pair.Key, 0);
		CEmpire.ResourcesChange.Add(Pair.Key, 0);
	}

	return Entity;
}

FEntity FGalaxyGenerator::CreateWarpZone(FVector Pos, FEntity StarToWarp)
{
	FEntity E = Reg.create();

	FCompTransform CTransform;
	CTransform.Pos = Pos;
	CTransform.CopyToPrev();
	Reg.emplace<FCompTransform>(E, CTransform);

	FCompWarpZone CWarpZone{ StarToWarp, EWarpZoneType::Star };
	Reg.emplace<FCompWarpZone>(E, CWarpZone);

	return E;
}

class LNetDistanceFiller
{
public:
	LNetDistanceFiller(FRegistry& Reg)
		: Reg(Reg)
	{
		for (auto [E, CStar] : Reg.view<FCompStar>().each())
		{
			StarDistToNearestEmpire.Add(E, 0);
		}
	}

	void Clear()
	{
		for (auto& Pair : StarDistToNearestEmpire)
			Pair.Value = 10000;
	}

	void AddFillStart(FEntity Star)
	{
		if (!Reg.valid(Star))
			return;
		if (!StarDistToNearestEmpire.Contains(Star))
			return;
		StarDistToNearestEmpire[Star] = 0;
	}

	void Fill()
	{
		int FillNum = 0;
		TArray<FEntity> FillFront;
		TArray<FEntity> FillFrontNew;

		for (auto& Pair : StarDistToNearestEmpire)
			if (Pair.Value == 0)
				FillFront.Add(Pair.Key);

		while (FillFront.Num() > 0)
		{
			FillNum++;
			for (auto E : FillFront)
				for (auto& Connect : Reg.get<FCompStar>(E).Connects)
					if (StarDistToNearestEmpire[Connect.Star] > FillNum)
					{
						StarDistToNearestEmpire[Connect.Star] = FillNum;
						FillFrontNew.Add(Connect.Star);
					}

			Swap(FillFront, FillFrontNew);
			FillFrontNew.SetNum(0, false);
		}
	}

	FRegistry& Reg;

	TMap<FEntity, int> StarDistToNearestEmpire;
};

void FGalaxyGenerator::SpawnEmpireAt(FRandomStream& Rg, FEntity Empire, FEntity Star)
{
	// LOG("EmpireSpawn");

	auto EarthPlRange = GetRange(FCompPlanet::EType::Earth);

	double RangeSize = GOptigameVars.PlanetOrbitMax - GOptigameVars.PlanetOrbitMin;

	double Radius = Rg.FRandRange(
		GOptigameVars.PlanetOrbitMin + RangeSize * EarthPlRange.Min,
		GOptigameVars.PlanetOrbitMin + RangeSize * EarthPlRange.Max);

	FCompOrbit COrbit = GenerateOrbit(Rg, Radius);

	Reg.get<FCompStar>(Star).Objects.SetNum(Algo::RemoveIf(Reg.get<FCompStar>(Star).Objects, [&](auto& E) {
		if (Reg.all_of<FCompOrbit>(E))
		{
			if (abs(Reg.get<FCompOrbit>(E).Radius - Radius) < GOptigameVars.PlanetDistMin)
			{
				return true;
			}
		}
		return false;
	}));

	auto Planet = GeneratePlanet(Rg, Star, COrbit);

	Reg.get<FCompPlanet>(Planet).Type = FCompPlanet::EType::Earth;

	FCompColony& CColony = Reg.emplace_or_replace<FCompColony>(Planet);

	CColony.Buildings.SetNum(0);

	auto SpawnBreed = Reg.ctx().get<LBreedRegistry*>()->GetBreed("es_default");
	if (!SpawnBreed.IsValid())
	{
		LOG();
		return;
	}
	auto& CEmpireSpawnInfo = SpawnBreed.get_or_emplace<FCompEmpireSpawnInfo>();

	auto& CEmpire = Reg.get<FCompEmpire>(Empire);

	for (auto& Pair : CEmpireSpawnInfo.Resources.Resources)
	{
		CEmpire.Resources.FindOrAdd(Pair.Key) = Pair.Value;
	}

	for (auto& E : CEmpireSpawnInfo.ShipTemplates)
	{
		auto Entity = Reg.create();
		Reg.emplace<FCompShipTemplate>(Entity, E);
		CEmpire.ShipTemplates.Add(Entity);
	}

	// CEmpire.Resources["res_food"] = 10001;
	// CEmpire.Resources["res_ore"] = 10002;
	// CEmpire.Resources["res_metal"] = 10003;
	// CEmpire.Resources["res_chips"] = 10004;
	// CEmpire.Resources["res_processors"] = 10005;

	CColony.Population = CEmpireSpawnInfo.Population;
	for (auto BuildingId : CEmpireSpawnInfo.Buildings)
	{
		CColony.Buildings.Add(HelperBuilding::Create(Reg, *Reg.ctx().get<LBreedRegistry*>(), BuildingId, Planet));
	}

	for (size_t i = 0; i < CEmpireSpawnInfo.ColonySize && CColony.Buildings.Num() < CEmpireSpawnInfo.ColonySize; i++)
		CColony.Buildings.Add(entt::null);

	FCompOwner COwner;
	COwner.Empire = Empire;
	Reg.emplace_or_replace<FCompOwner>(Planet, COwner);

	if (CEmpireSpawnInfo.Station.HullId != NAME_None)
	{
		UHelperColony::CreateStation(FEnttColonyHandle(Reg, Planet), CEmpireSpawnInfo.Station.HullId);

		for (auto& Id : CEmpireSpawnInfo.Station.Modules)
		{
			UHelperStation::AddModule({ Reg, Reg.get<FCompColony>(Planet).Station }, { Id, 100, 1 });
		}
	}

	for (auto& ShipsToSpawn : CEmpireSpawnInfo.Ships)
	{
		auto It = CEmpire.ShipTemplates.FindByPredicate([&](FEntity const& E) {
			return Reg.get<FCompShipTemplate>(E).Name == ShipsToSpawn.Name;
		});
		if (!It)
		{
			LOG();
			continue;
		}

		auto& CShipTemplate = Reg.get<FCompShipTemplate>(*It);

		auto PlanetPos = Reg.get<FCompTransform>(Planet).Pos;
		FTransform Trs;
		Trs.SetLocation(PlanetPos + FVector(0, 0, GOptigameVars.GamePlaneHeight));
		UHelperShip::SpawnShips(CShipTemplate, FEnttStarHandle(Reg, Star), Trs, ShipsToSpawn.Count);
	}
}

void FGalaxyGenerator::SpawnEmpires(FRandomStream& Rg)
{
	TArray<FEntity> Empires;
	TArray<FEntity> EmpiresSpawns;
	TArray<FEntity> Stars;

	for (auto [E, CEmpire] : Reg.view<FCompEmpire>().each())
	{
		Empires.Add(E);
		EmpiresSpawns.Add(entt::null);
	}

	for (auto [E, CStar] : Reg.view<FCompStar>().each())
	{
		Stars.Add(E);
	}

	if (Empires.Num() > Stars.Num())
	{
		LOG("Bad SpawnEmpires: Empires:" + FString::FromInt(Empires.Num()) + " Stars:" + FString::FromInt(Stars.Num()));
	}

	for (int i = 0; i < Empires.Num(); i++)
		EmpiresSpawns[i] = Stars[i];

	LNetDistanceFiller Filler(Reg);

	for (int i = 0; i < Empires.Num(); i++)
	{
		Filler.Clear();
		for (int r = 0; r < Empires.Num(); r++)
			if (i != r)
				Filler.AddFillStart(EmpiresSpawns[r]);

		Filler.Fill();

		auto PosSpawnScore = [&](FEntity const& E) {
			double Score = 0;
			if (Reg.get<FCompStar>(E).Connects.Num() == 2)
				Score = -2;
			if (Reg.get<FCompStar>(E).Connects.Num() == 1)
				Score = -5;
			Score += Filler.StarDistToNearestEmpire[E];

			return Score;
		};
		auto PairMaxPtr = Algo::MaxElement(Filler.StarDistToNearestEmpire, [&](auto& A, auto& B) { return PosSpawnScore(A.Key) < PosSpawnScore(B.Key); });

		EmpiresSpawns[i] = PairMaxPtr->Key;
	}

	for (int i = 0; i < Empires.Num(); i++)
	{
		SpawnEmpireAt(Rg, Empires[i], EmpiresSpawns[i]);
	}
}

FGalaxyGenerator::FGalaxyGenerator(FRegistry& Reg)
	: Reg(Reg)
{
}

void FGalaxyGenerator::Generate(int Count, double ConnectionsPercent, uint64 Seed, int AiEmpiresCount)
{
	FGalaxyNetGenerator GalaxyNetGenerator;
	auto GraphNet = GalaxyNetGenerator.GenerateNet(Count, ConnectionsPercent, Seed);

	TArray<FEntity> Stars;

	for (auto& Point : GraphNet.Points)
		Stars.Add(CreateStar(Reg, FVector(Point.X, Point.Y, 0) * 4));

	for (auto& Pair : GraphNet.Connects)
	{
		FVector2D Dir = GraphNet.Points[Pair.Value] - GraphNet.Points[Pair.Key];
		Dir.Normalize();
		FVector WarpPosA{ Dir.X * GOptigameVars.WarpZoneDistance, Dir.Y * GOptigameVars.WarpZoneDistance, GOptigameVars.GamePlaneHeight };
		FVector WarpPosB{ -Dir.X * GOptigameVars.WarpZoneDistance, -Dir.Y * GOptigameVars.WarpZoneDistance, GOptigameVars.GamePlaneHeight };

		auto WarpZoneA = CreateWarpZone(WarpPosA, Stars[Pair.Value]);
		auto WarpZoneB = CreateWarpZone(WarpPosB, Stars[Pair.Key]);

		auto& CStar1 = Reg.get<FCompStar>(Stars[Pair.Key]);
		CStar1.Connects.Add({ .Star = Stars[Pair.Value], .WarpZone = WarpZoneA });
		auto& CStar2 = Reg.get<FCompStar>(Stars[Pair.Value]);
		CStar2.Connects.Add({ .Star = Stars[Pair.Key], .WarpZone = WarpZoneB });
	}

	FRandomStream Rg;
	Rg.Initialize((int32_t)Seed);

	for (auto& Star : Stars)
	{
		GenerateStar(Rg, Star);
	}

	FCtxPlayerInfo CtxPlayerInfo;

	CtxPlayerInfo.Empire = GenerateEmpire(Rg);
	Reg.ctx().emplace<FCtxPlayerInfo>(CtxPlayerInfo);

	for (int i = 0; i < AiEmpiresCount; i++)
	{
		auto AiEmpire = GenerateEmpire(Rg);
	}

	SpawnEmpires(Rg);
}