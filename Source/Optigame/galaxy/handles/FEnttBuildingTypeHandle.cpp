// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttBuildingTypeHandle.h"
#include "Optigame/Galaxy/Components/ComponentsBuilding.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Misc/NumFmt.h"

FEnttBuildingTypeHandle::FEnttBuildingTypeHandle()
	: FEnttHandle()
{
}

FEnttBuildingTypeHandle::FEnttBuildingTypeHandle(FRegistry& Reg, FEntity Entity)
	: FEnttHandle(Reg, Entity)
{
}

FEnttBuildingTypeHandle::FEnttBuildingTypeHandle(FEnttHandle Handle)
	: FEnttHandle(Handle)
{
}

bool FEnttBuildingTypeHandle::IsValid() const
{
	if (!FEnttHandle::IsValid())
		return false;

	return all_of<FCompBuildingType>();
}

FEnttBuildingTypeHandle::~FEnttBuildingTypeHandle()
{
}

UTexture2D* FEnttBuildingTypeHandle::GetTexture() const
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	if (!IsValid())
		return nullptr;
	auto Id = get<FCompBuildingType>().ImageId;
	if (!OptigameData->TexturesMap.Contains(Id))
		return nullptr;
	return OptigameData->TexturesMap[Id];
}

FText FEnttBuildingTypeHandle::GetName() const
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	if (!IsValid())
		return FText::FromString("BuildingTypeHandle invalid");
	auto Id = get<FCompBuildingType>().NameId;
	return OptigameData->GetFTextFromId(Id);
}

class LBuildingTextCreateFactory
{
public:
	LBuildingTextCreateFactory();

	FText CreateText(FEnttBuildingTypeHandle Handle)
	{
		FString Res;
		for (auto pair : Handle.storage())
		{
			entt::id_type Id = pair.first;

			if (TextGetFuncMap.Contains(Id))
			{
				auto StrC = TextGetFuncMap[Id](Handle);
				if (!StrC.IsEmpty())
				{
					if (!Res.IsEmpty())
						Res += "\n";
					Res += StrC;
				}
			}
			else
			{
				if (Id == entt::type_id<FCompBreedId>().hash())
					continue;
				if (!Res.IsEmpty())
					Res += "\n";
				Res += "No text factory for " + FString::FromInt(Id);
			}
		}

		return FText::FromString(Res);
	}

protected:
	void RegisterComponent(FName Name, entt::type_info Id,
		TFunction<FString(FEnttBuildingTypeHandle Handle)> LoaderFunc)
	{
		if (TextGetFuncMap.Contains(Id.hash()))
		{
			LOG("Component name double:" + Name.ToString());
			return;
		}
		TextGetFuncMap.Add(Id.hash(), LoaderFunc);
	}
	TMap<entt::id_type, TFunction<FString(FEnttBuildingTypeHandle Handle)>> TextGetFuncMap;
};

FText FEnttBuildingTypeHandle::GetInfo() const
{
	if (!IsValid())
		return FText::FromString("BuildingTypeHandle invalid");
	auto Id = get<FCompBuildingType>().NameId;

	static LBuildingTextCreateFactory Factory;
	return Factory.CreateText(*this);
}

template <class>
inline constexpr bool always_false_v = false;

template <typename C>
FString GetComponentTextImpl(FEnttBuildingTypeHandle Handle, C const& Component)
{
	static_assert(!"NoLoader" && always_false_v<C>);
	return {};
}

template <>
FString GetComponentTextImpl<>(FEnttBuildingTypeHandle Handle, FCompBuildingType const& Component)
{
	return "";
}

template <>
FString GetComponentTextImpl<>(FEnttBuildingTypeHandle Handle, FCompBuildingHouse const& Component)
{
	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	FText Str = OptigameData->GetFTextFromId("building_c_house_desc");

	FFormatNamedArguments Arguments;
	Arguments.Add("Size", UNumFmt::NumFmtStats(Component.Size));
	return FText::Format(Str, Arguments).ToString();
}

template <>
FString GetComponentTextImpl<>(FEnttBuildingTypeHandle Handle, FCompBuildingWorkers const& Component)
{
	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	FText Str = OptigameData->GetFTextFromId("building_c_workers_desc");

	FFormatNamedArguments Arguments;
	Arguments.Add("Workers", UNumFmt::NumFmtStats(Component.Workers));
	return FText::Format(Str, Arguments).ToString();
}

template <>
FString GetComponentTextImpl<>(FEnttBuildingTypeHandle Handle, FCompBuildingProduct const& Component)
{
	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	FText Str = OptigameData->GetFTextFromId("building_c_product_desc");
	FText StrRes = OptigameData->GetFTextFromId("building_resource_plus_text");

	FString StrText;
	for (auto& Pair : Component.Out.Resources)
	{
		if (!StrText.IsEmpty())
			StrText += ", ";

		auto HandleResType = OptigameData->BreedRegistry.GetBreed(Pair.Key);
		if (!HandleResType)
		{
			LOG();
			continue;
		}

		FFormatNamedArguments Arguments;
		Arguments.Add("Count", UNumFmt::NumFmtStats(Pair.Value));

		if (HandleResType.all_of<FCompResourceType>())
			Arguments.Add("Text", OptigameData->GetFTextFromId(HandleResType.get<FCompResourceType>().NameId));
		else
			LOG("Pair.Key is res? " + Pair.Key.ToString());

		FText ResText = FText::Format(StrRes, Arguments);

		StrText += ResText.ToString();
	}

	FFormatNamedArguments Arguments;
	Arguments.Add("Text", FText::FromString(StrText));

	return FText::Format(Str, Arguments).ToString();
}

template <>
FString GetComponentTextImpl<>(FEnttBuildingTypeHandle Handle, FCompBuildingConsume const& Component)
{
	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	FText Str = OptigameData->GetFTextFromId("building_c_consume_desc");
	FText StrRes = OptigameData->GetFTextFromId("building_resource_minus_text");

	FString StrText;
	for (auto& Pair : Component.In.Resources)
	{
		if (!StrText.IsEmpty())
			StrText += ", ";

		auto HandleResType = OptigameData->BreedRegistry.GetBreed(Pair.Key);
		if (!HandleResType)
		{
			LOG();
			continue;
		}

		FFormatNamedArguments Arguments;
		Arguments.Add("Count", UNumFmt::NumFmtStats(Pair.Value));

		if (HandleResType.all_of<FCompResourceType>())
			Arguments.Add("Text", OptigameData->GetFTextFromId(HandleResType.get<FCompResourceType>().NameId));
		else
			LOG("Pair.TypeId is res? " + Pair.Key.ToString());

		FText ResText = FText::Format(StrRes, Arguments);

		StrText += ResText.ToString();
	}

	FFormatNamedArguments Arguments;
	Arguments.Add("Text", FText::FromString(StrText));

	return FText::Format(Str, Arguments).ToString();
}

template <>
FString GetComponentTextImpl<>(FEnttBuildingTypeHandle Handle, FCompBuildingCost const& Component)
{
	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	FText Str = OptigameData->GetFTextFromId("building_c_cost_desc");

	FString StrText;
	for (auto& Pair : Component.Cost.Resources)
	{
		if (!StrText.IsEmpty())
			StrText += ", ";

		auto HandleResType = OptigameData->BreedRegistry.GetBreed(Pair.Key);
		if (!HandleResType)
		{
			LOG();
			continue;
		}

		FFormatNamedArguments Arguments;
		Arguments.Add("Count", UNumFmt::NumFmtCost(Pair.Value));

		if (HandleResType.all_of<FCompResourceType>())
			Arguments.Add("Text", OptigameData->GetFTextFromId(HandleResType.get<FCompResourceType>().NameId));
		else
			LOG("Pair.TypeId is res? " + Pair.Key.ToString());

		FText ResText = FText::Format(FText::FromString("{Count}{Text}"), Arguments);

		StrText += ResText.ToString();
	}

	FFormatNamedArguments Arguments;
	Arguments.Add("Text", FText::FromString(StrText));

	return FText::Format(Str, Arguments).ToString();
}

template <typename C>
FString GetComponentText(FEnttBuildingTypeHandle Handle)
{
	C& Component = Handle.get<C>();
	return GetComponentTextImpl(Handle, Component);
}

#define REG_COMPONENT(Component) \
	RegisterComponent(#Component, entt::type_id<Component>(), GetComponentText<Component>)

LBuildingTextCreateFactory::LBuildingTextCreateFactory()
{
	REG_COMPONENT(FCompBuildingType);
	REG_COMPONENT(FCompBuildingHouse);
	REG_COMPONENT(FCompBuildingProduct);
	REG_COMPONENT(FCompBuildingConsume);
	REG_COMPONENT(FCompBuildingWorkers);

	REG_COMPONENT(FCompBuildingCost);
}

#undef REG_COMPONENT