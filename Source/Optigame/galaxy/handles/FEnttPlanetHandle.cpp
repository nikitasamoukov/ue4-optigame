// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttPlanetHandle.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"
#include "FEnttColonyHandle.h"

FEnttPlanetHandle::FEnttPlanetHandle()
	: FEnttHandle()
{
}

FEnttPlanetHandle::FEnttPlanetHandle(FRegistry& Reg, FEntity Entity)
	: FEnttHandle(Reg, Entity)
{
}

FEnttPlanetHandle::FEnttPlanetHandle(FEnttHandle Handle)
	: FEnttHandle(Handle)
{
}

bool FEnttPlanetHandle::IsValid() const
{
	if (!FEnttHandle::IsValid())
		return false;

	return all_of<FCompPlanet, FCompCurrentTimePos>();
}

FVector FEnttPlanetHandle::GetPosition() const
{
	if (!IsValid())
		return {};

	return get<FCompCurrentTimePos>().Pos;
}

EPlanetType FEnttPlanetHandle::GetType() const
{
	if (!IsValid())
		return EPlanetType::Unknown;

	auto Type = get<FCompPlanet>().Type;

	switch (Type)
	{
		case FCompPlanet::EType::Desert:
			return EPlanetType::Desert;
		case FCompPlanet::EType::Earth:
			return EPlanetType::Earth;
		case FCompPlanet::EType::Ice:
			return EPlanetType::Ice;
		case FCompPlanet::EType::Lava:
			return EPlanetType::Lava;
		case FCompPlanet::EType::Rock:
			return EPlanetType::Rock;
		case FCompPlanet::EType::Water:
			return EPlanetType::Water;
		default:
			return EPlanetType::Unknown;
	}
}

FEnttEmpireHandle FEnttPlanetHandle::GetOwner() const
{
	if (!IsValid())
		return {};
	if (!all_of<FCompOwner>())
		return {};
	return { *registry(), get<FCompOwner>().Empire };
}

FString FEnttPlanetHandle::GetName() const
{
	if (!IsValid())
		return "Unknown";

	return get<FCompPlanet>().Name;
}

void FEnttPlanetHandle::SetName(FString const& Name) const
{
	if (!IsValid())
		return;
	get<FCompPlanet>().Name = Name;
}

bool FEnttPlanetHandle::HasColony() const
{
	if (!IsValid())
		return false;
	return all_of<FCompColony>();
}

FEnttColonyHandle FEnttPlanetHandle::GetColony() const
{
	return FEnttColonyHandle(*this);
}

FEnttColonyHandle UFEnttPlanetHandleLibrary::GetColony(FEnttPlanetHandle const& Handle)
{
	return Handle.GetColony();
}

FEnttPlanetHandle::~FEnttPlanetHandle()
{
}