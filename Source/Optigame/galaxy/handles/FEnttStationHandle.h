// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FEnttStationHandle.generated.h"

USTRUCT(BlueprintType)
struct FEnttStationHandle : public FEnttHandle
{
	GENERATED_BODY()
public:
	FEnttStationHandle();
	FEnttStationHandle(FRegistry& Reg, FEntity Entity);
	explicit FEnttStationHandle(FEnttHandle Handle);

	~FEnttStationHandle();

	// Extend Functions

	bool  IsValid() const;
	void  RemoveModule(int Idx) const;
	FName GetHullMeshId() const;
};

UCLASS()
class UFEnttStationHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttStationHandle const& Handle) { return Handle.IsValid(); }
	UFUNCTION(BlueprintCallable)
	static void RemoveModule(FEnttStationHandle const& Handle, int Idx) { Handle.RemoveModule(Idx); }
};