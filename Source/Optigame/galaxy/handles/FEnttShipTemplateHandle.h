// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Optigame/Misc/FRegistry.h"

#include "FEnttShipTemplateHandle.generated.h"

USTRUCT(BlueprintType)
struct FEnttShipTemplateHandle : public FEnttHandle
{
	GENERATED_BODY()
public:
	FEnttShipTemplateHandle();

	// Reg can be nullptr
	FEnttShipTemplateHandle(FRegistry& Reg, FEntity Entity);
	explicit FEnttShipTemplateHandle(FEnttHandle Handle);

	~FEnttShipTemplateHandle();

	// Extend Functions

	bool IsValid() const;
};

UCLASS()
class UFEnttShipTemplateHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttShipTemplateHandle const& Handle) { return Handle.IsValid(); }
};