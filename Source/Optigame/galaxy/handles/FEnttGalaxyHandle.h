// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FEnttEmpireHandle.h"
#include "FEnttGalaxyHandle.generated.h"

class FGalaxy;

USTRUCT(BlueprintType)
struct FEnttGalaxyHandle
{
	GENERATED_BODY()
public:
	FEnttGalaxyHandle();
	FEnttGalaxyHandle(FGalaxy* Galaxy);

	~FEnttGalaxyHandle();

	// Extend Functions

	bool IsValid() const;

	FEnttEmpireHandle GetPlayerEmpire() const;
	void SetTimeSpeed(double TimeSpeed) const;

	FRegistry* Reg = nullptr;
	FGalaxy* Galaxy = nullptr;
};

UCLASS()
class UFEnttGalaxyHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttGalaxyHandle const& Handle) { return Handle.IsValid(); }
	UFUNCTION(BlueprintCallable)
	static FEnttEmpireHandle GetPlayerEmpire(FEnttGalaxyHandle const& Handle) { return Handle.GetPlayerEmpire(); }
	UFUNCTION(BlueprintCallable)
	static void SetTimeSpeed(FEnttGalaxyHandle const& Handle, double TimeSpeed) { Handle.SetTimeSpeed(TimeSpeed); }
};