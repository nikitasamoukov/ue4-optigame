// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttColonyHandle.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsBuilding.h"
#include "FEnttBuildingTypeHandle.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"

FEnttColonyHandle::FEnttColonyHandle()
	: FEnttHandle()
{
}

FEnttColonyHandle::FEnttColonyHandle(FRegistry& Reg, FEntity Entity)
	: FEnttHandle(Reg, Entity)
{
}

FEnttColonyHandle::FEnttColonyHandle(FEnttHandle Handle)
	: FEnttHandle(Handle)
{
}

bool FEnttColonyHandle::IsValid() const
{
	if (!FEnttHandle::IsValid())
		return false;

	return has<FCompPlanet, FCompColony, FCompTransform>();
}

int FEnttColonyHandle::GetSlotsCount() const
{
	if (!IsValid())
		return 0;
	return get<FCompColony>().Buildings.Num();
}

FEnttColonyBuildingSlotHandle FEnttColonyHandle::GetSlot(int Idx) const
{
	return FEnttColonyBuildingSlotHandle(*this, Idx);
}

int FEnttColonyHandle::GetHouse() const
{
	if (!IsValid())
		return 0;
	return get<FCompColony>().House;
}

int FEnttColonyHandle::GetPopulation() const
{
	if (!IsValid())
		return 0;
	return get<FCompColony>().Population;
}

double FEnttColonyHandle::GetPopulationProgress() const
{
	if (!IsValid())
		return 0;
	return get<FCompColony>().PopulationProgress;
}

double FEnttColonyHandle::GetPopulationProgressIncome() const
{
	if (!IsValid())
		return 0;
	return get<FCompColony>().PopulationProgressIncome;
}

int FEnttColonyHandle::GetWorkerPlaces() const
{
	if (!IsValid())
		return 0;
	return get<FCompColony>().WorkerPlaces;
}

FText FEnttColonyHandle::GetResourcesInfo() const
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	if (!IsValid())
		return FText::FromString("<Error>");

	FText StrResPlus = OptigameData->GetFTextFromId("planet_info_resource_plus");
	FText StrResZero = OptigameData->GetFTextFromId("planet_info_resource_zero");
	FText StrResMinus = OptigameData->GetFTextFromId("planet_info_resource_minus");

	FString StrText;
	for (auto& e : get<FCompColony>().GetResourceDiff())
	{
		if (!StrText.IsEmpty())
			StrText += ", ";

		auto HandleResType = OptigameData->BreedRegistry.GetBreed(e.Key);
		if (!HandleResType)
		{
			LOG();
			continue;
		}

		FFormatNamedArguments Arguments;
		Arguments.Add("Count", FText::AsNumber(FMath::Abs(e.Value)));

		if (HandleResType.all_of<FCompResourceType>())
		{
			Arguments.Add("Text", OptigameData->GetFTextFromId(HandleResType.get<FCompResourceType>().NameId));
		}
		else
			LOG("e.TypeId is res? " + e.Key.ToString());

		FText ResText;

		if (e.Value > 0)
			ResText = FText::Format(StrResPlus, Arguments);
		else if (e.Value == 0)
			ResText = FText::Format(StrResZero, Arguments);
		else
			ResText = FText::Format(StrResMinus, Arguments);

		StrText += ResText.ToString();
	}

	return FText::FromString(StrText);
}

FEnttEmpireHandle FEnttColonyHandle::GetEmpire() const
{
	if (!IsValid())
		return {};
	if (!has<FCompOwner>())
		return {};

	return FEnttEmpireHandle(*registry(), get<FCompOwner>().Empire);
}

FEnttColonyBuildingSlotHandle::FEnttColonyBuildingSlotHandle(FEnttColonyHandle Handle, int Idx)
{
	this->Handle = Handle;
	this->Idx = Idx;
}

bool FEnttColonyBuildingSlotHandle::IsValid() const
{
	return Handle.IsValid() && Idx >= 0 && Idx < Handle.get<FCompColony>().Buildings.Num();
}

bool FEnttColonyBuildingSlotHandle::HaveBuilding() const
{
	if (!IsValid())
		return false;
	auto BuildingEntity = Handle.get<FCompColony>().Buildings[Idx];
	return Handle.registry()->valid(BuildingEntity);
}

FEnttBuildingTypeHandle FEnttColonyBuildingSlotHandle::GetBuildingTypeHandle() const
{
	if (!IsValid())
		return {};
	auto  BuildingEntity = Handle.get<FCompColony>().Buildings[Idx];
	auto& Reg = *Handle.registry();
	if (!Reg.valid(BuildingEntity))
		return {};
	if (!Reg.all_of<FCompBuilding>(BuildingEntity))
		return {};
	auto BasicHandle = Reg.get<FCompBuilding>(BuildingEntity).TypeBreed;
	return { *BasicHandle.registry(), BasicHandle.entity() };
}

TArray<FEnttBuildingTypeHandle> FEnttColonyBuildingSlotHandle::GetPossibleBuildingTypes() const
{
	auto							OptigameData = UOptigameProjectSettings::GetOptigameData();
	TArray<FEnttBuildingTypeHandle> Res;

	for (auto Pair : OptigameData->BreedRegistry.GetBreeds(LBreedRegistry::EBreedType::Building))
	{
		auto&					H = Pair.Value;
		FEnttBuildingTypeHandle HandleType(*H.registry(), H.entity());
		if (Handle.IsValid())
		{
			Res.Add(HandleType);
		}
	}

	return Res;
}

FEnttColonyHandle::~FEnttColonyHandle()
{
}