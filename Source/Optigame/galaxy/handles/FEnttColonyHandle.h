// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FEnttBuildingTypeHandle.h"
#include "FEnttEmpireHandle.h"
#include "FEnttColonyHandle.generated.h"

struct FEnttColonyBuildingSlotHandle;

USTRUCT(BlueprintType)
struct FEnttColonyHandle : public FEnttHandle
{
	GENERATED_BODY()
public:
	FEnttColonyHandle();
	FEnttColonyHandle(FRegistry& Reg, FEntity Entity);
	explicit FEnttColonyHandle(FEnttHandle Handle);

	~FEnttColonyHandle();

	// Extend Functions

	bool IsValid() const;

	int							  GetSlotsCount() const;
	FEnttColonyBuildingSlotHandle GetSlot(int Idx) const;

	int	  GetHouse() const;
	int	  GetPopulation() const;
	double GetPopulationProgress() const;
	double GetPopulationProgressIncome() const;
	int	  GetWorkerPlaces() const;
	FText GetResourcesInfo() const;

	FEnttEmpireHandle GetEmpire() const;
};

USTRUCT(BlueprintType)
struct FEnttColonyBuildingSlotHandle
{
	GENERATED_BODY()
public:
	FEnttColonyBuildingSlotHandle() = default;
	FEnttColonyBuildingSlotHandle(FEnttColonyHandle Handle, int Idx);

	// Extend Functions

	bool IsValid() const;

	bool					HaveBuilding() const;
	FEnttBuildingTypeHandle GetBuildingTypeHandle() const;

	TArray<FEnttBuildingTypeHandle> GetPossibleBuildingTypes() const;

	int				  Idx = -1;
	FEnttColonyHandle Handle;
};

UCLASS()
class UFEnttColonyHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttColonyHandle const& Handle) { return Handle.IsValid(); }
	UFUNCTION(BlueprintCallable)
	static int GetSlotsCount(FEnttColonyHandle const& Handle) { return Handle.GetSlotsCount(); }
	UFUNCTION(BlueprintCallable)
	static FEnttColonyBuildingSlotHandle GetSlot(FEnttColonyHandle const& Handle, int Idx) { return Handle.GetSlot(Idx); }
	UFUNCTION(BlueprintCallable)
	static int GetHouse(FEnttColonyHandle const& Handle) { return Handle.GetHouse(); }
	UFUNCTION(BlueprintCallable)
	static int GetPopulation(FEnttColonyHandle const& Handle) { return Handle.GetPopulation(); }
	UFUNCTION(BlueprintCallable)
	static double GetPopulationProgress(FEnttColonyHandle const& Handle) { return Handle.GetPopulationProgress(); }
	UFUNCTION(BlueprintCallable)
	static double GetPopulationProgressIncome(FEnttColonyHandle const& Handle) { return Handle.GetPopulationProgressIncome(); }
	UFUNCTION(BlueprintCallable)
	static double GetWorkerPlaces(FEnttColonyHandle const& Handle) { return Handle.GetWorkerPlaces(); }
	UFUNCTION(BlueprintCallable)
	static FText GetResourcesInfo(FEnttColonyHandle const& Handle) { return Handle.GetResourcesInfo(); }
	UFUNCTION(BlueprintCallable)
	static FEnttEmpireHandle GetEmpire(FEnttColonyHandle const& Handle) { return Handle.GetEmpire(); }
};

UCLASS()
class UFEnttColonyBuildingSlotHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttColonyBuildingSlotHandle const& Handle) { return Handle.IsValid(); }

	UFUNCTION(BlueprintCallable)
	static bool HaveBuilding(FEnttColonyBuildingSlotHandle const& Handle) { return Handle.HaveBuilding(); }

	UFUNCTION(BlueprintCallable)
	static FEnttBuildingTypeHandle GetBuildingTypeHandle(FEnttColonyBuildingSlotHandle const& Handle)
	{
		return Handle.GetBuildingTypeHandle();
	}

	UFUNCTION(BlueprintPure, meta = (CompactNodeTitle = "=="))
	static bool Equal(FEnttColonyBuildingSlotHandle const& A, FEnttColonyBuildingSlotHandle const& B) { return A.Handle == B.Handle && A.Idx == B.Idx; }

	UFUNCTION(BlueprintCallable)
	static TArray<FEnttBuildingTypeHandle> GetPossibleBuildingTypes(FEnttColonyBuildingSlotHandle const& Handle)
	{
		return Handle.GetPossibleBuildingTypes();
	}
};