// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/Misc/FRegistry.h"
#include "Optigame/OptigamePch.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FEnttBuildingTypeHandle.generated.h"

USTRUCT(BlueprintType)
struct FEnttBuildingTypeHandle : public FEnttHandle
{
	GENERATED_BODY()
public:
	FEnttBuildingTypeHandle();

	// Reg can be nullptr
	FEnttBuildingTypeHandle(FRegistry& Reg, FEntity Entity);
	explicit FEnttBuildingTypeHandle(FEnttHandle Handle);

	~FEnttBuildingTypeHandle();

	// Extend Functions

	UTexture2D* GetTexture() const;
	FText		GetName() const;
	FText		GetInfo() const;

	bool IsValid() const;
};

UCLASS()
class UFEnttBuildingTypeHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttBuildingTypeHandle const& Handle) { return Handle.IsValid(); }
	UFUNCTION(BlueprintCallable)
	static UTexture2D* GetTexture(FEnttBuildingTypeHandle const& Handle) { return Handle.GetTexture(); }
	UFUNCTION(BlueprintCallable)
	static FText GetName(FEnttBuildingTypeHandle const& Handle) { return Handle.GetName(); }
	UFUNCTION(BlueprintCallable)
	static FText GetInfo(FEnttBuildingTypeHandle const& Handle) { return Handle.GetInfo(); }
};