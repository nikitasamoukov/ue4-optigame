// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FEnttModuleTypeHandle.generated.h"

USTRUCT(BlueprintType)
struct FEnttModuleTypeHandle : public FEnttHandle
{
	GENERATED_BODY()
public:
	FEnttModuleTypeHandle();

	// Reg can be nullptr
	FEnttModuleTypeHandle(FRegistry& Reg, FEntity Entity);
	explicit FEnttModuleTypeHandle(FEnttHandle Handle);

	~FEnttModuleTypeHandle();

	// Extend Functions

	bool IsValid() const;
};

UCLASS()
class UFEnttModuleTypeHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttModuleTypeHandle const& Handle) { return Handle.IsValid(); }
	UFUNCTION(BlueprintCallable)
	static FEnttModuleTypeHandle GetModuleTypeFromId(FName Id);
};