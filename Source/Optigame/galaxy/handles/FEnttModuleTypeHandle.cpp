// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttModuleTypeHandle.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"

FEnttModuleTypeHandle::FEnttModuleTypeHandle()
	: FEnttHandle()
{
}

FEnttModuleTypeHandle::FEnttModuleTypeHandle(FRegistry& Reg, FEntity Entity)
	: FEnttHandle(Reg, Entity)
{
}

FEnttModuleTypeHandle::FEnttModuleTypeHandle(FEnttHandle Handle)
	: FEnttHandle(Handle)
{
}

bool FEnttModuleTypeHandle::IsValid() const
{
	if (!FEnttHandle::IsValid())
		return false;

	return has<FCompShipModule>();
}

FEnttModuleTypeHandle::~FEnttModuleTypeHandle()
{
}

FEnttModuleTypeHandle UFEnttModuleTypeHandleLibrary::GetModuleTypeFromId(FName Id)
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto Handle = OptigameData->BreedRegistry.GetBreed(Id);
	return { *Handle.registry(), Handle.entity() };
}