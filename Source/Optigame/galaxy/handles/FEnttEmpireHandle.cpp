// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttEmpireHandle.h"

#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Components/ComponentsEmpire.h"
#include "Optigame/Galaxy/BreedRegistry/LBreedRegistry.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"

FEnttEmpireHandle::FEnttEmpireHandle()
	: FEnttHandle()
{
}

FEnttEmpireHandle::FEnttEmpireHandle(FRegistry& Reg, FEntity Entity)
	: FEnttHandle(Reg, Entity)
{
}

FEnttEmpireHandle::FEnttEmpireHandle(FEnttHandle Handle)
	: FEnttHandle(Handle)
{
}

FEnttEmpireHandle::~FEnttEmpireHandle()
{
}

bool FEnttEmpireHandle::IsValid() const
{
	if (!FEnttHandle::IsValid())
		return false;

	return has<FCompEmpire>();
}

double FEnttEmpireHandle::GetResourceCount(FName ResId) const
{
	if (!IsValid())
		return -1;
	if (!get<FCompEmpire>().Resources.Contains(ResId))
	{
		LOG("NoRes:" + ResId.ToString());
		return -1;
	}
	return get<FCompEmpire>().Resources[ResId];
}

double FEnttEmpireHandle::GetResourceChangeCount(FName ResId) const
{
	if (!IsValid())
		return 0;
	auto& Empire = get<FCompEmpire>();
	if (!Empire.ResourcesChange.Contains(ResId))
	{
		LOG("NoRes:" + ResId.ToString());
		return 0;
	}
	return get<FCompEmpire>().ResourcesChange[ResId];
}

UTexture2D* FEnttEmpireHandle::GetIconTexture() const
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	if (!IsValid())
		return nullptr;
	auto TexPtr = OptigameData->EmpireIcons.IconsMap.Find(get<FCompEmpire>().Icon.Texture);
	if (!TexPtr)
		return nullptr;
	return *TexPtr;
}

FColor FEnttEmpireHandle::GetIconColorMain() const
{
	if (!IsValid())
		return {};
	return get<FCompEmpire>().Icon.ColorMain;
}

FColor FEnttEmpireHandle::GetIconColorSecond() const
{
	if (!IsValid())
		return {};
	return get<FCompEmpire>().Icon.ColorSecond;
}

FEnttShipTemplateHandle FEnttEmpireHandle::GetShipTemplateEditing() const
{
	if (!IsValid())
		return {};
	return FEnttShipTemplateHandle{ *registry(), get<FCompEmpire>().ShipTemplateEditing };
}

TArray<FEnttShipTemplateHandle> FEnttEmpireHandle::GetShipTemplatesHandles() const
{
	if (!IsValid())
		return {};
	TArray<FEnttShipTemplateHandle> Res;

	for (auto E : get<FCompEmpire>().ShipTemplates)
		Res.Add({ *registry(), E });

	return Res;
}

TArray<FShipTemplateModule> FEnttEmpireHandle::GetPossibleShipModuleTypes() const
{
	if (!IsValid())
		return {};
	TArray<FShipTemplateModule> Res;

	auto& BreedRegistry = *registry()->ctx().get<LBreedRegistry*>();
	for (auto& Pair : BreedRegistry.GetBreeds(LBreedRegistry::EBreedType::ShipModule))
	{
		auto C = Pair.Value.try_get<FCompShipModule>();
		if (!C)
			continue;
		if (C->Type == EShipModuleType::Shipyard)
			continue;
		if (C->Type == EShipModuleType::HullStation)
			continue;

		auto&				CBreedId = Pair.Value.get<FCompBreedId>();
		FShipTemplateModule E;

		int TierCount = 1;
		if (C->Type != EShipModuleType::Hull)
			TierCount = 3;

		for (int i = 1; i <= TierCount; i++)
		{
			E.BreedId = CBreedId.Id;
			E.Tier = i;
			Res.Add(E);
		}
	}

	return Res;
}

TArray<FEnttModuleTypeHandle> FEnttEmpireHandle::GetPossibleStationModuleTypes() const
{
	if (!IsValid())
		return {};
	TArray<FEnttModuleTypeHandle> Res;

	auto& BreedRegistry = *registry()->ctx().get<LBreedRegistry*>();
	for (auto& Pair : BreedRegistry.GetBreeds(LBreedRegistry::EBreedType::ShipModule))
	{
		auto C = Pair.Value.try_get<FCompShipModule>();
		if (!C)
			continue;
		if (C->Type == EShipModuleType::Engine)
			continue;
		if (C->Type == EShipModuleType::Hull)
			continue;
		if (C->Type == EShipModuleType::Reactor)
			continue;
		if (C->Type == EShipModuleType::HullStation)
			continue;

		Res.Add({ *Pair.Value.registry(), Pair.Value.entity() });
	}

	return Res;
}