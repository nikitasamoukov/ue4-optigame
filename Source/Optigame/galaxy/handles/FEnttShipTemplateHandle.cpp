// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttShipTemplateHandle.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"

FEnttShipTemplateHandle::FEnttShipTemplateHandle()
	: FEnttHandle()
{
}

FEnttShipTemplateHandle::FEnttShipTemplateHandle(FRegistry& Reg, FEntity Entity)
	: FEnttHandle(Reg, Entity)
{
}

FEnttShipTemplateHandle::FEnttShipTemplateHandle(FEnttHandle Handle)
	: FEnttHandle(Handle)
{
}

bool FEnttShipTemplateHandle::IsValid() const
{
	if (!FEnttHandle::IsValid())
		return false;

	return has<FCompShipTemplate>();
}

FEnttShipTemplateHandle::~FEnttShipTemplateHandle()
{
}