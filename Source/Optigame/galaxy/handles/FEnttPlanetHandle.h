// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FEnttEmpireHandle.h"
#include "FEnttColonyHandle.h"
#include "FEnttPlanetHandle.generated.h"

UENUM(BlueprintType)
enum class EPlanetType : uint8
{
	Unknown,
	Earth,
	Rock,
	Ice,
	Lava,
	Desert,
	Water,
};

USTRUCT(BlueprintType)
struct FEnttPlanetHandle : public FEnttHandle
{
	GENERATED_BODY()
public:
	FEnttPlanetHandle();
	FEnttPlanetHandle(FRegistry& Reg, FEntity Entity);
	explicit FEnttPlanetHandle(FEnttHandle Handle);

	~FEnttPlanetHandle();

	// Extend Functions

	bool IsValid() const;

	FVector GetPosition() const;

	EPlanetType GetType() const;

	FString GetName() const;
	void	SetName(FString const& Name) const;

	FEnttEmpireHandle GetOwner() const;

	bool			  HasColony() const;
	FEnttColonyHandle GetColony() const;
};

UCLASS()
class UFEnttPlanetHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttPlanetHandle const& Handle) { return Handle.IsValid(); }
	UFUNCTION(BlueprintCallable)
	static EPlanetType GetType(FEnttPlanetHandle const& Handle) { return Handle.GetType(); }
	UFUNCTION(BlueprintCallable)
	static FString GetName(FEnttPlanetHandle const& Handle) { return Handle.GetName(); }
	UFUNCTION(BlueprintCallable)
	static void SetName(FEnttPlanetHandle const& Handle, FString const& Name) { Handle.SetName(Name); }
	UFUNCTION(BlueprintCallable)
	static FEnttEmpireHandle GetOwner(FEnttPlanetHandle const& Handle) { return Handle.GetOwner(); }
	UFUNCTION(BlueprintCallable)
	static bool HasColony(FEnttPlanetHandle const& Handle) { return Handle.HasColony(); }
	UFUNCTION(BlueprintCallable)
	static FEnttColonyHandle GetColony(FEnttPlanetHandle const& Handle);
};