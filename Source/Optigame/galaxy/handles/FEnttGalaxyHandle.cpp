// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttGalaxyHandle.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/galaxy/FGalaxy.h"

FEnttGalaxyHandle::FEnttGalaxyHandle()
{
}

FEnttGalaxyHandle::FEnttGalaxyHandle(FGalaxy* Galaxy)
	: Reg(&Galaxy->Reg), Galaxy(Galaxy)
{
}

FEnttGalaxyHandle::~FEnttGalaxyHandle()
{
}

bool FEnttGalaxyHandle::IsValid() const
{
	return Reg != nullptr;
}

FEnttEmpireHandle FEnttGalaxyHandle::GetPlayerEmpire() const
{
	if (!Reg || !Reg->ctx().contains<FCtxPlayerInfo>())
	{
		LOG();
		return {};
	}

	return { *Reg, Reg->ctx().get<FCtxPlayerInfo>().Empire };
}

void FEnttGalaxyHandle::SetTimeSpeed(double TimeSpeed) const
{
	Galaxy->TimeSpeed = TimeSpeed;
}
