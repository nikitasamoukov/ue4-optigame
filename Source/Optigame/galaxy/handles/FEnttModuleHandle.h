// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FEnttModuleHandle.generated.h"

USTRUCT(BlueprintType)
struct FEnttModuleHandle : public FEnttHandle
{
	GENERATED_BODY()
public:
	FEnttModuleHandle();

	// Reg can be nullptr
	FEnttModuleHandle(FRegistry& Reg, FEntity Entity);
	explicit FEnttModuleHandle(FEnttHandle Handle);

	~FEnttModuleHandle();

	// Extend Functions

	FText GetName() const;
	bool  GetLocal() const;
	bool  GetAdditive() const;

	bool IsValid() const;
};

UCLASS()
class UFEnttModuleHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttModuleHandle const& Handle) { return Handle.IsValid(); }
	UFUNCTION(BlueprintCallable)
	static FText GetName(FEnttModuleHandle const& Handle) { return Handle.GetName(); }
	UFUNCTION(BlueprintCallable)
	static bool GetLocal(FEnttModuleHandle const& Handle) { return Handle.GetLocal(); }
	UFUNCTION(BlueprintCallable)
	static bool GetAdditive(FEnttModuleHandle const& Handle) { return Handle.GetAdditive(); }
	UFUNCTION(BlueprintCallable)
	static FEnttModuleHandle GetResourceTypeFromId(FName Id);
};