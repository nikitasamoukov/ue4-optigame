// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttStationHandle.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "Optigame/Galaxy/Helpers/HelperStation.h"

FEnttStationHandle::FEnttStationHandle()
	: FEnttHandle()
{
}

FEnttStationHandle::FEnttStationHandle(FRegistry& Reg, FEntity Entity)
	: FEnttHandle(Reg, Entity)
{
}

FEnttStationHandle::FEnttStationHandle(FEnttHandle Handle)
	: FEnttHandle(Handle)
{
}

bool FEnttStationHandle::IsValid() const
{
	if (!FEnttHandle::IsValid())
		return false;

	return has<FCompShipTemplate, FCompStation, FCompTransform, FCompSpaceObject>();
}

void FEnttStationHandle::RemoveModule(int Idx) const
{
	if (!FEnttHandle::IsValid())
		return;

	auto& Modules = get<FCompShipTemplate>().Modules;

	if (Idx >= 0 && Idx < Modules.Num())
		Modules.RemoveAt(Idx);

	UHelperStation::RefreshStats(*this);
}

FName FEnttStationHandle::GetHullMeshId() const
{
	if (!FEnttHandle::IsValid())
		return {};

	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto& BreedRegistry = OptigameData->BreedRegistry;

	auto& CShipTemplate = get<FCompShipTemplate>();
	auto  Breed = BreedRegistry.GetBreed(CShipTemplate.Hull.BreedId);
	auto  CMesh = Breed.try_get<FCompMesh>();
	if (!CMesh)
		return {};

	return CMesh->Name;
}

FEnttStationHandle::~FEnttStationHandle()
{
}