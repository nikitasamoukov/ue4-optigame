// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "Optigame/Galaxy/Components/ComponentsBuildQueue.h"
#include "RegComponentsLibrary.generated.h"

#define COMP_DEF(C)                                                                       \
	U##FUNCTION(BlueprintCallable) static bool HasComp##C(FEnttHandle Handle)             \
	{                                                                                     \
		if (!Handle)                                                                      \
			return false;                                                                 \
		return Handle.has<FC##C>();                                                       \
	}                                                                                     \
	U##FUNCTION(BlueprintCallable) static FC##C GetComp##C(FEnttHandle Handle)            \
	{                                                                                     \
		return !!Handle && Handle.has<FC##C>() ? Handle.get<FC##C>() : FC##C();           \
	}                                                                                     \
	U##FUNCTION(BlueprintCallable) static void SetComp##C(FEnttHandle Handle, FC##C Comp) \
	{                                                                                     \
		if (Handle.registry())                                                            \
			Handle.emplace_or_replace<FC##C>(Comp);                                       \
	}

UCLASS()
class URegComponentsLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool HasCompSpaceObject(FEnttHandle Handle)
	{
		if (!Handle)
			return false;
		return Handle.has<FCompSpaceObject>();
	}
	UFUNCTION(BlueprintCallable)
	static FCompSpaceObject GetCompSpaceObject(FEnttHandle Handle) { return !!Handle && Handle.has<FCompSpaceObject>() ? Handle.get<FCompSpaceObject>() : FCompSpaceObject(); }
	UFUNCTION(BlueprintCallable)
	static void SetCompSpaceObject(FEnttHandle Handle, FCompSpaceObject Comp)
	{
		if (Handle.registry())
			Handle.emplace_or_replace<FCompSpaceObject>(Comp);
	}
	UFUNCTION(BlueprintCallable)
	static bool HasCompShipTemplate(FEnttHandle Handle)
	{
		if (!Handle)
			return false;
		return Handle.has<FCompShipTemplate>();
	}
	UFUNCTION(BlueprintCallable)
	static FCompShipTemplate GetCompShipTemplate(FEnttHandle Handle) { return !!Handle && Handle.has<FCompShipTemplate>() ? Handle.get<FCompShipTemplate>() : FCompShipTemplate(); }
	UFUNCTION(BlueprintCallable)
	static void SetCompShipTemplate(FEnttHandle Handle, FCompShipTemplate Comp)
	{
		if (Handle.registry())
			Handle.emplace_or_replace<FCompShipTemplate>(Comp);
	}

	UFUNCTION(BlueprintCallable)
	static bool HasCompShip(FEnttHandle Handle)
	{
		if (!Handle)
			return false;
		return Handle.has<FCompShip>();
	}
	UFUNCTION(BlueprintCallable)
	static FCompShip GetCompShip(FEnttHandle Handle) { return !!Handle && Handle.has<FCompShip>() ? Handle.get<FCompShip>() : FCompShip(); }
	UFUNCTION(BlueprintCallable)
	static void SetCompShip(FEnttHandle Handle, FCompShip Comp)
	{
		if (Handle.registry())
			Handle.emplace_or_replace<FCompShip>(Comp);
	}
	UFUNCTION(BlueprintCallable)
	static bool HasCompShipModule(FEnttHandle Handle)
	{
		if (!Handle)
			return false;
		return Handle.has<FCompShipModule>();
	}
	UFUNCTION(BlueprintCallable)
	static FCompShipModule GetCompShipModule(FEnttHandle Handle) { return !!Handle && Handle.has<FCompShipModule>() ? Handle.get<FCompShipModule>() : FCompShipModule(); }
	UFUNCTION(BlueprintCallable)
	static void SetCompShipModule(FEnttHandle Handle, FCompShipModule Comp)
	{
		if (Handle.registry())
			Handle.emplace_or_replace<FCompShipModule>(Comp);
	}
	UFUNCTION(BlueprintCallable)
	static bool HasCompShipModuleHull(FEnttHandle Handle)
	{
		if (!Handle)
			return false;
		return Handle.has<FCompShipModuleHull>();
	}
	UFUNCTION(BlueprintCallable)
	static FCompShipModuleHull GetCompShipModuleHull(FEnttHandle Handle) { return !!Handle && Handle.has<FCompShipModuleHull>() ? Handle.get<FCompShipModuleHull>() : FCompShipModuleHull(); }
	UFUNCTION(BlueprintCallable)
	static void SetCompShipModuleHull(FEnttHandle Handle, FCompShipModuleHull Comp)
	{
		if (Handle.registry())
			Handle.emplace_or_replace<FCompShipModuleHull>(Comp);
	}

	UFUNCTION(BlueprintCallable)
	static bool HasCompBreedId(FEnttHandle Handle)
	{
		if (!Handle)
			return false;
		return Handle.has<FCompBreedId>();
	}
	UFUNCTION(BlueprintCallable)
	static FCompBreedId GetCompBreedId(FEnttHandle Handle) { return !!Handle && Handle.has<FCompBreedId>() ? Handle.get<FCompBreedId>() : FCompBreedId(); }
	UFUNCTION(BlueprintCallable)
	static void SetCompBreedId(FEnttHandle Handle, FCompBreedId Comp)
	{
		if (Handle.registry())
			Handle.emplace_or_replace<FCompBreedId>(Comp);
	}

	UFUNCTION(BlueprintCallable)
	static bool HasCompBuildQueue(FEnttHandle Handle)
	{
		if (!Handle)
			return false;
		return Handle.has<FCompBuildQueue>();
	}
	UFUNCTION(BlueprintCallable)
	static FCompBuildQueue GetCompBuildQueue(FEnttHandle Handle) { return !!Handle && Handle.has<FCompBuildQueue>() ? Handle.get<FCompBuildQueue>() : FCompBuildQueue(); }
	UFUNCTION(BlueprintCallable)
	static void SetCompBuildQueue(FEnttHandle Handle, FCompBuildQueue Comp)
	{
		if (Handle.registry())
			Handle.emplace_or_replace<FCompBuildQueue>(Comp);
	}

	/*
	COMP_DEF(SpaceObject)
	COMP_DEF(ShipTemplate)

	COMP_DEF(Ship)
	COMP_DEF(ShipModule)
	COMP_DEF(ShipModuleHull)

	COMP_DEF(BreedId)

	COMP_DEF(BuildQueue)
	 */

	// COMP_DEF(C)
};

////ENTT Integration
//
// USTRUCT(BlueprintType)
// struct FRegistryHandle
//{
//	GENERATED_BODY()
// public:
//	FRegistry* Reg = nullptr;
//};
//
// USTRUCT(BlueprintType)
// struct FEnttEntity
//{
//	GENERATED_BODY()
// public:
//	FEntity Entity = entt::null;
//};
//
//
//
//
// USTRUCT(BlueprintType)
// struct FCompHp
//{
//	GENERATED_BODY()
// public:
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Val")
//		double Hp;
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Val")
//		double MaxHp;
//};
//// This work
////UCLASS()
////class UFCompHpRegLibrary : public UBlueprintFunctionLibrary
////{
////	GENERATED_BODY()
////public:
////	UFUNCTION(BlueprintCallable)
////		static FCompHp GetCompHp(FRegistryHandle RegistryHandle, FEnttEntity Entity) { return RegistryHandle.Reg ? RegistryHandle.Reg->get<FCompHp>(Entity.Entity) : FCompHp(); }
////	UFUNCTION(BlueprintCallable)
////		static void SetCompHp(FRegistryHandle RegistryHandle, FEnttEntity Entity, FCompHp Comp) { if (RegistryHandle.Reg) RegistryHandle.Reg->emplace_or_replace<FCompHp>(Entity.Entity, Comp); }
////};
//
////// UHT reject
////#define COMP_DEF(C) UCLASS() \
////class UFComp ## C ## RegLibrary : public UBlueprintFunctionLibrary \
////{
////
////#define COMP_DEF_END(C) public:\
////	UFUNCTION(BlueprintCallable)\
////		static FComp ## C GetComp ## C(FRegistryHandle RegistryHandle, FEnttEntity Entity) { return RegistryHandle.Reg ? RegistryHandle.Reg->get<FComp ## C>(Entity.Entity) : FComp ## C(); }\
////	UFUNCTION(BlueprintCallable)\
////		static void SetComp ## C(FRegistryHandle RegistryHandle, FEnttEntity Entity, FComp ## C Comp) { if (RegistryHandle.Reg) RegistryHandle.Reg->emplace_or_replace<FComp ## C>(Entity.Entity, Comp); }\
////
////
////UCLASS() class UFCompHpRegLibrary : public UBlueprintFunctionLibrary { GENERATED_BODY() COMP_DEF_END(Hp) };
//
////UCLASS() class UFCompRegLibrary : public UBlueprintFunctionLibrary {
////GENERATED_BODY()
////	UFUNCTION(BlueprintCallable)
////		static void GetComp(FRegistryHandle RegistryHandle, FEnttEntity Entity,TChooseClass CompType)
////	{
////		//CompType
////		//return RegistryHandle.Reg ? RegistryHandle.Reg->get<FComp ## C>(Entity.Entity) : FComp ## C();
////	}
////};
//
// UCLASS() class UFCompRegLibrary : public UBlueprintFunctionLibrary
//{
//	GENERATED_BODY()
// public:
//	// Example Blueprint function that receives any struct as input
//	UFUNCTION(BlueprintCallable, Category = "Example", CustomThunk, meta = (CustomStructureParam = "AnyStruct"))
//		static void ReceiveSomeStruct(UStruct* AnyStruct);
//
//	DECLARE_FUNCTION(execReceiveSomeStruct)
//	{
//		// Steps into the stack, walking to the next property in it
//		Stack.Step(Stack.Object, NULL);
//
//		// Grab the last property found when we walked the stack
//		// This does not contains the property value, only its type information
//		FProperty* StructProperty = CastField<FProperty>(Stack.MostRecentProperty);
//
//		// Grab the base address where the struct actually stores its data
//		// This is where the property value is truly stored
//		void* StructPtr = Stack.MostRecentPropertyAddress;
//
//		// We need this to wrap up the stack
//		P_FINISH;
//
//		// Iterate through the struct
//		IterateThroughStructProperty(StructProperty, StructPtr);
//	}
//
// private:
//	/*
//	* Example function for iterating through all properties of a struct
//	* param StructProperty The struct property reflection data
//	* param StructPtr The pointer to the struct value
//	*/
//	static void IterateThroughStructProperty(FProperty* StructProperty, void* StructPtr)
//	{
//		LOG(StructProperty->GetCPPType());
//
//		LOG(((FEnttEmpireHandle*)StructPtr)->GetIconColorMain().ToHex());
//		// empty body
//	};
//
//};