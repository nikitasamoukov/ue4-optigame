// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttModuleHandle.h"
#include "Optigame/OptigameData.h"
#include "Optigame/common/common.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"

FEnttModuleHandle::FEnttModuleHandle()
	: FEnttHandle()
{
}

FEnttModuleHandle::FEnttModuleHandle(FRegistry& Reg, FEntity Entity)
	: FEnttHandle(Reg, Entity)
{
}

FEnttModuleHandle::FEnttModuleHandle(FEnttHandle Handle)
	: FEnttHandle(Handle)
{
}

bool FEnttModuleHandle::IsValid() const
{
	if (!FEnttHandle::IsValid())
		return false;

	return has<FCompResourceType>();
}

FEnttModuleHandle::~FEnttModuleHandle()
{
}

FText FEnttModuleHandle::GetName() const
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	if (!IsValid())
		return FText::FromString("BuildingTypeHandle invalid");
	auto Id = get<FCompResourceType>().NameId;
	return OptigameData->GetFTextFromId(Id);
}

bool FEnttModuleHandle::GetLocal() const
{
	if (!IsValid())
		return false;
	return get<FCompResourceType>().Local;
}

bool FEnttModuleHandle::GetAdditive() const
{
	if (!IsValid())
		return false;
	return get<FCompResourceType>().Additive;
}

FEnttModuleHandle UFEnttModuleHandleLibrary::GetResourceTypeFromId(FName Id)
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto Handle = OptigameData->BreedRegistry.GetBreed(Id);
	return { *Handle.registry(), Handle.entity() };
}