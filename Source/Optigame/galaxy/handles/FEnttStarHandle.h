// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "FEnttEmpireHandle.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FEnttStarHandle.generated.h"

USTRUCT(BlueprintType)
struct FEnttStarHandle : public FEnttHandle
{
	GENERATED_BODY()
public:
	FEnttStarHandle();
	FEnttStarHandle(FRegistry& Reg, FEntity Entity);
	explicit FEnttStarHandle(FEnttHandle Handle);

	~FEnttStarHandle();

	// Extend Functions

	bool IsValid() const;

	FVector					GetPosition();
	TArray<FEnttStarHandle> GetPositiveConnections();
	TArray<FEnttStarHandle> GetConnections();
	FString					GetName() const;

	TArray<FEnttEmpireHandle> GetEmpiresList() const;
};

template <>
struct TStructOpsTypeTraits<FEnttStarHandle> : public TStructOpsTypeTraitsBase2<FEnttStarHandle>
{
	enum
	{
		WithIdenticalViaEquality = true,
	};
};

UCLASS()
class UFEnttStarHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttStarHandle const& Handle) { return Handle.IsValid(); }
	UFUNCTION(BlueprintCallable)
	static TArray<FEnttEmpireHandle> GetEmpiresList(FEnttStarHandle const& Handle) { return Handle.GetEmpiresList(); }
	UFUNCTION(BlueprintCallable)
	static FString GetName(FEnttStarHandle const& Handle) { return Handle.GetName(); }
};