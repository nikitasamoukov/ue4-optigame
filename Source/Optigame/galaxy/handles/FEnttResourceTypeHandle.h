// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FEnttResourceTypeHandle.generated.h"

USTRUCT(BlueprintType)
struct FEnttResourceTypeHandle : public FEnttHandle
{
	GENERATED_BODY()
public:
	FEnttResourceTypeHandle();

	// Reg can be nullptr
	FEnttResourceTypeHandle(FRegistry& Reg, FEntity Entity);
	explicit FEnttResourceTypeHandle(FEnttHandle Handle);

	~FEnttResourceTypeHandle();

	// Extend Functions

	FText GetName() const;
	bool  GetLocal() const;
	bool  GetAdditive() const;

	bool IsValid() const;
};

UCLASS()
class UFEnttResourceTypeHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttResourceTypeHandle const& Handle) { return Handle.IsValid(); }
	UFUNCTION(BlueprintCallable)
	static FText GetName(FEnttResourceTypeHandle const& Handle) { return Handle.GetName(); }
	UFUNCTION(BlueprintCallable)
	static bool GetLocal(FEnttResourceTypeHandle const& Handle) { return Handle.GetLocal(); }
	UFUNCTION(BlueprintCallable)
	static bool GetAdditive(FEnttResourceTypeHandle const& Handle) { return Handle.GetAdditive(); }
	UFUNCTION(BlueprintCallable)
	static FEnttResourceTypeHandle GetResourceTypeFromId(FName Id);
};