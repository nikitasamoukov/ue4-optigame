// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttStarHandle.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"

FEnttStarHandle::FEnttStarHandle()
	: FEnttHandle()
{
}

FEnttStarHandle::FEnttStarHandle(FRegistry& Reg, FEntity Entity)
	: FEnttHandle(Reg, Entity)
{
}

FEnttStarHandle::FEnttStarHandle(FEnttHandle Handle)
	: FEnttHandle(Handle)
{
}

bool FEnttStarHandle::IsValid() const
{
	if (!FEnttHandle::IsValid())
		return false;

	return all_of<FCompStar>();
}

FVector FEnttStarHandle::GetPosition()
{
	if (!IsValid() || !all_of<FCompCurrentTimePos>())
		return FVector::ZeroVector;
	return get<FCompCurrentTimePos>().Pos;
}

TArray<FEnttStarHandle> FEnttStarHandle::GetPositiveConnections()
{
	if (!IsValid())
		return {};

	TArray<FEnttStarHandle> Res;

	for (auto& Connect : get<FCompStar>().Connects)
	{
		if (entity() > Connect.Star)
		{
			FEnttStarHandle Handle(*registry(), Connect.Star);
			if (Handle.IsValid())
				Res.Add(Handle);
		}
	}

	return Res;
}

TArray<FEnttStarHandle> FEnttStarHandle::GetConnections()
{
	if (!IsValid())
		return {};

	TArray<FEnttStarHandle> Res;

	for (auto& Connect : get<FCompStar>().Connects)
	{
		FEnttStarHandle Handle(*registry(), Connect.Star);
		if (Handle.IsValid())
			Res.Add(Handle);
	}

	return Res;
}

FString FEnttStarHandle::GetName() const
{
	if (!IsValid())
		return "<Error>";
	return get<FCompStar>().Name;
}

TArray<FEnttEmpireHandle> FEnttStarHandle::GetEmpiresList() const
{
	if (!IsValid())
		return {};

	TArray<FEnttEmpireHandle> Res;

	for (auto Object : get<FCompStar>().Objects)
	{
		if (registry()->all_of<FCompOwner>(Object))
		{
			FEnttEmpireHandle H = { *registry(), registry()->get<FCompOwner>(Object).Empire };
			if (!Res.Contains(H))
				Res.Add(H);
		}
	}

	return Res;
}

FEnttStarHandle::~FEnttStarHandle()
{
}