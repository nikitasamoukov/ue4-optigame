// Fill out your copyright notice in the Description page of Project Settings.

#include "FEnttResourceTypeHandle.h"
#include "Optigame/OptigameData.h"
#include "Optigame/common/common.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"

FEnttResourceTypeHandle::FEnttResourceTypeHandle()
	: FEnttHandle()
{
}

FEnttResourceTypeHandle::FEnttResourceTypeHandle(FRegistry& Reg, FEntity Entity)
	: FEnttHandle(Reg, Entity)
{
}

FEnttResourceTypeHandle::FEnttResourceTypeHandle(FEnttHandle Handle)
	: FEnttHandle(Handle)
{
}

bool FEnttResourceTypeHandle::IsValid() const
{
	if (!FEnttHandle::IsValid())
		return false;

	return has<FCompResourceType>();
}

FEnttResourceTypeHandle::~FEnttResourceTypeHandle()
{
}

FText FEnttResourceTypeHandle::GetName() const
{
	if (!IsValid())
		return FText::FromString("BuildingTypeHandle invalid");
	auto Id = get<FCompResourceType>().NameId;
	return UOptigameProjectSettings::GetOptigameData()->GetFTextFromId(Id);
}

bool FEnttResourceTypeHandle::GetLocal() const
{
	if (!IsValid())
		return false;
	return get<FCompResourceType>().Local;
}

bool FEnttResourceTypeHandle::GetAdditive() const
{
	if (!IsValid())
		return false;
	return get<FCompResourceType>().Additive;
}

FEnttResourceTypeHandle UFEnttResourceTypeHandleLibrary::GetResourceTypeFromId(FName Id)
{
	auto Handle = UOptigameProjectSettings::GetOptigameData()->BreedRegistry.GetBreed(Id);
	return { *Handle.registry(), Handle.entity() };
}