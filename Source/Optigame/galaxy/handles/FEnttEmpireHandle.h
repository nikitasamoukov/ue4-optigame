// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "FEnttShipTemplateHandle.h"
#include "FEnttModuleTypeHandle.h"

#include "Optigame/Galaxy/Components/ComponentsShip.h"

#include "FEnttEmpireHandle.generated.h"

USTRUCT(BlueprintType)
struct FEnttEmpireHandle : public FEnttHandle
{
	GENERATED_BODY()
public:
	FEnttEmpireHandle();
	FEnttEmpireHandle(FRegistry& Reg, FEntity Entity);
	explicit FEnttEmpireHandle(FEnttHandle Handle);

	~FEnttEmpireHandle();

	// Extend Functions

	bool IsValid() const;

	double GetResourceCount(FName ResId) const;
	double GetResourceChangeCount(FName ResId) const;
	UTexture2D* GetIconTexture() const;
	FColor GetIconColorMain() const;
	FColor GetIconColorSecond() const;
	FEnttShipTemplateHandle GetShipTemplateEditing() const;
	TArray<FEnttShipTemplateHandle> GetShipTemplatesHandles() const;
	TArray<FShipTemplateModule> GetPossibleShipModuleTypes() const;
	TArray<FEnttModuleTypeHandle> GetPossibleStationModuleTypes() const;
};

template <>
struct TStructOpsTypeTraits<FEnttEmpireHandle> : public TStructOpsTypeTraitsBase2<FEnttEmpireHandle>
{
	enum
	{
		WithIdenticalViaEquality = true,
	};
};

UCLASS()
class UFEnttEmpireHandleLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static bool IsValid(FEnttEmpireHandle const& Handle) { return Handle.IsValid(); }
	UFUNCTION(BlueprintCallable)
	static double GetResourceCount(FEnttEmpireHandle const& Handle, FName ResId) { return Handle.GetResourceCount(ResId); }
	UFUNCTION(BlueprintCallable)
	static double GetResourceChangeCount(FEnttEmpireHandle const& Handle, FName ResId) { return Handle.GetResourceChangeCount(ResId); }
	UFUNCTION(BlueprintCallable)
	static UTexture2D* GetIconTexture(FEnttEmpireHandle const& Handle) { return Handle.GetIconTexture(); }
	UFUNCTION(BlueprintCallable)
	static FColor GetIconColorMain(FEnttEmpireHandle const& Handle) { return Handle.GetIconColorMain(); }
	UFUNCTION(BlueprintCallable)
	static FColor GetIconColorSecond(FEnttEmpireHandle const& Handle) { return Handle.GetIconColorSecond(); }
	UFUNCTION(BlueprintCallable)
	static FEnttShipTemplateHandle GetShipTemplateEditing(FEnttEmpireHandle const& Handle) { return Handle.GetShipTemplateEditing(); }
	UFUNCTION(BlueprintCallable)
	static TArray<FEnttShipTemplateHandle> GetShipTemplatesHandles(FEnttEmpireHandle const& Handle) { return Handle.GetShipTemplatesHandles(); }
	UFUNCTION(BlueprintCallable)
	static TArray<FShipTemplateModule> GetPossibleShipModuleTypes(FEnttEmpireHandle const& Handle) { return Handle.GetPossibleShipModuleTypes(); }
	UFUNCTION(BlueprintCallable)
	static TArray<FEnttModuleTypeHandle> GetPossibleStationModuleTypes(FEnttEmpireHandle const& Handle) { return Handle.GetPossibleStationModuleTypes(); }
};

double GetColorDist(FColor C1, FColor C2);

UCLASS()
class UMiscColorsExtLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static double CalcColorDist(FColor C1, FColor C2) { return GetColorDist(C1, C2); }
};