// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "Helpers/Pathfinder.h"
#include <Optigame/StartGame/NewGameSettings.h>

class AOptigameCore;
struct FCompOrbit;
class LBreedRegistry;

class FGalaxy
{
public:
	FGalaxy();
	~FGalaxy();

	void Init(AOptigameCore* OptigameCore);
	void Clear();

	void Generate(FNewGameSettings const& NewGameSettings);

	void Tick(float DeltaTime);
	void Update();

	FRegistry Reg;

	uint64_t CurrentFrame = 0;
	double ProgressToCurrentFrame = 1;
	double TimeSpeed = 1;

	int Months = 0;
	int Days = 0;
	int DayProgress = 0;

	FPathfinder Pathfinder;

protected:
	int CalculateDays();
};

/*
Galaxy is:

Star
	Planet
	Planet
	Asteroid belt

Star
	Asteroid belt
	Planet(Colony)
		Colony stats
		Building
		Building
		Building
		Building
		Building
		Building
		Building
		Building
		Building
		Building
	Asteroid belt
	Planet

Ship
Ship
Ship
Ship
Ship
Ship
Ship
Ship
Ship
Ship
Ship
Ship
Ship
Ship
Ship
Ship
Ship

Star to star connect have interlink
Connection (A, B) is positive when A.entity > B.entity
A.entity == B.entity illegal
Connection (A, B) is negative when A.entity < B.entity


FCompStar <- APrStar <- BP_PrStar


*/