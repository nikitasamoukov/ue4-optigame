// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

class LComponentBreedsFactory;
class FXmlNode;
class UObjectsDataAsset;
struct FObjectData;

class LBreedRegistry
{
public:
	LBreedRegistry();
	~LBreedRegistry();

	enum class EBreedType
	{
		Building,
		Resource,
		EmpireSpawn,
		ShipModule,
		Unknown,
	};

	void LoadBreed(UObjectsDataAsset* ObjectsDataAsset, EBreedType BreedType);
	void LoadBreed(UObjectsDataAsset* ObjectsDataAsset, EBreedType BreedType, TFunction<void(FEnttHandle Handle)> const& Finalize);

	// Return template. DANGER ITS ENTITY IN OTHER REGISTRY.
	FEnttHandle Copy(FName Id, FEnttHandle Handle);

	// Return template. DANGER ITS ENTITY IN OTHER REGISTRY.
	FEnttHandle GetBreed(FName Name);

	// Return templates. DANGER ITS ENTITY IN OTHER REGISTRY.
	TArray<TPair<FName, FEnttHandle>> const& GetBreeds(EBreedType BreedType);

	auto& GetReg() const { return Reg; }

	auto& _GetNamesMap() const { return IdMap; }

protected:
	TUniquePtr<LComponentBreedsFactory> Loader;

	FRegistry											Reg;
	TMap<FName, FEntity>								IdMap;
	TMap<EBreedType, TArray<TPair<FName, FEnttHandle>>> BreedsPerType;

	FEnttHandle LoadBreed(FObjectData const& ObjectData, TFunction<void(FEnttHandle Handle)> const& Finalize);
};