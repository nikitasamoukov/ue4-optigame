// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "Optigame/Galaxy/Components/FFactoryComponents.h"
#include <Optigame\DataAssets\ObjectsDataAsset.h>

class FXmlNode;

class LComponentBreedsFactory
{
public:
	void Load(FEnttHandle Handle, FInstancedStruct const& Data);
	void AddId(FEnttHandle Handle, FName Id);
	void Copy(FEnttHandle BreedHandle, FEnttHandle EntityHandle);

	FFactoryComponents FactoryComponents;
};