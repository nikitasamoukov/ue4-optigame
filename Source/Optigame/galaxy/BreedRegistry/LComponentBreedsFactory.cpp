// Fill out your copyright notice in the Description page of Project Settings.

#include "LComponentBreedsFactory.h"
#include "Optigame/Common/common.h"
#include "Optigame/Galaxy/Components/ComponentsBuilding.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include <string>

void LComponentBreedsFactory::Load(FEnttHandle Handle, FInstancedStruct const& Data)
{
	FName ClassName = *Data.GetScriptStruct()->GetStructCPPName();
	auto  TypeCell = FactoryComponents.GetTypeCell(ClassName);
	if (!TypeCell || !TypeCell->FuncUnpack)
	{
		LOG("Cant load C: " + ClassName.ToString());
		return;
	}
	TypeCell->FuncUnpack(Handle, Data);
}

void LComponentBreedsFactory::AddId(FEnttHandle Handle, FName Id)
{
	Handle.emplace_or_replace<FCompBreedId>(Id);
}

TSet<entt::id_type> SetBreedNoCopyComp = {
	entt::type_id<FCompBreedId>().hash(),
	entt::type_id<FCompBuildingType>().hash()
};

void LComponentBreedsFactory::Copy(FEnttHandle BreedHandle, FEnttHandle EntityHandle)
{
	for (auto it : BreedHandle.storage())
	{
		entt::id_type Id = it.first;
		auto		  TypeCell = FactoryComponents.GetTypeCell(Id);
		if (!TypeCell)
		{
			LOG(("Bad comp:" + std::to_string(Id)).c_str());
			return;
		}

		if (SetBreedNoCopyComp.Contains(Id))
			continue;
		TypeCell->FuncCopy(BreedHandle, EntityHandle);
	}
}

FString ComponentNameStrip(FString const& Name)
{
	FString NewName = Name;
	NewName.RemoveFromStart("FComp");
	return NewName;
}