// Fill out your copyright notice in the Description page of Project Settings.

#include "LBreedRegistry.h"
#include "LComponentBreedsFactory.h"

#include "Engine/StaticMesh.h"
#include "Engine/StaticMeshSocket.h"

#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "Optigame/Galaxy/Components/ComponentsEmpire.h"
#include "Optigame/Galaxy/Helpers/HelperShipTemplate.h"

LBreedRegistry::LBreedRegistry()
{
}

LBreedRegistry::~LBreedRegistry()
{
}

FTransform SocketTrs(UStaticMeshSocket* Socket)
{
	return FTransform(Socket->RelativeRotation, Socket->RelativeLocation, Socket->RelativeScale);
}

void LoadFinalizer(FEnttHandle Handle)
{
	if (auto CShipModuleHull = Handle.try_get<FCompShipModuleHull>())
		if (auto CMesh = Handle.try_get<FCompMesh>())
		{
			auto OptigameData = UOptigameProjectSettings::GetOptigameData();
			if (!OptigameData->MeshesMap.Contains(CMesh->Name))
			{
				LOG("No mesh:" + CMesh->Name.ToString());
				return;
			}
			UStaticMesh* Mesh = OptigameData->MeshesMap[CMesh->Name];
			for (auto& Slot : CShipModuleHull->Weapons)
			{
				auto Socket = Mesh->FindSocket(Slot.SocketName);
				if (!Socket)
				{
					LOG("No socket:" + Slot.SocketName.ToString());
					return;
				}
				Slot.SocketTransform = SocketTrs(Socket);
			}
		}
	if (auto CEmpireSpawnInfo = Handle.try_get<FCompEmpireSpawnInfo>())
	{
		for (auto& ShipT : CEmpireSpawnInfo->ShipTemplates)
		{
			UHelperShipTemplate::UpdateWeaponsSlots(ShipT);
		}
	}
}

void LBreedRegistry::LoadBreed(UObjectsDataAsset* ObjectsDataAsset, EBreedType BreedType)
{
	LoadBreed(ObjectsDataAsset, BreedType, [](FEnttHandle Handle) {});
}

void LBreedRegistry::LoadBreed(UObjectsDataAsset* ObjectsDataAsset, EBreedType BreedType, TFunction<void(FEnttHandle Handle)> const& Finalize)
{
	Loader = MakeUnique<LComponentBreedsFactory>();

	for (auto& ObjectData : ObjectsDataAsset->Objects)
	{
		auto Handle = LoadBreed(ObjectData, Finalize);

		if (!Handle.IsValid())
			continue;

		LoadFinalizer(Handle);

		FName Id = ObjectData.Id;
		BreedsPerType.FindOrAdd(BreedType).Add(TPair<FName, FEnttHandle>(Id, Handle));
	}
}

FEnttHandle LBreedRegistry::Copy(FName Id, FEnttHandle Handle)
{
	if (!IdMap.Contains(Id))
	{
		LOG("Copy breed Id incorrect:" + Id.ToString());
		return {};
	}

	auto Entity = IdMap[Id];

	Loader->Copy({ Reg, Entity }, Handle);
	Handle.emplace_or_replace<FCompBreedId>(Id);

	return { Reg, Entity };
}

FEnttHandle LBreedRegistry::GetBreed(FName Name)
{
	if (!IdMap.Contains(Name))
	{
		LOG("GetBreed Id incorrect: " + Name.ToString());
		return {};
	}

	auto Entity = IdMap[Name];

	return { Reg, Entity };
}

TArray<TPair<FName, FEnttHandle>> const& LBreedRegistry::GetBreeds(EBreedType BreedType)
{
	return BreedsPerType.FindOrAdd(BreedType);
}

FEnttHandle LBreedRegistry::LoadBreed(FObjectData const& ObjectData, TFunction<void(FEnttHandle Handle)> const& Finalize)
{
	FName Id = ObjectData.Id;

	if (Id == "")
	{
		LOG("Breed id incorrect:" + Id.ToString());
		return {};
	}

	if (IdMap.Contains(Id))
	{
		LOG("Breed id duplicate:" + Id.ToString());
		return {};
	}

	FEntity Entity = Reg.create();

	IdMap.Add(Id, Entity);

	FEnttHandle Handle{ Reg, Entity };

	for (auto& ComponentsData : ObjectData.Components)
	{
		Loader->Load(Handle, ComponentsData);
	}
	Loader->AddId(Handle, Id);

	Finalize(Handle);

	return Handle;
}