// Fill out your copyright notice in the Description page of Project Settings.

#include "FGalaxy.h"
#include "Optigame/OptigameData.h"
#include "Generator/GalaxyGenerator.h"
#include "Systems/LSystemOrbits.h"
#include "Systems/LSystemPositionCopyOldPos.h"
#include "Kismet/KismetMathLibrary.h"
#include "Systems/LSystemPlanets.h"
#include "Systems/LSystemEmpires.h"
#include "Systems/LSystemPhys.h"
#include "Systems/LSystemSpace.h"
#include <Optigame/common/XorHasher.h>

struct FCompTransform;

FGalaxy::FGalaxy()
{
}

FGalaxy::~FGalaxy()
{
}

void FGalaxy::Init(AOptigameCore* OptigameCore)
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	Reg.ctx().emplace<LBreedRegistry*>(&OptigameData->BreedRegistry);
	Reg.ctx().emplace<AOptigameCore*>(OptigameCore);
	Reg.ctx().emplace<FGalaxy*>(this);
}

void FGalaxy::Clear()
{
	Reg.clear();

	CurrentFrame = 0;
	ProgressToCurrentFrame = 1;
	TimeSpeed = 1;

	Months = 0;
	Days = 0;
	DayProgress = 0;
}

void FGalaxy::Generate(FNewGameSettings const& NewGameSettings)
{
	FGalaxyGenerator Generator(Reg);
	Generator.Generate(
		NewGameSettings.Galaxy.GalaxyGenStarsCount,
		NewGameSettings.Galaxy.GalaxyGenLinkPercent/100.0,
		XorHash(NewGameSettings.Galaxy.Seed),
		NewGameSettings.Enemy.Count);
}

void FGalaxy::Tick(float DeltaTime)
{
	ProgressToCurrentFrame += DeltaTime * GOptigameVars.Ups;

	for (int i = 0; i < 10 && ProgressToCurrentFrame >= 1; i++)
	{
		Update();
		CurrentFrame++;
		ProgressToCurrentFrame--;
	}

	if (!UKismetMathLibrary::InRange_FloatFloat(ProgressToCurrentFrame, 0, 1, true, true))
	{
		// LOG("Bad keep speed tick galaxy");
	}

	ProgressToCurrentFrame = FMath::Clamp<float>(ProgressToCurrentFrame, 0, 1);
}

void FGalaxy::Update()
{
	bool IsNewDay = (bool)CalculateDays();

	LSystemPositionCopyOldPos::Update(Reg);

	LSystemPhys(Reg).Update();
	LSystemOrbits(Reg).Update();
	LSystemSpace(Reg).Update();

	if (IsNewDay)
	{
		LSystemEmpires(Reg).UpdateResetIncome();

		LSystemPlanets(Reg).Update();

		LSystemEmpires(Reg).UpdateApplyIncome();
	}
}

int FGalaxy::CalculateDays()
{
	double Frames = CurrentFrame;
	double Seconds = Frames * GOptigameVars.FrameTime;
	double NewDays = Seconds / FMath::Max(0.001f, GOptigameVars.DayTime);

	int DaysPrev = Days;

	DayProgress = NewDays - FMath::FloorToDouble(NewDays);
	Days = FMath::FloorToDouble(NewDays);
	Months = Days / FMath::Max(1, GOptigameVars.DaysInMonth);

	if (Days - DaysPrev > 1)
	{
		LOG();
	}

	return Days - DaysPrev;
}