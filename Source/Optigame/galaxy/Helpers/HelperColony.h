// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Optigame/Galaxy/Handles/FEnttStationHandle.h"
#include "Optigame/Galaxy/Handles/FEnttColonyHandle.h"
#include "HelperColony.generated.h"

class LBreedRegistry;

UCLASS()
class UHelperColony : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	static FEntity GetOwner(FEnttHandle const& Planet);

	UFUNCTION(BlueprintCallable)
	static bool HaveStation(FEnttHandle const& Planet);
	UFUNCTION(BlueprintCallable)
	static FEnttStationHandle GetStation(FEnttHandle const& Planet);
	UFUNCTION(BlueprintCallable)
	static void CreateStation(FEnttColonyHandle const& Colony, FName Hull = "ship_m_hull_station_1");
};