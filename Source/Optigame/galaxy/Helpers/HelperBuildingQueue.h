// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Optigame/Galaxy/Components/ComponentsBuildQueue.h"
#include "Optigame/Galaxy/Handles/FEnttBuildingTypeHandle.h"
#include "Optigame/Galaxy/Handles/FEnttColonyHandle.h"
#include "Optigame/Galaxy/Handles/FEnttStationHandle.h"
#include "HelperBuildingQueue.generated.h"

class AOptigameCore;
USTRUCT(BlueprintType)
struct FBuildResultDrawInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName ImageId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Name;
};

UCLASS()
class UHelperBuildingQueue : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static FBuildResultDrawInfo GetDrawInfo(FEnttHandle const& Object, FBuildOperation const& BuildOperation);
	UFUNCTION(BlueprintCallable)
	static void AddBuildQueueOperation(FEnttHandle const& Object, FBuildOperation const& BuildOperation);
	UFUNCTION(BlueprintCallable)
	static bool CanBuildOperation(FEnttHandle const& Object, FBuildOperation const& BuildOperation);
	UFUNCTION(BlueprintCallable)
	static void ExecBuildOperation(FEnttHandle const& Object, FBuildOperation const& BuildOperation);
	UFUNCTION(BlueprintCallable)
	static bool IsStationBuildOperation(FEnttColonyHandle const& Colony);

	UFUNCTION(BlueprintCallable)
	static FBuildOperation CreateBuildOperation(FEnttBuildingTypeHandle const& BuildingType);
	UFUNCTION(BlueprintCallable)
	static FBuildOperation CreateBuildOperationUnique(AOptigameCore* OptigameCore, EBuildResultUnique Val);
	UFUNCTION(BlueprintCallable)
	static FBuildOperation CreateBuildOperationStationModule(AOptigameCore* OptigameCore, FEnttStationHandle Station, FShipTemplateModule const& Module);
	UFUNCTION(BlueprintCallable)
	static FBuildOperation CreateBuildOperationShip(AOptigameCore* OptigameCore, FEnttShipTemplateHandle const& Val);
};