// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "Optigame/galaxy/handles/FEnttStarHandle.h"

#include "HelperShip.generated.h"

UCLASS()
class UHelperShip : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	// Creating ship at current point. Only internal used.
	UFUNCTION(BlueprintCallable)
	static FEnttHandle CreateShip(FCompShipTemplate const& CShipTemplate, FEnttStarHandle const& Star, FTransform const& Trs);

	// Spawn some ships at point. Ships positions will be separated;
	UFUNCTION(BlueprintCallable)
	static TArray<FEnttHandle> SpawnShips(FCompShipTemplate const& CShipTemplate, FEnttStarHandle const& Star, FTransform const& Trs, int32 Count);

	// ShipWarpExec called when ship ready to use warp.
	static void ShipWarpExec(FRegistry& Reg, FEntity Star, FEntity Ship, FEntity WarpZone);
};