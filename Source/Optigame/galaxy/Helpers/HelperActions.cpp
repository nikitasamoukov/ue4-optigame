// Fill out your copyright notice in the Description page of Project Settings.

#include "HelperActions.h"

#include "Optigame/OptigameCore.h"
#include "Optigame/Common/SphereFibPoints.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"

void UHelperActions::Move(AOptigameCore* OptigameCore, FVector Start, FVector Vec, bool IsShift)
{
	auto  Star = OptigameCore->GalaxyProjector->CurrentViewStar;
	auto& Reg = OptigameCore->Galaxy->Reg;

	if (!Star.IsValid())
	{
		LOG();
		return;
	}

	FPlane GamePlane({ 0, 0, GOptigameVars.GamePlaneHeight }, { 0, 0, 1 });
	if (FMath::Abs(FVector::DotProduct(Vec, GamePlane.GetNormal())) < 0.01)
		return;
	FVector PointToMove = FMath::RayPlaneIntersection(Start, Vec, GamePlane);

	TArray<FEntity> Ships;
	for (auto [E, _] : Reg.view<FCompSelectTag>().each())
	{
		Ships.Add(E);
	}

	Ships.Sort();
	// ArrayShuffle(Ships);

	FPointsGenerator PointsGenerator;
	FShipAction		 Action;
	Action.Set<FActionMove>({ Star.entity(), PointToMove });

	for (auto& E : Ships)
	{
		Action.Get<FActionMove>().GamePos.Pos = PointToMove + PointsGenerator.GetPoint() * 10;
		AddActionToShip({ Reg, E }, Action, IsShift);
		PointsGenerator.SkipPoints(1);
	}
}

FGamePos UHelperActions::GetActionTargetPos(FShipAction const& Action)
{
	TOptional<FGamePos> Res;
	Visit([&]<typename Type>(Type const& ValRaw) {
		if constexpr (std::is_same_v<Type, FActionMove>)
		{
			FActionMove const& Val = ValRaw;
			Res = Val.GamePos;
		}
	},
		Action);

	if (Res)
		return *Res;
	LOG();
	return {};
}

void UHelperActions::AddActionToShip(FEnttHandle Ship, FShipAction const& Action, bool IsShift)
{
	if (!Ship.IsValid())
	{
		LOG();
		return;
	}
	if (!Ship.all_of<FCompShipController>())
	{
		LOG();
		return;
	}
	auto& CShipController = Ship.get<FCompShipController>();
	if (!IsShift)
		CShipController.Actions.Empty(1);
	CShipController.Actions.Add(Action);
}