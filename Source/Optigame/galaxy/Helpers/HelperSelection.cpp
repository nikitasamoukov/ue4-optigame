// Fill out your copyright notice in the Description page of Project Settings.

#include "HelperSelection.h"

#include "Optigame/OptigameCore.h"
#include "Optigame/galaxy/Components/ComponentsBasic.h"
#include "Optigame/galaxy/Components/ComponentsShip.h"

void UHelperSelection::SelectShips(AOptigameCore* OptigameCore, FPlane P1, FPlane P2, FPlane P3, FPlane P4, bool IsAdditive)
{
	if (!OptigameCore->Galaxy)
	{
		LOG();
		return;
	}
	auto&		   Reg = OptigameCore->Galaxy->Reg;
	TArray<FPlane> Planes = { P1, P2, P3, P4 };

	if (!IsAdditive)
	{
		for (auto [E, _] : Reg.view<FCompSelectTag>().each())
		{
			Reg.remove<FCompSelectTag>(E);
		}
	}

	for (auto [E, CCurrentTimePos, CShip] : Reg.view<FCompCurrentTimePos, FCompShip>().each())
	{
		bool IsInRect = true;
		for (auto& Plane : Planes)
		{
			auto Dist = Plane.PlaneDot(CCurrentTimePos.Pos);
			if (Dist < 0)
				IsInRect = false;
		}
		if (IsInRect)
		{
			Reg.emplace_or_replace<FCompSelectTag>(E);
		}
	}
}