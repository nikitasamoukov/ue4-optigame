// Fill out your copyright notice in the Description page of Project Settings.

#include "HelperColony.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsEmpire.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"
#include "Optigame/Galaxy/Handles/FEnttColonyHandle.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "HelperShipTemplate.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Components/ComponentsBuildQueue.h"

FEntity UHelperColony::GetOwner(FEnttHandle const& Planet)
{
	if (!Planet)
	{
		LOG();
		return entt::null;
	}
	if (!Planet.all_of<FCompOwner>())
		return entt::null;
	auto Empire = Planet.get<FCompOwner>().Empire;
	if (Planet.registry()->valid(Empire) && !Planet.registry()->all_of<FCompEmpire>(Empire))
	{
		LOG();
		return entt::null;
	}
	return Empire;
}

bool UHelperColony::HaveStation(FEnttHandle const& Planet)
{
	if (!Planet)
	{
		LOG();
		return false;
	}
	return Planet.registry()->valid(GetStation(Planet));
}

FEnttStationHandle UHelperColony::GetStation(FEnttHandle const& Planet)
{
	if (!Planet)
	{
		LOG();
		return {};
	}
	if (!Planet.registry()->all_of<FCompColony>(Planet))
		return {};
	auto Station = Planet.registry()->get<FCompColony>(Planet).Station;
	if (!Planet.registry()->valid(Station))
		return {};
	return { *Planet.registry(), Station };
}

void UHelperColony::CreateStation(FEnttColonyHandle const& Colony, FName Hull)
{
	if (!Colony.IsValid())
	{
		LOG();
		return;
	}

	auto& CColony = Colony.get<FCompColony>();
	auto& CTransform = Colony.get<FCompTransform>();
	auto  COwner = Colony.try_get<FCompOwner>();

	if (!COwner)
	{
		LOG();
		return;
	}

	auto& Reg = *Colony.registry();
	if (Reg.valid(CColony.Station))
	{
		LOG();
		return;
	}

	auto& BreedRegistry = *Reg.ctx().get<LBreedRegistry*>();

	FEnttHandle Station{ Reg, Reg.create() };

	Station.emplace<FCompTransform>(CTransform);
	auto& CStation = Station.emplace<FCompStation>();
	CStation.Planet = Colony.entity();

	auto& CShipTemplate = Station.emplace<FCompShipTemplate>();
	CShipTemplate.Hull = FShipTemplateModule{ Hull, 100, 1 };

	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto StInfo = UHelperShipTemplate::CalcShipInfoSum(Station);
	Station.emplace<FCompSpaceObject>(StInfo.CSpaceObject);
	CStation.Build = StInfo.Build;

	Station.emplace<FCompOwner>(*COwner);
	(void)Station.emplace<FCompBuildQueue>();
	Station.emplace<FCompPosStar>(Colony.get<FCompPosStar>());

	CColony.Station = Station.entity();
}