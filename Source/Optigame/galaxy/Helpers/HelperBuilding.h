// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"

class LBreedRegistry;

struct HelperBuilding
{
	static FEntity Create(FRegistry& Reg, LBreedRegistry& BreedRegistry, FName Name, FEntity Planet);
};