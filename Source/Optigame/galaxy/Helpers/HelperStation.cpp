// Fill out your copyright notice in the Description page of Project Settings.

#include "HelperStation.h"
#include "HelperShipTemplate.h"
#include "Optigame/OptigameData.h"
#include "Optigame/galaxy/handles/FEnttPlanetHandle.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"
#include "Optigame/Misc/NumFmt.h"

FText UHelperStation::CalcStationInfoText(FEnttStationHandle const& Station)
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	if (!Station.IsValid())
	{
		LOG();
		return {};
	}

	auto ShipInfoSum = UHelperShipTemplate::CalcShipInfoSum(Station);

	FFormatNamedArguments Arguments;

	Arguments.Add("Modules", FText::AsNumber(ShipInfoSum.Modules));
	Arguments.Add("ModulesMax", FText::AsNumber(ShipInfoSum.ModulesMax));
	Arguments.Add("Mass", UNumFmt::NumFmtStats(ShipInfoSum.Mass));
	Arguments.Add("Energy", UNumFmt::NumFmtStats(ShipInfoSum.Energy));
	Arguments.Add("Thrust", UNumFmt::NumFmtStats(ShipInfoSum.Thrust));

	Arguments.Add("Hp", UNumFmt::NumFmtStats(ShipInfoSum.CSpaceObject.Hp.Max));
	Arguments.Add("Sp", UNumFmt::NumFmtStats(ShipInfoSum.CSpaceObject.Sp.Max));
	Arguments.Add("Ap", UNumFmt::NumFmtStats(ShipInfoSum.CSpaceObject.Ap.Max));
	Arguments.Add("Speed", UNumFmt::NumFmtStats(ShipInfoSum.CShip.SpeedForwardMax));
	Arguments.Add("Dps", UNumFmt::NumFmtStats(ShipInfoSum.Dps));
	Arguments.Add("Build", UNumFmt::NumFmtStats(ShipInfoSum.Build));

	auto Planet = FEnttPlanetHandle(*Station.registry(), Station.get<FCompStation>().Planet);
	if (!Planet.IsValid())
	{
		LOG();
		return {};
	}
	Arguments.Add("BuildFromPlanet", UNumFmt::NumFmtStats(Planet.get<FCompColony>().BuildToStation));

	FText TextFmt = OptigameData->GetFTextFromId("planet_station_info");
	return FText::Format(TextFmt, Arguments);
}

FCompShipModuleHullStation UHelperStation::GetHull(FEnttStationHandle const& Station)
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	if (!Station.IsValid())
	{
		LOG();
		return {};
	}
	auto StationHull = OptigameData->BreedRegistry.GetBreed(Station.get<FCompShipTemplate>().Hull.BreedId);
	if (!StationHull)
	{
		LOG();
		return {};
	}
	if (!StationHull.all_of<FCompShipModuleHullStation>())
	{
		LOG();
		return {};
	}
	return StationHull.get<FCompShipModuleHullStation>();
}

void UHelperStation::RefreshStats(FEnttStationHandle const& Station)
{
	if (!Station.IsValid())
	{
		LOG();
		return;
	}
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto ShipInfoSum = UHelperShipTemplate::CalcShipInfoSum(Station);

	auto& CSpaceObject = Station.get<FCompSpaceObject>();
	CSpaceObject.Refresh(ShipInfoSum.CSpaceObject);

	Station.get<FCompStation>().Build = ShipInfoSum.Build;
}

void UHelperStation::AddModule(FEnttStationHandle const& Station, FShipTemplateModule const& Module)
{
	if (!Station.IsValid())
	{
		LOG();
		return;
	}

	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto& BreedRegistry = OptigameData->BreedRegistry;

	auto& CShipTemplate = Station.get<FCompShipTemplate>();

	auto ModuleBreed = BreedRegistry.GetBreed(Module.BreedId);
	if (!ModuleBreed)
	{
		LOG();
		return;
	}
	if (!ModuleBreed.all_of<FCompShipModule>())
	{
		LOG();
		return;
	}

	CShipTemplate.Modules.Add(Module);

	RefreshStats(FEnttStationHandle(Station));
}