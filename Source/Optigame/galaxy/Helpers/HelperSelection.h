// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "HelperSelection.generated.h"

class AOptigameCore;

UCLASS()
class UHelperSelection : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static void SelectShips(AOptigameCore* OptigameCore, FPlane P1, FPlane P2, FPlane P3, FPlane P4, bool IsAdditive = false);
};