// Fill out your copyright notice in the Description page of Project Settings.

#include "Pathfinder.h"

#include "Optigame/Galaxy/Components/ComponentsBasic.h"

TArray<FEntity> FPathfinder::Find(FRegistry& Reg, FEntity Start, FEntity Goal)
{
	Update();

	TPair<FEntity, FEntity> MapKey(Start, Goal);
	if (MapPaths.Contains(MapKey))
	{
		MapPaths[MapKey].UsagesAvg++;
		return MapPaths[MapKey].Path;
	}
	AddNewPath(Reg, MapKey);

	return MapPaths[MapKey].Path;
}

void FPathfinder::ClearCached()
{
	MapPaths.Reset();
}

TArray<FEntity> FPathfinder::FindNew(FRegistry& Reg, FEntity Start, FEntity Goal)
{

	auto ReconstructPath = [](TMap<FEntity, FEntity>& CameFrom, FEntity Current) {
		TArray<FEntity> TotalPath = { Current };
		while (CameFrom.Contains(Current))
		{
			Current = CameFrom[Current];
			TotalPath.Add(Current);
		}

		Algo::Reverse(TotalPath);
		return TotalPath;
	};

	auto FuncH = [&](FEntity E) {
		return 0;
	};
	auto FuncD = [&](FEntity A, FEntity B) {
		return 1;
	};

	TArray<FEntity> OpenSet = { Start };

	TMap<FEntity, FEntity> CameFrom;

	TMap<FEntity, double> ScoreG;
	ScoreG.Add(Start, 0);

	TMap<FEntity, double> ScoreF;
	ScoreF.Add(Start, FuncH(Start));

	auto HeapLess = [&](FEntity const& A, FEntity const& B) {
		if (!ScoreF.Contains(A))
			return true;
		if (!ScoreF.Contains(B))
			return false;
		return ScoreF[A] < ScoreF[B];
	};

	while (OpenSet.Num() > 0)
	{
		FEntity Current;
		OpenSet.HeapPop(Current, HeapLess);
		if (Current == Goal)
			return ReconstructPath(CameFrom, Current);

		auto CStar = Reg.try_get<FCompStar>(Current);
		if (!CStar)
		{
			LOG();
			continue;
		}

		for (auto& NeighborInfo : CStar->Connects)
		{
			auto Neighbor = NeighborInfo.Star;

			double Tentative_ScoreG = ScoreG[Current] + FuncD(Current, Neighbor);
			if (!ScoreG.Contains(Neighbor) || Tentative_ScoreG < ScoreG[Neighbor])
			{
				if (!CameFrom.Contains(Neighbor))
					OpenSet.HeapPush(Neighbor, HeapLess);
				CameFrom.Add(Neighbor, Current);
				ScoreG.Add(Neighbor, Tentative_ScoreG);
				ScoreF.Add(Neighbor, ScoreG[Neighbor] + FuncH(Neighbor));
			}
		}
	}

	return {};
}

void FPathfinder::AddNewPath(FRegistry& Reg, TPair<FEntity, FEntity> const& MapKey)
{
	FCachedPath CachedPath;
	CachedPath.Path = FindNew(Reg, MapKey.Key, MapKey.Value);

	MapPaths.Add(MapKey, CachedPath);
}

void FPathfinder::Update()
{
	CallsSinceUpd++;

	if (CallsSinceUpd < 50)
		return;

	struct FStrVec
	{
		TPair<FEntity, FEntity> Key;
		double					UsagesAvg = 0;
	};

	if (MapPaths.Num() > 1100)
	{
		TArray<FStrVec> Vec;
		for (auto& Pair : MapPaths)
		{
			Vec.Add(FStrVec{ Pair.Key, Pair.Value.UsagesAvg });
		}
		Algo::Sort(Vec, [](auto& A, auto& B) { return A.UsagesAvg < B.UsagesAvg; });

		for (size_t i = 1000; i < Vec.Num(); i++)
		{
			MapPaths.Remove(Vec[i].Key);
		}
	}
	for (auto& Pair : MapPaths)
	{
		Pair.Value.UsagesAvg *= 0.97;
	}
}