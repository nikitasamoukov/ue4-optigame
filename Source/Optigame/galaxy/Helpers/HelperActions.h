// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"

#include "HelperActions.generated.h"

class AOptigameCore;

UCLASS()
class UHelperActions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static void Move(AOptigameCore* OptigameCore, FVector Start, FVector Vec, bool IsShift);

	static FGamePos GetActionTargetPos(FShipAction const& Action);

	static void AddActionToShip(FEnttHandle Ship, FShipAction const& Action, bool IsShift);
};