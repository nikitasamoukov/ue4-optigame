// Fill out your copyright notice in the Description page of Project Settings.

#include "HelperShip.h"
#include "HelperShipTemplate.h"
#include "Optigame/Common/CommonMath.h"
#include "Optigame/Common/SphereFibPoints.h"
#include "Optigame/Galaxy/BreedRegistry/LBreedRegistry.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "Optigame/Galaxy/Handles/FEnttStarHandle.h"

FEnttHandle UHelperShip::CreateShip(FCompShipTemplate const& CShipTemplate, FEnttStarHandle const& Star, FTransform const& Trs)
{
	if (!Star.IsValid())
	{
		LOG();
		return {};
	}
	auto& Reg = *Star.registry();
	auto& BreedRegistry = *Reg.ctx().get<LBreedRegistry*>();

	auto& CStar = Star.get<FCompStar>();

	auto ShipInfo = UHelperShipTemplate::CalcShipInfoSum(CShipTemplate);

	auto HullModule = BreedRegistry.GetBreed(CShipTemplate.Hull.BreedId);
	if (!HullModule)
	{
		LOG();
		return {};
	}
	auto ModuleCMesh = HullModule.try_get<FCompMesh>();
	if (!ModuleCMesh)
	{
		LOG();
		return {};
	}

	FEnttHandle Ship(Reg, Reg.create());

	auto& CTransform = Ship.emplace<FCompTransform>();
	CTransform.Pos = Trs.GetLocation();
	CTransform.Quat = Trs.GetRotation();
	CTransform.CopyToPrev();

	Ship.emplace<FCompSpaceObject>(ShipInfo.CSpaceObject);
	Ship.emplace<FCompShip>(ShipInfo.CShip);
	Ship.emplace<FCompMesh>(*ModuleCMesh);
	Ship.emplace<FCompPosStar>(Star.entity());
	(void)Ship.emplace<FCompShipController>();
	Ship.emplace<FCompPhys>(ShipInfo.CPhys);
	Ship.emplace<FCompShipWarpOffset>(GeneratePointInSphere(Ship.entity()) * GOptigameVars.WarpZoneRadius);

	CStar.Ships.Add(Ship.entity());

	return Ship;
}

TArray<FEnttHandle> UHelperShip::SpawnShips(FCompShipTemplate const& CShipTemplate, FEnttStarHandle const& Star,
	FTransform const& Trs, int32 Count)
{
	TArray<FEnttHandle> Res;

	FPointsGenerator PointsGenerator;
	for (size_t i = 0; i < Count; i++)
	{
		FVector PosOffset = PointsGenerator.GetPoint() * 10;
		PointsGenerator.SkipPoints(1);

		FTransform ShipTrs = Trs;
		ShipTrs.AddToTranslation(PosOffset);
		auto E = CreateShip(CShipTemplate, Star, ShipTrs);
		Res.Add(E);
	}

	return Res;
}

void UHelperShip::ShipWarpExec(FRegistry& Reg, FEntity Star, FEntity Ship, FEntity WarpZone)
{
	auto CStar = Reg.try_get<FCompStar>(Star);
	auto CWarpZone = Reg.try_get<FCompWarpZone>(WarpZone);

	if (!CStar)
	{
		LOG();
		return;
	}
	if (!CWarpZone)
	{
		LOG();
		return;
	}

	{
		int i = CStar->Ships.Find(Ship);
		if (i == INDEX_NONE)
		{
			LOG();
		}
		CStar->Ships.RemoveSwap(Ship);
	}

	auto NewCStar = Reg.try_get<FCompStar>(CWarpZone->Star);
	if (!NewCStar)
	{
		LOG();
		return;
	}

	NewCStar->Ships.Add(Ship);

	auto StarConnect = NewCStar->Connects.FindByPredicate([&](FStarConnect const& E) { return E.Star == Star; });

	auto NewCWarpZonePos = Reg.try_get<FCompTransform>(StarConnect->WarpZone)->Pos;

	auto CTransform = Reg.try_get<FCompTransform>(Ship);
	auto CShipWarpOffset = Reg.try_get<FCompShipWarpOffset>(Ship);

	CTransform->Pos = NewCWarpZonePos + CShipWarpOffset->Offset;
	CTransform->CopyToPrev();
}