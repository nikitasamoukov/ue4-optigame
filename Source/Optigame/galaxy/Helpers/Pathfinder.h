// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Pathfinder.generated.h"

USTRUCT()
struct FPathfinder
{
	GENERATED_BODY()
public:
	TArray<FEntity> Find(FRegistry& Reg, FEntity Start, FEntity Goal);
	void ClearCached();

protected:
	TArray<FEntity> FindNew(FRegistry& Reg, FEntity Start, FEntity Goal);
	void AddNewPath(FRegistry& Reg, TPair<FEntity, FEntity> const& MapKey);
	void Update();

	struct FCachedPath
	{
		TArray<FEntity> Path{};
		double UsagesAvg = 0;
	};

	TMap<TPair<FEntity, FEntity>, FCachedPath> MapPaths;
	int CallsSinceUpd = 0;
};