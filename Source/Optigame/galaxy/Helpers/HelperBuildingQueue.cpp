// Fill out your copyright notice in the Description page of Project Settings.

#include "HelperBuildingQueue.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Components/ComponentsBuilding.h"
#include "Optigame/Galaxy/Components/ComponentsEmpire.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"
#include "HelperColony.h"
#include "Optigame/common/common.h"
#include "HelperBuilding.h"
#include "Optigame/Galaxy/Handles/FEnttColonyHandle.h"
#include "Optigame/OptigameCore.h"
#include "HelperStation.h"
#include "HelperShipTemplate.h"
#include "HelperShip.h"
#include "Optigame/galaxy/handles/FEnttStarHandle.h"

FBuildResultDrawInfo UHelperBuildingQueue::GetDrawInfo(FEnttHandle const& Object, FBuildOperation const& BuildOperation)
{
	FBuildResultDrawInfo Res;

	if (!Object.IsValid())
	{
		LOG();
		return {};
	}

	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto& BreedRegistry = OptigameData->BreedRegistry;

	// TVariant<FBuildResultBuilding, FBuildResultShip, FBuildResultStationModule, FBuildResultUnique>

	Res.Name = FText::FromString("<unknown>");

	if (auto Val = BuildOperation.BuildResult.Val.TryGet<FBuildResultBuilding>())
	{
		auto BuildingHandle = BreedRegistry.GetBreed(Val->Id);
		if (!BuildingHandle)
		{
			LOG();
			return {};
		}
		auto CBuildingType = BuildingHandle.try_get<FCompBuildingType>();
		if (!CBuildingType)
		{
			LOG();
			return {};
		}

		Res.ImageId = CBuildingType->ImageId;
		Res.Name = OptigameData->GetFTextFromId(CBuildingType->NameId);
	}
	if (auto Val = BuildOperation.BuildResult.Val.TryGet<FBuildResultShip>())
	{
		auto ModuleHandle = BreedRegistry.GetBreed(Val->ShipTemplate.Hull.BreedId);
		if (!ModuleHandle)
		{
			LOG();
			return {};
		}
		auto CShipModule = ModuleHandle.try_get<FCompShipModule>();
		if (!CShipModule)
		{
			LOG();
			return {};
		}

		Res.ImageId = CShipModule->ImageId;
		Res.Name = FText::FromString(Val->ShipTemplate.Name);
	}
	if (auto Val = BuildOperation.BuildResult.Val.TryGet<FBuildResultStationModule>())
	{
		auto ModuleHandle = BreedRegistry.GetBreed(Val->TemplateModule.BreedId);
		auto CShipModule = ModuleHandle.try_get<FCompShipModule>();
		if (!CShipModule)
		{
			LOG();
			return {};
		}

		Res.ImageId = CShipModule->ImageId;

		FText				  TextFmt = OptigameData->GetFTextFromId("ship_module_with_tier");
		FFormatNamedArguments Arguments;
		Arguments.Add("Name", OptigameData->GetFTextFromId(CShipModule->NameId));
		Arguments.Add("Tier", FText::AsNumber(Val->TemplateModule.Tier));

		Res.Name = FText::Format(TextFmt, Arguments);
	}
	if (auto Val = BuildOperation.BuildResult.Val.TryGet<FBuildResultUnique>())
	{
		Res.ImageId = "";
		Res.Name = FText::FromString("<Some unique project> :" + GetEnumValueAsString(Val->Val));
	}

	return Res;
}

void UHelperBuildingQueue::AddBuildQueueOperation(FEnttHandle const& Object, FBuildOperation const& BuildOperation)
{
	if (!Object)
	{
		LOG();
		return;
	}
	if (!CanBuildOperation(Object, BuildOperation))
	{
		LOG("!CanBuildOperation");
		return;
	}
	if (!Object.all_of<FCompBuildQueue>())
	{
		LOG();
		return;
	}
	auto  Empire = FEnttHandle(*Object.registry(), Object.get<FCompOwner>().Empire);
	auto& CEmpire = Empire.get<FCompEmpire>();

	LOG("Try add to queue");

	if (!CEmpire.IsHaveResources(BuildOperation.Cost))
		return;

	LOG("Adding to queue");

	CEmpire.ResourcesCostPay(BuildOperation.Cost);

	auto& CBuildQueue = Object.get<FCompBuildQueue>();
	CBuildQueue.Queue.Add(BuildOperation);
}

bool UHelperBuildingQueue::CanBuildOperation(FEnttHandle const& Object, FBuildOperation const& BuildOperation)
{
	if (!Object)
	{
		LOG();
		return false;
	}
	if (!Object.all_of<FCompOwner>())
		return false;
	auto  Empire = FEnttHandle(*Object.registry(), Object.get<FCompOwner>().Empire);
	auto& CEmpire = Empire.get<FCompEmpire>();
	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto& BreedRegistry = OptigameData->BreedRegistry;

	if (BuildOperation.BuildResult.Val.IsType<FBuildResultBuilding>())
	{
		if (!Object.all_of<FCompColony>())
		{
			LOG("No colony");
			return false;
		}
		auto& CColony = Object.get<FCompColony>();
		if (CColony.Buildings.Contains(entt::null))
			return true;
		return false;
	}
	else if (BuildOperation.BuildResult.Val.IsType<FBuildResultShip>())
	{
		return true;
	}
	else if (BuildOperation.BuildResult.Val.IsType<FBuildResultStationModule>())
	{
		LOG("FBuildResultStationModule st");
		auto Val = BuildOperation.BuildResult.Val.TryGet<FBuildResultStationModule>();

		if (!Object.all_of<FCompShipTemplate>())
		{
			LOG();
			return false;
		}
		auto& CShipTemplate = Object.get<FCompShipTemplate>();

		auto CShipModuleHullStation = UHelperStation::GetHull(FEnttStationHandle(Object));

		if (CShipTemplate.Modules.Num() >= CShipModuleHullStation.Modules)
			return false;

		LOG("FBuildResultStationModule ok");
		return true;
	}
	else if (BuildOperation.BuildResult.Val.IsType<FBuildResultUnique>())
	{
		auto& Val = *BuildOperation.BuildResult.Val.TryGet<FBuildResultUnique>();
		if (Val.Val == EBuildResultUnique::Station)
		{
			if (!UHelperColony::HaveStation(Object))
				return true;
			return false;
		}
		return false;
	}
	return false;
}

void UHelperBuildingQueue::ExecBuildOperation(FEnttHandle const& Object, FBuildOperation const& BuildOperation)
{
	if (!Object)
	{
		LOG();
		return;
	}
	if (!Object.all_of<FCompOwner>())
	{
		LOG();
		return;
	}

	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto& BreedRegistry = OptigameData->BreedRegistry;

	if (!CanBuildOperation(Object, BuildOperation))
	{
		LOG("Need implement failure build op");
		return;
	}

	if (auto Val = BuildOperation.BuildResult.Val.TryGet<FBuildResultBuilding>())
	{
		if (!Object.all_of<FCompColony>())
		{
			LOG();
			return;
		}
		auto& CColony = Object.get<FCompColony>();
		auto  Idx = CColony.Buildings.Find(entt::null);
		if (Idx == INDEX_NONE)
		{
			LOG();
			return;
		}

		auto Building = HelperBuilding::Create(*Object.registry(), BreedRegistry, Val->Id, Object.entity());
		CColony.Buildings[Idx] = Building;
	}
	if (auto Val = BuildOperation.BuildResult.Val.TryGet<FBuildResultShip>())
	{
		auto CPosStar = Object.try_get<FCompPosStar>();
		if (!CPosStar)
		{
			LOG();
			return;
		}
		auto CTransform = Object.try_get<FCompTransform>();
		if (!CTransform)
		{
			LOG();
			return;
		}
		FTransform Trs;
		Trs.SetLocation(CTransform->Pos + FVector(0, 0, GOptigameVars.GamePlaneHeight));
		UHelperShip::CreateShip(Val->ShipTemplate, FEnttStarHandle(*Object.registry(), CPosStar->Star), Trs);
	}
	if (auto Val = BuildOperation.BuildResult.Val.TryGet<FBuildResultStationModule>())
	{
		UHelperStation::AddModule(FEnttStationHandle(Object), Val->TemplateModule);
	}
	if (auto Val = BuildOperation.BuildResult.Val.TryGet<FBuildResultUnique>())
	{
		switch (Val->Val)
		{
			case EBuildResultUnique::Station:
			{
				UHelperColony::CreateStation(FEnttColonyHandle(Object));
				break;
			}
		}
	}
}

bool UHelperBuildingQueue::IsStationBuildOperation(FEnttColonyHandle const& Colony)
{
	if (!Colony)
	{
		LOG();
		return false;
	}
	if (!Colony.all_of<FCompBuildQueue>())
		return false;
	auto& CBuildQueue = Colony.get<FCompBuildQueue>();

	for (auto& E : CBuildQueue.Queue)
		if (auto Val = E.BuildResult.Val.TryGet<FBuildResultUnique>())
		{
			if (Val->Val == EBuildResultUnique::Station)
				return true;
		}

	return false;
}

FBuildOperation UHelperBuildingQueue::CreateBuildOperation(FEnttBuildingTypeHandle const& BuildingType)
{
	if (!BuildingType)
	{
		LOG();
		return {};
	}

	FBuildOperation Res;

	FBuildResultBuilding BuildRes;
	auto				 CTemplateId = BuildingType.try_get<FCompBreedId>();
	if (!CTemplateId)
	{
		LOG();
		return {};
	}
	BuildRes.Id = CTemplateId->Id;
	Res.BuildResult.Val.Set<FBuildResultBuilding>(BuildRes);

	auto CBuildingCost = BuildingType.try_get<FCompBuildingCost>();
	if (!CBuildingCost)
	{
		LOG();
		return {};
	}
	Res.Cost = CBuildingCost->Cost;

	Res.BuildFull = Res.Cost.Resources.FindOrAdd("res_build");

	return Res;
}

FBuildOperation UHelperBuildingQueue::CreateBuildOperationUnique(AOptigameCore* OptigameCore, EBuildResultUnique Val)
{
	FBuildOperation Res;

	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto& BreedRegistry = OptigameData->BreedRegistry;
	auto  ModuleH = BreedRegistry.GetBreed("ship_m_hull_station_1");
	if (!ModuleH)
	{
		LOG();
		return {};
	}
	if (!ModuleH.all_of<FCompShipModule>())
	{
		LOG();
		return {};
	}
	auto& CShipModule = ModuleH.get<FCompShipModule>();

	Res.BuildResult.Val.Set<FBuildResultUnique>({ Val });
	Res.Cost = CShipModule.Cost;
	Res.BuildFull = Res.Cost.Resources.FindOrAdd("res_build");

	return Res;
}

FBuildOperation UHelperBuildingQueue::CreateBuildOperationStationModule(AOptigameCore* OptigameCore, FEnttStationHandle Station, FShipTemplateModule const& Module)
{
	FBuildOperation Res;

	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto& BreedRegistry = OptigameData->BreedRegistry;
	auto  ModuleH = BreedRegistry.GetBreed(Module.BreedId);
	if (!ModuleH)
	{
		LOG();
		return {};
	}
	if (!ModuleH.all_of<FCompShipModule>())
	{
		LOG();
		return {};
	}
	auto& CShipModule = ModuleH.get<FCompShipModule>();

	if (!Station.IsValid())
	{
		LOG();
		return {};
	}
	auto StationHull = BreedRegistry.GetBreed(Station.get<FCompShipTemplate>().Hull.BreedId);
	if (!StationHull)
	{
		LOG();
		return {};
	}
	if (!StationHull.all_of<FCompShipModuleHullStation>())
	{
		LOG();
		return {};
	}
	double ModuleVolume = StationHull.get<FCompShipModuleHullStation>().Volume;

	Res.BuildResult.Val.Set<FBuildResultStationModule>({ Module });
	Res.BuildResult.Val.Get<FBuildResultStationModule>().TemplateModule.Size = ModuleVolume;
	Res.Cost = CShipModule.GetScaled(ModuleVolume * GOptigameVars.StationModuleCostScale /*Not best but work*/, Module.Tier).Cost;
	Res.BuildFull = Res.Cost.Resources.FindOrAdd("res_build");

	return Res;
}

FBuildOperation UHelperBuildingQueue::CreateBuildOperationShip(AOptigameCore* OptigameCore, FEnttShipTemplateHandle const& Val)
{
	FBuildOperation Res;

	if (!Val.IsValid())
		return Res;
	auto& CShipTemplate = Val.get<FCompShipTemplate>();

	Res.BuildResult.Val.Set<FBuildResultShip>({ CShipTemplate });
	auto ShipInfo = UHelperShipTemplate::CalcShipInfoSum(CShipTemplate);
	Res.Cost = ShipInfo.Cost;

	Res.BuildFull = Res.Cost.Resources.FindOrAdd("res_build");

	return Res;
}