// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Optigame/Galaxy/Handles/FEnttStationHandle.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "HelperStation.generated.h"

UCLASS()
class UHelperStation : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static FText CalcStationInfoText(FEnttStationHandle const& Station);
	UFUNCTION(BlueprintCallable)
	static FCompShipModuleHullStation GetHull(FEnttStationHandle const& Station);
	UFUNCTION(BlueprintCallable)
	static void RefreshStats(FEnttStationHandle const& Station);
	UFUNCTION(BlueprintCallable)
	static void AddModule(FEnttStationHandle const& Station, FShipTemplateModule const& Module);
};