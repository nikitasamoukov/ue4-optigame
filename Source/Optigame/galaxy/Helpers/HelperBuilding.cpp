// Fill out your copyright notice in the Description page of Project Settings.

#include "HelperBuilding.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Components/ComponentsBuilding.h"

FEntity HelperBuilding::Create(FRegistry& Reg, LBreedRegistry& BreedRegistry, FName Name, FEntity Planet)
{
	FEntity Entity = Reg.create();
	auto	Breed = BreedRegistry.Copy(Name, { Reg, Entity });
	auto&	CBuilding = Reg.emplace_or_replace<FCompBuilding>(Entity);

	CBuilding.TypeBreed = Breed;
	CBuilding.Planet = Planet;

	return Entity;
}