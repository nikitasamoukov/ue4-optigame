// Fill out your copyright notice in the Description page of Project Settings.

#include "Optigame/OptigamePch.h"
#include "HelperShipTemplate.h"

#include "Engine/Texture2D.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsEmpire.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "Optigame/Galaxy/BreedRegistry/LBreedRegistry.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Misc/NumFmt.h"

FString UHelperShipTemplate::CostTextCalc(FResPile const& Cost)
{
	FString Res;

	auto OptigameData = UOptigameProjectSettings::GetOptigameData();

	for (auto& e : Cost.Resources)
	{
		if (!Res.IsEmpty())
			Res += ", ";

		auto HandleResType = OptigameData->BreedRegistry.GetBreed(e.Key);
		if (!HandleResType)
		{
			LOG();
			continue;
		}

		FFormatNamedArguments Arguments;
		Arguments.Add("Count", UNumFmt::NumFmtCost(e.Value));

		if (HandleResType.all_of<FCompResourceType>())
			Arguments.Add("Text", OptigameData->GetFTextFromId(HandleResType.get<FCompResourceType>().NameId));
		else
			LOG("e.TypeId is res? " + e.Key.ToString());

		FText ResText = FText::Format(FText::FromString("{Count}{Text}"), Arguments);

		Res += ResText.ToString();
	}

	return Res;
}

void UHelperShipTemplate::CreateEmpireEmptyBuildShip(FEnttHandle const& Empire)
{
	if (!Empire)
	{
		LOG();
		return;
	}
	if (!Empire.all_of<FCompEmpire>())
	{
		LOG();
		return;
	}
	DestroyEmpireBuildShip(Empire);
	auto& CEmpire = Empire.get<FCompEmpire>();
	auto& Reg = *Empire.registry();

	CEmpire.ShipTemplateEditing = Reg.create();
	FEnttHandle Template(Reg, CEmpire.ShipTemplateEditing);

	FCompShipTemplate CShipTemplate;
	CShipTemplate.Hull.BreedId = GOptigameVars.BasicShipHullId;
	CShipTemplate.Hull.Size = 0;
	Template.emplace<FCompShipTemplate>(CShipTemplate);
}

void UHelperShipTemplate::DestroyEmpireBuildShip(FEnttHandle const& Empire)
{
	if (!Empire)
	{
		LOG();
		return;
	}
	if (!Empire.all_of<FCompEmpire>())
	{
		LOG();
		return;
	}
	auto& CEmpire = Empire.get<FCompEmpire>();
	auto& Reg = *Empire.registry();

	if (!Reg.valid(CEmpire.ShipTemplateEditing))
		return;
	Reg.destroy(CEmpire.ShipTemplateEditing);
	CEmpire.ShipTemplateEditing = entt::null;
}

FString GenShipName(FRegistry& Reg, FCompEmpire& CEmpire)
{
	TSet<FString> Names;
	for (auto E : CEmpire.ShipTemplates)
	{
		auto& CShipTemplateE = Reg.get_or_emplace<FCompShipTemplate>(E);
		Names.Add(CShipTemplateE.Name);
	}

	for (int i = 0; i < Names.Num() + 10; i++)
	{
		FString Name = "Unnamed_" + FString::FromInt(i);
		if (!Names.Contains(Name))
		{
			return Name;
		}
	}
	LOG();
	return "out of names";
}

void UHelperShipTemplate::SaveEmpireBuildShip(FEnttHandle const& Empire, FString Name)
{
	if (!Empire)
	{
		LOG();
		return;
	}
	if (!Empire.all_of<FCompEmpire>())
	{
		LOG();
		return;
	}
	auto& CEmpire = Empire.get<FCompEmpire>();
	auto& Reg = *Empire.registry();

	if (!Reg.valid(CEmpire.ShipTemplateEditing))
	{
		LOG();
		return;
	}

	auto& CShipTemplate = Reg.get_or_emplace<FCompShipTemplate>(CEmpire.ShipTemplateEditing);

	if (Name.IsEmpty())
	{
		Name = GenShipName(Reg, CEmpire);
		CShipTemplate.Name = Name;
	}

	FEntity* ThisShipPtr = CEmpire.ShipTemplates.FindByPredicate([&](auto E) {
		auto& CShipTemplateE = Reg.get_or_emplace<FCompShipTemplate>(E);
		return CShipTemplateE.Name == CShipTemplate.Name;
	});

	FEntity ThisShip;

	if (ThisShipPtr) // owerwrite
	{
		ThisShip = *ThisShipPtr;
	}
	else // new
	{
		ThisShip = Reg.create();
		CEmpire.ShipTemplates.Add(ThisShip);
	}

	auto& CShipInStore = Reg.get_or_emplace<FCompShipTemplate>(ThisShip);
	CShipInStore = Reg.get_or_emplace<FCompShipTemplate>(CEmpire.ShipTemplateEditing);
}

FText UHelperShipTemplate::GetShipModuleInfo(FShipTemplateModule const& Module, bool IsStation)
{
	FString Res;

	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto ModuleBreed = OptigameData->BreedRegistry.GetBreed(Module.BreedId);
	if (!ModuleBreed.IsValid())
	{
		LOG();
		return {};
	}

	if (!ModuleBreed.all_of<FCompShipModule>())
	{
		LOG();
		return {};
	}

	auto CShipModule = ModuleBreed.get<FCompShipModule>().GetScaled(Module.Size, Module.Tier);

	{
		FFormatNamedArguments Arguments;
		if (!IsStation)
		{
			Arguments.Add("CostText", FText::FromString(CostTextCalc(CShipModule.Cost)));
		}
		else
		{
			FResPile ResCost = CShipModule.Cost;

			for (auto& E : ResCost.Resources)
				E.Value *= GOptigameVars.StationModuleCostScale;

			Arguments.Add("CostText", FText::FromString(CostTextCalc(ResCost)));
		}
		FText TextFmt = OptigameData->GetFTextFromId("str_ship_module_info_stat_cost");
		Res += FText::Format(TextFmt, Arguments).ToString();
	}
	Res += "\n";

	FString TextStats;

	if (!IsStation)
	{
		{
			FFormatNamedArguments Arguments;
			Arguments.Add("Mass", UNumFmt::NumFmtStats(CShipModule.Mass));
			FText TextFmt = OptigameData->GetFTextFromId("str_ship_module_info_stat_mass");
			TextStats += FText::Format(TextFmt, Arguments).ToString();
		}

		if (CShipModule.Energy)
		{
			TextStats += ", ";
			FText EnergyValText;
			{
				FFormatNamedArguments Arguments;
				Arguments.Add("Energy", UNumFmt::NumFmtStats(FMath::Abs(CShipModule.Energy)));
				FText TextFmt;
				if (CShipModule.Energy > 0)
					TextFmt = OptigameData->GetFTextFromId("str_ship_module_info_stat_energy_plus");
				else
					TextFmt = OptigameData->GetFTextFromId("str_ship_module_info_stat_energy_minus");
				EnergyValText = FText::Format(TextFmt, Arguments);
			}
			FFormatNamedArguments Arguments;
			Arguments.Add("Energy", EnergyValText);
			FText TextFmt = OptigameData->GetFTextFromId("str_ship_module_info_stat_energy");

			TextStats += FText::Format(TextFmt, Arguments).ToString();
		}
	}

	if (auto C = ModuleBreed.try_get<FCompShipModuleHull>())
	{
		if (!TextStats.IsEmpty())
			TextStats += "\n";
		FText TextFmt = OptigameData->GetFTextFromId("str_ship_m_c_hull");

		FFormatNamedArguments Arguments;
		Arguments.Add("Hp", UNumFmt::NumFmtStats(C->Hp));
		Arguments.Add("Volume", UNumFmt::NumFmtStats(C->Volume));
		TextStats += FText::Format(TextFmt, Arguments).ToString();
	}

	if (auto C = ModuleBreed.try_get<FCompShipModuleArmor>())
	{
		if (!TextStats.IsEmpty())
			TextStats += "\n";
		auto  Comp = C->GetScaled(Module.Size, Module.Tier);
		FText TextFmt = OptigameData->GetFTextFromId("str_ship_m_c_armor");

		FFormatNamedArguments Arguments;
		Arguments.Add("Ap", UNumFmt::NumFmtStats(Comp.Ap));
		Arguments.Add("Regen", UNumFmt::NumFmtStats(Comp.Regen));
		TextStats += FText::Format(TextFmt, Arguments).ToString();
	}

	if (auto C = ModuleBreed.try_get<FCompShipModuleShield>())
	{
		if (!TextStats.IsEmpty())
			TextStats += "\n";
		auto  Comp = C->GetScaled(Module.Size, Module.Tier);
		FText TextFmt = OptigameData->GetFTextFromId("str_ship_m_c_shield");

		FFormatNamedArguments Arguments;
		Arguments.Add("Sp", UNumFmt::NumFmtStats(Comp.Sp));
		Arguments.Add("Regen", UNumFmt::NumFmtStats(Comp.Regen));
		TextStats += FText::Format(TextFmt, Arguments).ToString();
	}

	if (auto C = ModuleBreed.try_get<FCompShipModuleEngine>())
	{
		if (!TextStats.IsEmpty())
			TextStats += "\n";
		auto  Comp = C->GetScaled(Module.Size, Module.Tier);
		FText TextFmt = OptigameData->GetFTextFromId("str_ship_m_c_engine");

		FFormatNamedArguments Arguments;
		Arguments.Add("Thrust", UNumFmt::NumFmtStats(Comp.Thrust));
		TextStats += FText::Format(TextFmt, Arguments).ToString();
	}

	if (auto C = ModuleBreed.try_get<FCompShipModuleWeapon>())
	{
		if (!TextStats.IsEmpty())
			TextStats += "\n";
		auto  Comp = C->GetScaled(Module.Size, Module.Tier);
		FText TextFmt = OptigameData->GetFTextFromId("str_ship_m_c_weapon");

		FFormatNamedArguments Arguments;
		Arguments.Add("Dps", UNumFmt::NumFmtStats(Comp.Dps));
		Arguments.Add("Colldown", UNumFmt::NumFmtStats(Comp.Cooldown));
		Arguments.Add("Range", UNumFmt::NumFmtStats(Comp.Range));
		TextStats += FText::Format(TextFmt, Arguments).ToString();
	}

	if (auto C = ModuleBreed.try_get<FCompShipModuleShipyard>())
	{
		if (!TextStats.IsEmpty())
			TextStats += "\n";
		FText TextFmt = OptigameData->GetFTextFromId("str_ship_m_c_shipyard");

		FFormatNamedArguments Arguments;
		Arguments.Add("Build", UNumFmt::NumFmtStats(C->Build));
		TextStats += FText::Format(TextFmt, Arguments).ToString();
	}

	FText TextStatsFmt = OptigameData->GetFTextFromId("str_ship_module_info_stats");

	FFormatNamedArguments Arguments;
	Arguments.Add("StatsText", FText::FromString(TextStats));
	Res += FText::Format(TextStatsFmt, Arguments).ToString();

	return FText::FromString(Res);
}

void UHelperShipTemplate::GetModuleFullInfo(
	FShipTemplateModule const& Module,
	bool					   IsStation,
	bool&					   Success,
	FShipModuleFullInfo&	   FullInfo)
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	Success = false;

	if (Module.BreedId == NAME_None)
	{
		return;
	}
	auto ModuleBreed = OptigameData->BreedRegistry.GetBreed(Module.BreedId);
	if (!ModuleBreed.IsValid())
	{
		return;
	}

	auto CShipModule = ModuleBreed.try_get<FCompShipModule>();
	if (!CShipModule)
	{
		return;
	}
	auto TexturePtr = OptigameData->TexturesMap.Find(CShipModule->ImageId);
	if (!TexturePtr)
	{
		return;
	}
	FullInfo.Image = *TexturePtr;

	FullInfo.Info = GetShipModuleInfo(Module, IsStation);

	auto ModuleName = OptigameData->StringsMap.Find(CShipModule->NameId);

	if (!ModuleName)
	{
		return;
	}
	FullInfo.Name = FText::FromString(*ModuleName);

	switch (CShipModule->Type)
	{
		case EShipModuleType::Hull:
		case EShipModuleType::HullStation:
			FullInfo.HaveTier = false;
			break;
		default:
			FullInfo.HaveTier = true;
			break;
	}

	Success = true;
}

FShipInfoStr UHelperShipTemplate::CalcShipInfoSum(FEnttHandle const& ShipTemplate)
{
	if (!ShipTemplate.IsValid())
	{
		LOG();
		return {};
	}
	if (!ShipTemplate.all_of<FCompShipTemplate>())
	{
		LOG();
		return {};
	}

	auto& CShipTemplate = ShipTemplate.get<FCompShipTemplate>();

	return CalcShipInfoSum(CShipTemplate);
}

FShipInfoStr UHelperShipTemplate::CalcShipInfoSum(FCompShipTemplate const& CShipTemplate)
{
	FShipInfoStr Res;

	auto  OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto& BreedRegistry = OptigameData->BreedRegistry;

	auto HullHandle = BreedRegistry.GetBreed(CShipTemplate.Hull.BreedId);

	if (!HullHandle)
	{
		LOG();
		return Res;
	}

	Res.CSpaceObject.HullBreedId = CShipTemplate.Hull.BreedId;

	if (auto C = HullHandle.try_get<FCompShipModuleHullStation>())
	{
		auto Module = C;
		Res.CSpaceObject.Hp.Max = Module->Hp;
		Res.VolumeFull = Module->Volume;
		Res.ModulesMax += Module->Modules;
	}
	if (auto C = HullHandle.try_get<FCompShipModuleHull>())
	{
		auto Module = C;
		Res.CSpaceObject.Hp.Max = Module->Hp;
		Res.VolumeFull = Module->Volume;
	}

	if (Res.VolumeFull <= 0)
	{
		LOG();
		return Res;
	}

	{
		auto& CShipModule = HullHandle.get<FCompShipModule>();
		Res.Energy += CShipModule.Energy;
		Res.Mass += CShipModule.Mass;
		Res.Cost.Add(CShipModule.Cost);
	}
	auto AddModule = [&](FShipTemplateModule const& E) {
		if (E.BreedId == NAME_None)
			return;
		auto Handle = BreedRegistry.GetBreed(E.BreedId);
		if (!Handle || !Handle.all_of<FCompShipModule>())
		{
			LOG("ERROR:" + E.BreedId.ToString());
			return;
		}

		double ModuleSize = (double)E.Size;

		auto CShipModule = Handle.get<FCompShipModule>().GetScaled(ModuleSize, E.Tier);

		Res.Energy += CShipModule.Energy;
		Res.Mass += CShipModule.Mass;
		Res.Cost.Add(CShipModule.Cost);
		if (CShipModule.Type != EShipModuleType::Armor)
			Res.VolumeUsed += E.Size;

		if (auto C = Handle.try_get<FCompShipModuleEngine>())
		{
			auto Module = C->GetScaled(ModuleSize, E.Tier);
			Res.Thrust += Module.Thrust;
		}
		if (auto C = Handle.try_get<FCompShipModuleArmor>())
		{
			auto Module = C->GetScaled(ModuleSize, E.Tier);
			Res.CSpaceObject.Ap.Max += Module.Ap;
			Res.CSpaceObject.Ap.Regen += Module.Regen;
		}
		if (auto C = Handle.try_get<FCompShipModuleShield>())
		{
			auto Module = C->GetScaled(ModuleSize, E.Tier);
			Res.CSpaceObject.Sp.Max += Module.Sp;
			Res.CSpaceObject.Sp.Regen += Module.Regen;
		}
		if (auto C = Handle.try_get<FCompShipModuleWeapon>())
		{
			auto Module = C->GetScaled(ModuleSize, E.Tier);
			Res.Dps += Module.Dps;
		}
		if (auto C = Handle.try_get<FCompShipModuleShipyard>())
		{
			auto Module = C;
			Res.Build += Module->Build;
		}
	};

	for (auto& E : CShipTemplate.Modules)
		AddModule(E);
	for (auto& E : CShipTemplate.Weapons)
		AddModule(E);

	Res.Modules = CShipTemplate.Modules.Num();
	Res.CShip.SpeedForwardMax = Res.Thrust / FMath::Max(0.00001f, Res.Mass) * GOptigameVars.Ships.SpeedMul;
	Res.CShip.SpeedSideMax = Res.CShip.SpeedForwardMax * GOptigameVars.Ships.SideSpeedMul;
	Res.CShip.SpeedRotateMax = FMath::DegreesToRadians(60) * GOptigameVars.Ships.RotateMul;

	Res.CSpaceObject.SetFull();

	if (auto CShipModuleHull = HullHandle.try_get<FCompShipModuleHull>())
	{
		if (CShipTemplate.Weapons.Num() > CShipModuleHull->Weapons.Num())
		{
			LOG();
			return {};
		}

		for (int i = 0; i < CShipTemplate.Weapons.Num(); i++)
		{
			auto&		TemplateModule = CShipTemplate.Weapons[i];
			FShipWeapon ShipWeapon;
			ShipWeapon.BreedId = TemplateModule.BreedId;
			ShipWeapon.Size = CShipModuleHull->Weapons[i].Size;
			ShipWeapon.BreedTier = TemplateModule.Tier;

			Res.CSpaceObject.Weapons.Add(ShipWeapon);
		}
	}

	return Res;
}

FText UHelperShipTemplate::CalcShipInfoText(FEnttShipTemplateHandle const& ShipTemplate)
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto ShipInfoSum = CalcShipInfoSum(ShipTemplate);

	FFormatNamedArguments Arguments;

	Arguments.Add("Cost", FText::FromString(CostTextCalc(ShipInfoSum.Cost)));

	Arguments.Add("VolumeFull", FText::AsNumber(ShipInfoSum.VolumeFull));
	Arguments.Add("VolumeUsed", FText::AsNumber(ShipInfoSum.VolumeUsed));
	Arguments.Add("Mass", UNumFmt::NumFmtStats(ShipInfoSum.Mass));
	Arguments.Add("Energy", UNumFmt::NumFmtStats(ShipInfoSum.Energy));
	Arguments.Add("Thrust", UNumFmt::NumFmtStats(ShipInfoSum.Thrust));

	Arguments.Add("Hp", UNumFmt::NumFmtStats(ShipInfoSum.CSpaceObject.Hp.Max));
	Arguments.Add("Sp", UNumFmt::NumFmtStats(ShipInfoSum.CSpaceObject.Sp.Max));
	Arguments.Add("Ap", UNumFmt::NumFmtStats(ShipInfoSum.CSpaceObject.Ap.Max));
	Arguments.Add("Speed", UNumFmt::NumFmtStats(ShipInfoSum.CShip.SpeedForwardMax));
	Arguments.Add("Dps", UNumFmt::NumFmtStats(ShipInfoSum.Dps));

	FText TextFmt = OptigameData->GetFTextFromId("ship_designer_ship_info");
	return FText::Format(TextFmt, Arguments);
}

TArray<FShipTemplateModule> UHelperShipTemplate::GetPossibleModulesForSlot(FEnttHandle const& EmpireRaw,
	FShipTemplateModuleSlotRefIdx const&													  SlotIdx)
{
	FEnttEmpireHandle Empire(EmpireRaw);
	if (!Empire.IsValid())
		return {};

	auto Res = Empire.GetPossibleShipModuleTypes();

	auto BreedRegistry = Empire.registry()->ctx().get<LBreedRegistry*>();

	Res.RemoveAll([&](FShipTemplateModule& E) {
		auto Breed = BreedRegistry->GetBreed(E.BreedId);
		if (!Breed.IsValid())
			return true;
		auto CShipModule = Breed.try_get<FCompShipModule>();
		if (!CShipModule)
			return true;

		switch (CShipModule->Type)
		{
			case EShipModuleType::Engine:
			case EShipModuleType::Reactor:
			case EShipModuleType::Armor:
			case EShipModuleType::Shield:
				return SlotIdx.Type != EShipTemplateModuleSlotType::Module;
			case EShipModuleType::Hull:
				return SlotIdx.Type != EShipTemplateModuleSlotType::Hull;
			case EShipModuleType::Weapon:
				return SlotIdx.Type != EShipTemplateModuleSlotType::Weapon;
			case EShipModuleType::HullStation:
			case EShipModuleType::Shipyard:
				return true;
		}
		return true;
	});

	return Res;
}

FShipTemplateModule* GetModulePtr(FCompShipTemplate& CShipTemplate, FShipTemplateModuleSlotRefIdx const& SlotIdx)
{
	FShipTemplateModule* ModulePtr = nullptr;

	switch (SlotIdx.Type)
	{
		case EShipTemplateModuleSlotType::Hull:
			ModulePtr = &CShipTemplate.Hull;
			break;
		case EShipTemplateModuleSlotType::Module:
			if (CShipTemplate.Modules.IsValidIndex(SlotIdx.Idx))
				ModulePtr = &CShipTemplate.Modules[SlotIdx.Idx];
			break;
		case EShipTemplateModuleSlotType::Weapon:
			if (CShipTemplate.Weapons.IsValidIndex(SlotIdx.Idx))
				ModulePtr = &CShipTemplate.Weapons[SlotIdx.Idx];
			break;
		default:
			ModulePtr = nullptr;
			break;
	}

	return ModulePtr;
}

void UHelperShipTemplate::SetModuleShip(
	FEnttShipTemplateHandle const&		 ShipTemplate,
	FShipTemplateModuleSlotRefIdx const& SlotIdx,
	FShipTemplateModule const&			 Module)
{
	if (!ShipTemplate.IsValid())
	{
		LOG("No ship");
		return;
	}

	auto& CShipTemplate = ShipTemplate.get<FCompShipTemplate>();

	auto ModulePtr = GetModulePtr(CShipTemplate, SlotIdx);

	// If add to virtual last slot in modules
	if (!ModulePtr && SlotIdx.Type == EShipTemplateModuleSlotType::Module && SlotIdx.Idx == CShipTemplate.Modules.Num() && Module.BreedId != NAME_None)
	{
		CShipTemplate.Modules.AddDefaulted();
		ModulePtr = &CShipTemplate.Modules[SlotIdx.Idx];
	}

	if (!ModulePtr)
	{
		LOG("Bad slot Idx");
		return;
	}

	auto ModuleFixed = Module;
	ModuleFixed.Size = FMath::Max(ModuleFixed.Size, 1);
	if (SlotIdx.Type == EShipTemplateModuleSlotType::Hull)
		ModuleFixed.Size = 0;

	*ModulePtr = ModuleFixed;

	if (SlotIdx.Type == EShipTemplateModuleSlotType::Hull || SlotIdx.Type == EShipTemplateModuleSlotType::Weapon)
	{
		UpdateWeaponsSlots(CShipTemplate);
	}

	if (SlotIdx.Type == EShipTemplateModuleSlotType::Module && Module.BreedId == NAME_None)
	{
		if (!CShipTemplate.Modules.IsValidIndex(SlotIdx.Idx))
		{
			LOG();
			return;
		}
		CShipTemplate.Modules.RemoveAt(SlotIdx.Idx);
	}
}

FShipTemplateModule UHelperShipTemplate::GetModuleShip(FEnttShipTemplateHandle const& ShipTemplate,
	FShipTemplateModuleSlotRefIdx const&											  SlotIdx)
{
	if (!ShipTemplate.IsValid())
	{
		LOG("No ship");
		return {};
	}

	auto& CShipTemplate = ShipTemplate.get<FCompShipTemplate>();
	return GetModuleShipC(CShipTemplate, SlotIdx);
}

FShipTemplateModule UHelperShipTemplate::GetModuleShipC(FCompShipTemplate const& CShipTemplate,
	FShipTemplateModuleSlotRefIdx const&										 SlotIdx)
{
	FCompShipTemplate Inst = CShipTemplate;
	auto			  ModulePtr = GetModulePtr(Inst, SlotIdx);

	if (!ModulePtr)
		return {};
	return *ModulePtr;
}

FShipSlotModuleDrawInfo UHelperShipTemplate::GetShipSlotModuleDrawInfo(
	FEnttShipTemplateHandle const&		 ShipTemplate,
	FShipTemplateModuleSlotRefIdx const& SlotIdx)
{
	if (!ShipTemplate.IsValid())
	{
		LOG("No ship");
		return {};
	}
	auto& CShipTemplate = ShipTemplate.get<FCompShipTemplate>();

	auto BreedRegistry = ShipTemplate.registry()->ctx().get<LBreedRegistry*>();
	auto ModuleBreed = BreedRegistry->GetBreed(CShipTemplate.Hull.BreedId);
	if (!ModuleBreed)
	{
		LOG("No ship");
		return {};
	}

	FShipSlotModuleDrawInfo Res;

	switch (SlotIdx.Type)
	{
		case EShipTemplateModuleSlotType::Hull:
			Res.bDynSize = false;
			Res.bTier = false;
			Res.DefaultSize = 0;
			break;
		case EShipTemplateModuleSlotType::Module:
			if (CShipTemplate.Modules.IsValidIndex(SlotIdx.Idx))
			{
				Res.bDynSize = true;
				Res.bTier = true;
				Res.DefaultSize = 1;
			}
			break;
		case EShipTemplateModuleSlotType::Weapon:
			if (CShipTemplate.Weapons.IsValidIndex(SlotIdx.Idx))
			{
				auto HullBreed = BreedRegistry->GetBreed(CShipTemplate.Hull.BreedId);
				if (!HullBreed)
				{
					LOG("No ship");
					return {};
				}
				auto CShipModuleHull = HullBreed.try_get<FCompShipModuleHull>();
				if (!CShipModuleHull)
				{
					LOG();
					return {};
				}
				if (!CShipModuleHull->Weapons.IsValidIndex(SlotIdx.Idx))
				{
					LOG();
					return {};
				}

				Res.bDynSize = false;
				Res.bTier = true;
				Res.DefaultSize = WeaponSizeToInt(CShipModuleHull->Weapons[SlotIdx.Idx].Size);
			}
			break;
		default:
			break;
	}

	return Res;
}

void UHelperShipTemplate::UpdateWeaponsSlots(FEnttShipTemplateHandle const& ShipTemplate)
{
	if (!ShipTemplate.IsValid())
	{
		LOG("No ship");
		return;
	}
	auto& CShipTemplate = ShipTemplate.get<FCompShipTemplate>();
	UpdateWeaponsSlots(CShipTemplate);
}

void UHelperShipTemplate::UpdateWeaponsSlots(FCompShipTemplate& CShipTemplate)
{
	// CShipTemplate.Weapons.SetNum(0);
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();

	auto HullModuleBreed = OptigameData->BreedRegistry.GetBreed(CShipTemplate.Hull.BreedId);
	if (!HullModuleBreed.IsValid())
	{
		LOG();
		return;
	}
	auto CShipModuleHull = HullModuleBreed.try_get<FCompShipModuleHull>();
	if (!CShipModuleHull)
	{
		LOG();
		return;
	}

	int WeaponsNum = CShipModuleHull->Weapons.Num();

	CShipTemplate.Weapons.SetNum(WeaponsNum);

	for (int i = 0; i < WeaponsNum; i++)
	{
		if (CShipTemplate.Weapons[i].BreedId != NAME_None)
			CShipTemplate.Weapons[i].Size = GMapWeaponSize[CShipModuleHull->Weapons[i].Size];
	}
}

FCompShipModuleHull UHelperShipTemplate::GetBreedModuleHull(FName HullId)
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto Breed = OptigameData->BreedRegistry.GetBreed(HullId);
	if (!Breed.IsValid())
		return {};

	auto CShipModuleHull = Breed.try_get<FCompShipModuleHull>();
	if (!CShipModuleHull)
		return {};
	return *CShipModuleHull;
}

bool UHelperShipTemplate::Equal_FShipTemplateModuleSlotRefIdx(const FShipTemplateModuleSlotRefIdx& A,
	const FShipTemplateModuleSlotRefIdx&														   B)
{
	return A == B;
}