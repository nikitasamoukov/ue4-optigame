// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"

#include "Components/Button.h"

#include "Optigame/OptigamePch.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "Optigame/Galaxy/Handles/FEnttEmpireHandle.h"

#include "HelperShipTemplate.generated.h"

class LBreedRegistry;
USTRUCT(BlueprintType)
struct FShipInfoStr
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int VolumeFull = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int VolumeUsed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Mass = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Energy = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FCompSpaceObject CSpaceObject;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FCompShip CShip;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Thrust = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Dps = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Build = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Modules = 0;	// Only station
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int ModulesMax = 0; // Only station
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FResPile Cost;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FCompPhys CPhys;
};

UENUM(BlueprintType)
enum class EShipTemplateModuleSlotType : uint8
{
	None,
	Hull,
	Weapon,
	Module,
};

USTRUCT(BlueprintType)
struct FShipTemplateModuleSlotRefIdx
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EShipTemplateModuleSlotType Type = EShipTemplateModuleSlotType::None;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Idx = 0;

	bool operator==(const FShipTemplateModuleSlotRefIdx& ShipTemplateModuleSlotRefIdx) const = default;
};

USTRUCT(BlueprintType)
struct FShipSlotModuleDrawInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bTier = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bDynSize = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int DefaultSize = 1;
};

USTRUCT(BlueprintType)
struct FShipModuleFullInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Name;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FText Info;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TWeakObjectPtr<UTexture2D> Image = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool HaveTier = false;
};

UCLASS()
class UHelperShipTemplate : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static FString CostTextCalc(FResPile const& Cost);
	UFUNCTION(BlueprintCallable)
	static void CreateEmpireEmptyBuildShip(FEnttHandle const& Empire);
	UFUNCTION(BlueprintCallable)
	static void DestroyEmpireBuildShip(FEnttHandle const& Empire);
	UFUNCTION(BlueprintCallable)
	static void SaveEmpireBuildShip(FEnttHandle const& Empire, FString Name);
	UFUNCTION(BlueprintCallable)
	static FText GetShipModuleInfo(FShipTemplateModule const& Module, bool IsStation);
	UFUNCTION(BlueprintCallable)
	static void GetModuleFullInfo(FShipTemplateModule const& Module, bool IsStation, bool& Success, FShipModuleFullInfo& FullInfo);

	static FShipInfoStr CalcShipInfoSum(FEnttHandle const& ShipTemplate);
	UFUNCTION(BlueprintCallable)
	static FShipInfoStr CalcShipInfoSum(FCompShipTemplate const& CShipTemplate);
	UFUNCTION(BlueprintCallable)
	static FText CalcShipInfoText(FEnttShipTemplateHandle const& ShipTemplate);

	UFUNCTION(BlueprintCallable)
	static TArray<FShipTemplateModule> GetPossibleModulesForSlot(FEnttHandle const& EmpireRaw, FShipTemplateModuleSlotRefIdx const& SlotIdx);

	// Set ship module at slot. Auto resize modules array. Also used for remove.
	UFUNCTION(BlueprintCallable)
	static void SetModuleShip(FEnttShipTemplateHandle const& ShipTemplate, FShipTemplateModuleSlotRefIdx const& SlotIdx, FShipTemplateModule const& Module);
	UFUNCTION(BlueprintCallable)
	static FShipTemplateModule GetModuleShip(FEnttShipTemplateHandle const& ShipTemplate, FShipTemplateModuleSlotRefIdx const& SlotIdx);
	UFUNCTION(BlueprintCallable)
	static FShipTemplateModule GetModuleShipC(FCompShipTemplate const& CShipTemplate, FShipTemplateModuleSlotRefIdx const& SlotIdx);
	UFUNCTION(BlueprintCallable)
	static FShipSlotModuleDrawInfo GetShipSlotModuleDrawInfo(FEnttShipTemplateHandle const& ShipTemplate, FShipTemplateModuleSlotRefIdx const& SlotIdx);

	UFUNCTION(BlueprintCallable)
	static void UpdateWeaponsSlots(FEnttShipTemplateHandle const& ShipTemplate);
	static void UpdateWeaponsSlots(FCompShipTemplate& CShipTemplate);

	UFUNCTION(BlueprintCallable)
	static FCompShipModuleHull GetBreedModuleHull(FName HullId);

	UFUNCTION(BlueprintPure, meta = (DisplayName = "Equal FShipTemplateModuleSlotRefIdx", CompactNodeTitle = "==", Keywords = "== equal"), Category = "Math")
	static bool Equal_FShipTemplateModuleSlotRefIdx(const FShipTemplateModuleSlotRefIdx& A, const FShipTemplateModuleSlotRefIdx& B);
};
