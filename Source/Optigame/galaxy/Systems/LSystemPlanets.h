// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"

class LSystemPlanets
{
protected:
	FRegistry& Reg;

public:
	explicit LSystemPlanets(FRegistry& Reg)
		: Reg(Reg) {}

	// System funcs

	void Update();

protected:
	void BuildingsCalcActiveConsumeWorkers();
	void BuildingsCalcProduce();
	void BuildingsHouseCalc();
	void PlanetsApplyIncome();
	void BuildQueueProcess();
};