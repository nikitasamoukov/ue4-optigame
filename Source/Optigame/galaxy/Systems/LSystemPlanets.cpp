// Fill out your copyright notice in the Description page of Project Settings.

#include "LSystemPlanets.h"
#include "Optigame/Galaxy/BreedRegistry/LBreedRegistry.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"
#include "Optigame/Galaxy/Components/ComponentsBuilding.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsEmpire.h"
#include "Optigame/Galaxy/Components/ComponentsBuildQueue.h"

void LSystemPlanets::Update()
{
	BuildingsCalcActiveConsumeWorkers();
	BuildingsCalcProduce();
	BuildingsHouseCalc();
	PlanetsApplyIncome();
	BuildQueueProcess();
}

void LSystemPlanets::BuildingsCalcActiveConsumeWorkers()
{
	auto& BreedRegistry = *Reg.ctx().get<LBreedRegistry*>();

	for (auto [Colony, CColony, COwner] : Reg.view<FCompColony, FCompOwner>().each())
	{
		if (!Reg.valid(COwner.Empire) || !Reg.all_of<FCompEmpire>(COwner.Empire))
		{
			LOG();
			continue;
		}
		auto& CEmpire = Reg.get<FCompEmpire>(COwner.Empire);

		// 1-reset self
		for (auto& ResCount : CColony.ResourcesConsume)
			ResCount.Value = 0;
		CColony.WorkerPlaces = 0;

		// Per building update
		for (auto E : CColony.Buildings)
		{
			if (!Reg.valid(E))
				continue;
			if (!Reg.all_of<FCompBuilding>(E))
			{
				LOG();
				continue;
			}

			auto& CBuilding = Reg.get<FCompBuilding>(E);
			CBuilding.IsActive = !CBuilding.IsDisabled;

			if (CBuilding.IsDisabled)
				continue;

			// 1-LCBuildingWorkers
			if (auto CBuildingWorkers = Reg.try_get<FCompBuildingWorkers>(E))
			{
				if (CColony.WorkerPlaces + CBuildingWorkers->Workers > CColony.Population)
				{
					CBuilding.IsActive = false;
				}
				CColony.WorkerPlaces += CBuildingWorkers->Workers;
			}

			if (!CBuilding.IsActive)
				continue;

			if (auto CBuildingConsume = Reg.try_get<FCompBuildingConsume>(E))
			{
				bool CanRun = true;
				for (auto Pair : CBuildingConsume->In.Resources)
				{
					// LOG(Pair.TypeId.ToString()+" "+FString::Sanitizedouble(Pair.Count));
					auto Breed = BreedRegistry.GetBreed(Pair.Key);
					if (!Breed)
					{
						LOG();
						continue;
					}
					auto CResourceType = Breed.try_get<FCompResourceType>();
					if (!CResourceType)
					{
						LOG();
						continue;
					}

					if (CResourceType->Local)
					{
						if (CColony.ResourcesConsume.FindOrAdd(Pair.Key) + Pair.Value > CColony.ResourcesProduce.FindOrAdd(Pair.Key))
						{
							CanRun = false;
						}
					}
					else
					{
						if (CEmpire.Resources.FindOrAdd(Pair.Key) <= 0)
						{
							CanRun = false;
						}
					}
				}

				CBuilding.IsActive &= CanRun;
			}
		}
	}
}

void LSystemPlanets::BuildingsCalcProduce()
{
	// 1-reset
	for (auto [E, CColony] : Reg.view<FCompColony>().each())
	{
		for (auto& ResCount : CColony.ResourcesProduce)
			ResCount.Value = 0;
	}

	// 2-LCBuildingProduct
	for (auto [E, CBuilding, CBuildingProduct] : Reg.view<FCompBuilding, FCompBuildingProduct>().each())
	{
		if (!CBuilding.IsActive)
			continue;

		if (!Reg.valid(CBuilding.Planet))
		{
			LOG();
			continue;
		}
		auto CColony = Reg.try_get<FCompColony>(CBuilding.Planet);
		if (!CColony)
		{
			LOG();
			continue;
		}

		for (auto& Pair : CBuildingProduct.Out.Resources)
		{
			CColony->ResourcesProduce.FindOrAdd(Pair.Key) += Pair.Value;
		}
	}
	for (auto [E, CBuilding, CBuildingConsume] : Reg.view<FCompBuilding, FCompBuildingConsume>().each())
	{
		if (!CBuilding.IsActive)
			continue;

		if (!Reg.valid(CBuilding.Planet))
		{
			LOG();
			continue;
		}
		auto CColony = Reg.try_get<FCompColony>(CBuilding.Planet);
		if (!CColony)
		{
			LOG();
			continue;
		}

		for (auto& Pair : CBuildingConsume.In.Resources)
		{
			CColony->ResourcesConsume.FindOrAdd(Pair.Key) += Pair.Value;
		}
	}
}

void LSystemPlanets::BuildingsHouseCalc()
{
	for (auto [E, CColony] : Reg.view<FCompColony>().each())
	{
		CColony.House = 0;
	}

	for (auto [E, CBuilding, CBuildingHouse] : Reg.view<FCompBuilding, FCompBuildingHouse>().each())
	{
		if (CBuilding.IsDisabled)
			continue;

		if (!Reg.valid(CBuilding.Planet))
		{
			LOG();
			continue;
		}
		auto CColony = Reg.try_get<FCompColony>(CBuilding.Planet);
		if (!CColony)
		{
			LOG();
			continue;
		}

		CColony->House += CBuildingHouse.Size;
	}
}

void LSystemPlanets::PlanetsApplyIncome()
{
	for (auto [E, CColony, COwner] : Reg.view<FCompColony, FCompOwner>().each())
	{
		if (!Reg.valid(COwner.Empire) || !Reg.all_of<FCompEmpire>(COwner.Empire))
		{
			LOG();
			continue;
		}
		auto& CEmpire = Reg.get<FCompEmpire>(COwner.Empire);

		for (auto& ResCount : CColony.ResourcesProduce)
		{
			CEmpire.ResourcesChange.FindOrAdd(ResCount.Key) += ResCount.Value;
		}
		for (auto& ResCount : CColony.ResourcesConsume)
		{
			CEmpire.ResourcesChange.FindOrAdd(ResCount.Key) -= ResCount.Value;
		}

		for (auto It = CColony.ResourcesProduce.CreateIterator(); It; ++It)
			if (It->Value == 0)
				It.RemoveCurrent();
		for (auto It = CColony.ResourcesConsume.CreateIterator(); It; ++It)
			if (It->Value == 0)
				It.RemoveCurrent();
	}
}

void LSystemPlanets::BuildQueueProcess()
{
	double DailyIncomeMult = 1.0f / FMath::Max(1, GOptigameVars.DaysInMonth);

	for (auto [E, CColony] : Reg.view<FCompColony>().each())
	{
		CColony.BuildToStation = 0;
	}

	// Planet BuildQueue upd
	for (auto [E, CColony, CBuildQueue] : Reg.view<FCompColony, FCompBuildQueue>().each())
	{
		double BuildBase = CColony.ResourcesProduce.FindOrAdd("res_build");
		double BuildToApply = BuildBase * DailyIncomeMult;

		double BuildExtra = CBuildQueue.AddBuild({ Reg, E }, BuildToApply);

		if (BuildToApply == BuildExtra)
			CColony.BuildToStation = BuildBase;
		else
			CColony.BuildToStation = BuildExtra / DailyIncomeMult;
	}

	// Planet to station "build"
	for (auto [E, CColony] : Reg.view<FCompColony>().each())
	{
		if (!Reg.valid(CColony.Station))
			continue;
		if (!Reg.all_of<FCompStation>(CColony.Station))
			continue;

		Reg.get<FCompStation>(CColony.Station).BuildPlanet = CColony.BuildToStation;
	}

	// Planet to station "build"
	for (auto [E, CStation, CBuildQueue] : Reg.view<FCompStation, FCompBuildQueue>().each())
	{
		double BuildFull = CStation.Build + CStation.BuildPlanet;

		double BuildToApply = BuildFull * DailyIncomeMult;

		CBuildQueue.AddBuild({ Reg, E }, BuildToApply);
	}
}