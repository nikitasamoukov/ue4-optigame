// Fill out your copyright notice in the Description page of Project Settings.

#include "LSystemSpace.h"

#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "Optigame/Galaxy/Helpers/HelperShip.h"

bool CalcCanCatch(FCompTransform const& CTrs, FVector const& PosTarget, double Speed, double AngularSpeed)
{
	double Dist = FVector::Distance(CTrs.Pos, PosTarget);
	FVector EnemyDir = PosTarget - CTrs.Pos;
	EnemyDir.Normalize();
	FVector ForwardDir = CTrs.Quat.GetForwardVector();
	double Angle = FMath::Acos(FVector::DotProduct(ForwardDir, EnemyDir));
	// double CircleP = Speed / AngularSpeed * PI * 2;
	double CircleR = Speed / AngularSpeed;

	if (Angle > PI / 2)
		return false; // Not in front

	double CircleRagAngle = Angle - PI / 2;

	FVector2D CirclePos(FMath::Cos(CircleRagAngle) * CircleR, FMath::Sin(CircleRagAngle) * CircleR);

	if (FVector2D::Distance(CirclePos, { Dist, 0 }) < CircleR)
		return false; // Cant simple forward run

	return true;
}

bool ShipMoveIsAccelerate(double Speed, double Acc, double Dist)
{
	/*
	X
	XX
	XXX
	*/

	if (Speed < 0)
		return true;

	int StepsFull = floor(Speed / FMath::Max(0.000001f, Acc));
	double TrailIfAcc =
		Speed + Acc + Acc * (StepsFull * (StepsFull + 1) / 2.0f) + (StepsFull + 1) * (Speed - StepsFull * Acc);

	return TrailIfAcc < Dist;
}

void LSystemSpace::ShipIdleUpd(FEntity EShip, FCompTransform& CTransform, FCompShip& CShip, FCompShipController& CShipController)
{
	double Acc = CShip.SpeedForwardMax * GOptigameVars.Ships.AccelerateMul * GOptigameVars.FrameTime * GOptigameVars.FrameTime;

	auto CPhys = Reg.try_get<FCompPhys>(EShip);
	if (!CPhys)
		return;
	FVector VelDir(0);
	double VelSpeed = 0;
	CPhys->Vel.ToDirectionAndLength(VelDir, VelSpeed);

	double VelSpeedNew = 0;

	if (VelSpeed > Acc)
		VelSpeedNew = VelSpeed - Acc;

	CPhys->Vel = VelDir * VelSpeedNew;
}

bool LSystemSpace::ShipMoveUpd(FEntity Star, FEntity EShip, FCompTransform& CTransform, FCompShip& CShip, FCompShipController& CShipController, FVector TargetPos)
{
	auto ForwardDir = CTransform.Quat.GetForwardVector();

	// Rotate
	FVector TargetDir(TargetPos - CTransform.Pos);
	bool IsTargetDirNorm = TargetDir.Normalize();
	double TargetAngle = FMath::Acos(FVector::DotProduct(TargetDir, ForwardDir));
	if (IsTargetDirNorm)
	{
		FQuat TargetQuat = TargetDir.ToOrientationQuat();
		// FVector TestV = TargetQuat.GetForwardVector();
		double FrameRotateAngle = CShip.SpeedRotateMax * GOptigameVars.FrameTime;
		if (TargetAngle > FrameRotateAngle)
		{
			CTransform.Quat = FQuat::Slerp(CTransform.Quat, TargetQuat, FrameRotateAngle / TargetAngle);
		}
		else
		{
			CTransform.Quat = TargetQuat;
		}
	}

	// Accelerate
	auto CPhys = Reg.try_get<FCompPhys>(EShip);
	if (!CPhys)
		return true;
	auto& Vel = CPhys->Vel;

	double ForwardSpeed = FVector::DotProduct(ForwardDir, Vel);
	FVector ForwardPart = ForwardDir * ForwardSpeed;
	FVector SidePart = Vel - ForwardPart;

	double Acc = CShip.SpeedForwardMax * GOptigameVars.Ships.AccelerateMul * GOptigameVars.FrameTime * GOptigameVars.FrameTime;

	double NewForwardSpeed = ForwardSpeed;

	bool CanCatch = CalcCanCatch(CTransform, TargetPos, CShip.SpeedForwardMax, CShip.SpeedRotateMax);
	bool IsAcc = ShipMoveIsAccelerate(ForwardSpeed, Acc, FVector::Dist(CTransform.Pos, TargetPos));
	bool IsSlowing = false;

	if (CanCatch && IsAcc)
	{
		if (ForwardSpeed < CShip.SpeedForwardMax * GOptigameVars.FrameTime)
		{
			if (ForwardSpeed + Acc < CShip.SpeedForwardMax * GOptigameVars.FrameTime)
				NewForwardSpeed = ForwardSpeed + Acc;
			else
				NewForwardSpeed = CShip.SpeedForwardMax * GOptigameVars.FrameTime;
		}
		else
		{
			if (ForwardSpeed - Acc > CShip.SpeedForwardMax * GOptigameVars.FrameTime)
			{
				NewForwardSpeed = ForwardSpeed - Acc;
				IsSlowing = true;
			}
			else
				NewForwardSpeed = CShip.SpeedForwardMax * GOptigameVars.FrameTime;
		}
	}
	else
	{
		if (ForwardSpeed > Acc)
			NewForwardSpeed = ForwardSpeed - Acc;
		else
		{
			NewForwardSpeed = 0;
		}
		IsSlowing = true;
	}

	FVector NewForwardPart = ForwardDir * NewForwardSpeed;

	FVector SidePartDir(0);
	double SidePartLength = 0;

	FVector NewSidePart(0);
	SidePart.ToDirectionAndLength(SidePartDir, SidePartLength);
	bool IsSideSpeedSoBig = SidePartLength > Acc;
	if (IsSideSpeedSoBig)
		NewSidePart = SidePartDir * (SidePartLength - Acc);

	FVector NewVel = NewForwardPart + NewSidePart;
	Vel = NewVel;

	bool IsEndMove =
		IsSlowing &&					   // Slowing phase
		CanCatch && TargetAngle < 0.001 && // Rotate to target end
		!IsSideSpeedSoBig;				   // Not drifting
	return !IsEndMove;
}

void LSystemSpace::ShipUpdatePath(FEntity Star, FEntity EShip, FEntity TargetStar)
{
}

void LSystemSpace::ShipControllerUpdate(FEntity Star, FEntity Ship, FCompTransform& CTransform, FCompShip& CShip, FCompShipController& CShipController)
{
	if (CShipController.Actions.Num() == 0)
	{
		ShipIdleUpd(Ship, CTransform, CShip, CShipController);
		return;
	}

	auto& Action = CShipController.Actions[0];
	bool IsExecuteContinue = false;

	Visit([&]<typename Type>(Type& ValRaw) {
		if constexpr (std::is_same_v<Type, FActionMove>)
		{
			FActionMove& Val = ValRaw;
			// bool CanCatch = CalcCanCatch(CTransform, Val.Pos, CShip.SpeedForwardMax, FMath::DegreesToRadians(60) * GOptigameVars.ShipRotateMul);
			// LOG("CanCatch:" + FString::FromInt(CanCatch));
			if (Val.GamePos.Star == Star)
			{
				IsExecuteContinue = ShipMoveUpd(Star, Ship, CTransform, CShip, CShipController, Val.GamePos.Pos);
			}
			else
			{
				ValRaw.UpdateBase(Reg, { Star, CTransform.Pos });
				// ShipUpdatePath(Star, Ship, Val.GamePos.Star);
				if (Val.Path.Num() > 0)
				{
					if (!Reg.valid(Val.WarpZone))
					{
						LOG();
						return;
					}
					auto ZoneCTransform = Reg.try_get<FCompTransform>(Val.WarpZone);
					if (!ZoneCTransform)
					{
						LOG();
						return;
					}
					auto CShipWarpOffset = Reg.try_get<FCompShipWarpOffset>(Ship);
					if (!CShipWarpOffset)
					{
						LOG();
						return;
					}

					FVector WarpPos = ZoneCTransform->Pos + CShipWarpOffset->Offset;

					bool MoveToWarpPointContinue = ShipMoveUpd(Star, Ship, CTransform, CShip, CShipController, WarpPos);

					if (!MoveToWarpPointContinue)
					{
						Val.Path.RemoveAt(0);
						Reg.emplace<FCompShipWarpExec>(Ship, FCompShipWarpExec{ .Star = Star, .WarpZone = Val.WarpZone });
					}
					IsExecuteContinue = true;
				}
				else // No way???
					IsExecuteContinue = false;
			}
		}
		else if constexpr (std::is_same_v<Type, FActionAttack>)
		{
			LOG("Att");
		}
		else
			static_assert(always_false_v<Type>, "non-exhaustive visitor!");
	},
		Action);

	if (!IsExecuteContinue)
	{
		CShipController.Actions.RemoveAt(0);
	}
}

void LSystemSpace::Update()
{
	for (auto [Star, CStar] : Reg.view<FCompStar>().each())
	{
		for (auto& EShip : CStar.Ships)
		{
			auto [CTransform, CShip, CShipController] = Reg.try_get<FCompTransform, FCompShip, FCompShipController>(EShip);
			if (!CTransform || !CShip || !CShipController)
			{
				LOG();
				continue;
			}
			ShipControllerUpdate(Star, EShip, *CTransform, *CShip, *CShipController);
		}
	}

	for (auto [Ship, CShipWarpExec] : Reg.view<FCompShipWarpExec>().each())
	{
		UHelperShip::ShipWarpExec(Reg, CShipWarpExec.Star, Ship, CShipWarpExec.WarpZone);
		Reg.remove<FCompShipWarpExec>(Ship);
	}
}