// Fill out your copyright notice in the Description page of Project Settings.

#include "LSystemPositionCopyOldPos.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"

void LSystemPositionCopyOldPos::Update(FRegistry& Reg)
{
	for (auto [E, CTransform] : Reg.view<FCompTransform>().each())
	{
		CTransform.CopyToPrev();
	}
}