// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"

class LSystemSpace
{
protected:
	FRegistry& Reg;

public:
	explicit LSystemSpace(FRegistry& Reg)
		: Reg(Reg) {}

	// System funcs

	void Update();

protected:
	void ShipIdleUpd(FEntity EShip, FCompTransform& CTransform, FCompShip& CShip, FCompShipController& CShipController);
	// Return false when move end
	bool ShipMoveUpd(FEntity Star, FEntity EShip, FCompTransform& CTransform, FCompShip& CShip, FCompShipController& CShipController, FVector TargetPos);
	void ShipUpdatePath(FEntity Star, FEntity EShip, FEntity TargetStar);
	void ShipControllerUpdate(FEntity Star, FEntity Ship, FCompTransform& CTransform, FCompShip& CShip, FCompShipController& CShipController);
};