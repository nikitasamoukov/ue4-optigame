// Fill out your copyright notice in the Description page of Project Settings.

#include "LSystemOrbits.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"

void LSystemOrbits::Update()
{
	for (auto [E, COrbit, CTransform] : Reg.view<FCompOrbit, FCompTransform>().each())
	{
		COrbit.Update();
		CTransform.Pos = COrbit.CalculatePosition();
	}

	for (auto [E, CStation, CTransform] : Reg.view<FCompStation, FCompTransform>().each())
	{
		if (CStation.Planet == entt::null)
			continue;
		auto CPlanetTrs = Reg.try_get<FCompTransform>(CStation.Planet);
		if (!CPlanetTrs)
			continue;
		CTransform.Pos = CPlanetTrs->Pos;
	}
}