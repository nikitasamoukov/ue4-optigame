// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"

class LSystemEmpires
{
protected:
	FRegistry& Reg;

public:
	LSystemEmpires(FRegistry& Reg)
		: Reg(Reg) {}

	// System funcs

	void UpdateResetIncome();
	void UpdateApplyIncome();
};