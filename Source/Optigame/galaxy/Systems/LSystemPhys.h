// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"

class LSystemPhys
{
protected:
	FRegistry& Reg;

public:
	LSystemPhys(FRegistry& Reg)
		: Reg(Reg) {}

	// System funcs

	void Update();
};