// Fill out your copyright notice in the Description page of Project Settings.

#include "LSystemPhys.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"

void LSystemPhys::Update()
{
	for (auto [E, CPhys, CTransform] : Reg.view<FCompPhys, FCompTransform>().each())
	{
		CPhys.Update(CTransform);
	}
}