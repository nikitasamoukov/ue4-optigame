// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"

class LSystemOrbits
{
protected:
	FRegistry& Reg;

public:
	LSystemOrbits(FRegistry& Reg)
		: Reg(Reg) {}

	// System funcs

	void Update();
};