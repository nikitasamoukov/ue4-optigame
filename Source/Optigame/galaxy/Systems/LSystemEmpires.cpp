// Fill out your copyright notice in the Description page of Project Settings.

#include "LSystemEmpires.h"
#include "Optigame/Galaxy/Components/ComponentsEmpire.h"

void LSystemEmpires::UpdateResetIncome()
{
	for (auto [E, CEmpire] : Reg.view<FCompEmpire>().each())
	{
		for (auto& Pair : CEmpire.ResourcesChange)
			Pair.Value = 0;
	}
}

void LSystemEmpires::UpdateApplyIncome()
{
	double DailyIncomeMult = 1.0f / FMath::Max(1, GOptigameVars.DaysInMonth);
	for (auto [E, CEmpire] : Reg.view<FCompEmpire>().each())
	{
		for (auto& Pair : CEmpire.ResourcesChange)
			CEmpire.Resources.FindOrAdd(Pair.Key) += Pair.Value * DailyIncomeMult;
	}
}