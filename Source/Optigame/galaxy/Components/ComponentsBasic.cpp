#include "ComponentsBasic.h"

FVector FCompTransform::CalcMixPos(double ProgressToCurrentFrame) const
{
	return Pos * ProgressToCurrentFrame + PrevPos * (1 - ProgressToCurrentFrame);
}

FQuat FCompTransform::CalcMixQuat(double ProgressToCurrentFrame) const
{
	return FQuat::Slerp(PrevQuat, Quat, ProgressToCurrentFrame);
}

void FCompTransform::CopyToPrev()
{
	PrevPos = Pos;
	PrevQuat = Quat;
}

void FCompCurrentTimePos::FromTrs(FCompTransform const& CTransform, double ProgressToCurrentFrame)
{
	Pos = CTransform.CalcMixPos(ProgressToCurrentFrame);
	Quat = CTransform.CalcMixQuat(ProgressToCurrentFrame);
}

void FCompOrbit::CalcAngularSpeed()
{
	double Period = GOptigameVars.PlanetOrbitMinPeriod * sqrt(Radius / FMath::Max(GOptigameVars.PlanetDistMin, 0.01f));

	OrbitalAngularSpeed = 2 * PI / FMath::Max(Period, 0.01f);
}

void FCompOrbit::Update()
{
	CurrentAngle += GOptigameVars.FrameTime * OrbitalAngularSpeed;
	CurrentAngle = FMath::Fmod(CurrentAngle, 2 * PI);
}

FVector FCompOrbit::CalculatePosition() const
{
	return FVector(FMath::Cos(CurrentAngle), FMath::Sin(CurrentAngle), 0) * Radius;
}

void FResPile::Add(FResPile const& Cost)
{
	for (auto& Pair : Cost.Resources)
	{
		Resources.FindOrAdd(Pair.Key) += Pair.Value;
	}
}

void FResPile::Add(FName Name, double Count)
{
	Resources.FindOrAdd(Name) += Count;
}