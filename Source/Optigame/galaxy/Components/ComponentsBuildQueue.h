#pragma once

#include "Optigame/OptigamePch.h"
#include "ComponentsBasic.h"
#include "ComponentsShip.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "ComponentsBuildQueue.generated.h"

USTRUCT(BlueprintType)
struct FBuildResultBuilding
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Id;
};

USTRUCT(BlueprintType)
struct FBuildResultShip
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FCompShipTemplate ShipTemplate;
};

USTRUCT(BlueprintType)
struct FBuildResultStationModule
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FShipTemplateModule TemplateModule;
};

UENUM(BlueprintType)
enum class EBuildResultUnique : uint8
{
	Station,
	Terraform,
};

USTRUCT(BlueprintType)
struct FBuildResultUnique
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EBuildResultUnique Val;
};

USTRUCT(BlueprintType)
struct FBuildResult
{
	GENERATED_BODY()
public:
	TVariant<FBuildResultBuilding, FBuildResultShip, FBuildResultStationModule, FBuildResultUnique> Val;
};

UCLASS()
class UFBuildResultLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static TArray<FBuildResultBuilding> TryGetBuildResultBuilding(FBuildResult const& BuildResult)
	{
		auto t = BuildResult.Val.TryGet<FBuildResultBuilding>();
		if (t)
			return { *t };
		return {};
	}
	UFUNCTION(BlueprintCallable)
	static TArray<FBuildResultShip> TryGetBuildResultShip(FBuildResult const& BuildResult)
	{
		auto t = BuildResult.Val.TryGet<FBuildResultShip>();
		if (t)
			return { *t };
		return {};
	}
	UFUNCTION(BlueprintCallable)
	static TArray<FBuildResultStationModule> TryGetBuildResultStationModule(FBuildResult const& BuildResult)
	{
		auto t = BuildResult.Val.TryGet<FBuildResultStationModule>();
		if (t)
			return { *t };
		return {};
	}
	UFUNCTION(BlueprintCallable)
	static TArray<FBuildResultUnique> TryGetBuildResultUnique(FBuildResult const& BuildResult)
	{
		auto t = BuildResult.Val.TryGet<FBuildResultUnique>();
		if (t)
			return { *t };
		return {};
	}
};

USTRUCT(BlueprintType)
struct FBuildOperation
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FResPile Cost;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double BuildReady = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double BuildFull = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FBuildResult BuildResult;
};

USTRUCT(BlueprintType)
struct FCompBuildQueue
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FBuildOperation> Queue;
	double					AddBuild(FEnttHandle Object, double BuildToApply);
};