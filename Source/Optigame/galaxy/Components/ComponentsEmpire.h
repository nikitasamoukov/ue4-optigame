#pragma once

#include "ComponentsShip.h"
#include "Optigame/OptigamePch.h"
#include "ComponentsEmpire.generated.h"

struct FResPile;

USTRUCT(BlueprintType)
struct FEmpireIcon
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Texture;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FColor ColorMain;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FColor ColorSecond;
};

USTRUCT(BlueprintType)
struct FCompEmpire
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Name;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEmpireIcon Icon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FEntity> ShipTemplates;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEntity ShipTemplateEditing = entt::null;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FName, double> Resources;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FName, double> ResourcesChange;

	bool IsHaveResources(FResPile const& ResCost);
	void ResourcesCostPay(FResPile const& ResCost);
};

USTRUCT(BlueprintType)
struct FESStationStr
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName HullId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> Modules;
};

USTRUCT(BlueprintType)
struct FESShipsStr
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Name;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Count = 0;
};

USTRUCT(BlueprintType)
struct FESRes
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName HullId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> Modules;
};

USTRUCT(BlueprintType)
struct FCompEmpireSpawnInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Population = 1;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int ColonySize = 10;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FCompShipTemplate> ShipTemplates;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FESShipsStr> Ships;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> Buildings;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FResPile Resources;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FESStationStr Station;
};