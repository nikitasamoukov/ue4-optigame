#pragma once
#include <InstancedStruct.h>

struct FImGuiComponentDrawContext;
class FXmlNode;

struct FFactoryComponentsCCell
{
	FString Name;

	TFunction<void(FImGuiComponentDrawContext const& Ctx)> FuncImguiDraw;
	double ImguiDrawPriority = 10000;
	bool CompIsRef = false;

	using FFuncHaveComp = TFunction<bool(FRegistry const&, FEntity)>;
	using FFuncCopy = TFunction<void(FEnttHandle TemplateHandle, FEnttHandle EntityHandle)>;
	using FFuncUnpackStruct = TFunction<void(FEnttHandle TemplateHandle, FInstancedStruct const& Data)>;

	FFuncHaveComp FuncHaveComp;
	FFuncCopy FuncCopy;
	FFuncUnpackStruct FuncUnpack;
};

struct FFactoryComponents
{
	FFactoryComponents();

	void AddType(entt::type_info Ti, FFactoryComponentsCCell&& CInfo);
	FFactoryComponentsCCell const* GetTypeCell(entt::id_type TiSeq);
	FFactoryComponentsCCell const* GetTypeCell(entt::type_info Ti);
	FFactoryComponentsCCell const* GetTypeCell(FName CompName);
	TMap<entt::id_type, FFactoryComponentsCCell> const& GetAll() { return TypesMap; }

protected:
	TMap<entt::id_type, FFactoryComponentsCCell> TypesMap;
	TMap<FName, entt::type_info> NameToTypeInfo;
};