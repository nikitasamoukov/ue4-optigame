#pragma once

#include "Optigame/OptigamePch.h"
#include "ComponentsPlanet.generated.h"

USTRUCT(BlueprintType)
struct FCompPlanet
{
	GENERATED_BODY()
public:
	enum class EType
	{
		Earth,
		Rock,
		Ice,
		Lava,
		Desert,
		Water,
	};

	EType Type = EType::Earth;

	double Size = 30;

	FString Name = "<Unnamed>";
};

USTRUCT(BlueprintType)
struct FCompColony
{
	GENERATED_BODY()
public:
	FEntity Station = entt::null;

	TArray<FEntity> Buildings;

	int	  Population = 0;
	double PopulationProgress = 0;
	double PopulationProgressIncome = 0;
	int	  House = 0;
	int	  WorkerPlaces = 0;
	double BuildToStation = 0;

	TMap<FName, double> ResourcesProduce;
	TMap<FName, double> ResourcesConsume;
	TMap<FName, double> GetResourceDiff()
	{
		TMap<FName, double> Res;

		for (auto& Pair : ResourcesProduce)
			Res.FindOrAdd(Pair.Key) += Pair.Value;
		for (auto& Pair : ResourcesConsume)
			Res.FindOrAdd(Pair.Key) -= Pair.Value;

		return Res;
	}
};