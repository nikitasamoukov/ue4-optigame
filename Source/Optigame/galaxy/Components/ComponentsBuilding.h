#pragma once

#include "Optigame/OptigamePch.h"
#include "ComponentsBasic.h"
#include "ComponentsBuilding.generated.h"

USTRUCT(BlueprintType)
struct FCompBuilding
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool IsDisabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool IsActive = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEnttHandle TypeBreed;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEntity Planet = entt::null;
};

USTRUCT(BlueprintType)
struct FCompBuildingCost
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FResPile Cost;
};

USTRUCT(BlueprintType)
struct FCompBuildingType
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName NameId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName ImageId;
};

USTRUCT(BlueprintType)
struct FCompBuildingHouse
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Size = 0;
};

USTRUCT(BlueprintType)
struct FCompBuildingWorkers
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Workers = 0;
};

USTRUCT(BlueprintType)
struct FBuildingResProd
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName TypeId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Count = 0;
};

USTRUCT(BlueprintType)
struct FCompBuildingProduct
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FResPile Out;
};

USTRUCT(BlueprintType)
struct FCompBuildingConsume
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FResPile In;
};