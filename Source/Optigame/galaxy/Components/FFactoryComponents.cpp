#include "FFactoryComponents.h"

#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsBuilding.h"
#include "Optigame/Galaxy/Components/ComponentsBuildQueue.h"
#include "Optigame/Galaxy/Components/ComponentsEmpire.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"
#include "Optigame/Galaxy/Components/ComponentsPr.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "Optigame/ImGuiWindows/FRegistryImGuiWindow.h"
#include "Optigame/ImGuiWindows/FComponentsDrawFunctions.h"

struct FImGuiComponentDrawContext;
class LRegistryImGuiWindowImpl;

template <typename T>
concept CComponentSpecialImGuiDraw = requires(FImGuiComponentDrawContext const& Ctx, T const& C) {
	{
		ImGuiCDraw(Ctx, C)
	} -> std::same_as<void>;
};

void FFactoryComponents::AddType(entt::type_info Ti, FFactoryComponentsCCell&& CInfo)
{

	TypesMap.Add(Ti.hash(), CInfo);
	NameToTypeInfo.Add(*CInfo.Name, Ti);
}

FFactoryComponentsCCell const* FFactoryComponents::GetTypeCell(entt::id_type TiSeq)
{
	if (!TypesMap.Contains(TiSeq))
		return nullptr;
	return &TypesMap[TiSeq];
}

FFactoryComponentsCCell const* FFactoryComponents::GetTypeCell(entt::type_info Ti)
{
	return GetTypeCell(Ti.hash());
}

FFactoryComponentsCCell const* FFactoryComponents::GetTypeCell(FName CompName)
{
	if (!NameToTypeInfo.Contains(CompName))
		return nullptr;
	return GetTypeCell(NameToTypeInfo[CompName]);
}

template <typename C>
concept CHasXmlLoad = requires(C& Comp, FXmlNode* Node) {
	LoadComponentData(Comp, Node);
};

template <typename C>
void RegComp(FFactoryComponents& Factory, FString const& Name)
{
	FFactoryComponentsCCell Cell;

	Cell.Name = Name;

	{ // CompIsRef
		Cell.CompIsRef = CCompIsRef<C>;
	}
	{ // FuncImguiDraw
		if constexpr (CComponentSpecialImGuiDraw<C>)
		{
			// Cell.FuncImguiDraw = [](FImGuiComponentDrawContext const& Ctx) {
			//	if constexpr (!std::is_empty_v<C>) {
			//		auto Comp = Ctx.Reg->try_get<C>(Ctx.Entity);
			//		if (Comp)
			//			ImGuiCDraw(Ctx, *Comp);
			//		else
			//			LOG();
			//	}
			//	else
			//	{
			//		C Comp;
			//		ImGuiCDraw(Ctx, Comp);
			//	}
			// };
			if constexpr (!std::is_empty_v<C>)
			{
				Cell.FuncImguiDraw = [](FImGuiComponentDrawContext const& Ctx) {
					auto Comp = Ctx.Reg->try_get<C>(Ctx.Entity);
					if (Comp)
						ImGuiCDraw(Ctx, *Comp);
					else
						LOG();
				};
			}
			else
			{
				Cell.FuncImguiDraw = [](FImGuiComponentDrawContext const& Ctx) {
					C Comp;
					ImGuiCDraw(Ctx, Comp);
				};
			}
		}
		else
		{
			if constexpr (CCompIsRef<C> && !std::is_empty_v<C>)
			{
				Cell.FuncImguiDraw = [](FImGuiComponentDrawContext const& Ctx) {
					auto Comp = Ctx.Reg->try_get<C>(Ctx.Entity);
					if (Comp)
						ImGuiCDrawRef(Ctx, *Comp);
					else
						LOG();
				};
			}
			else
			{
				Cell.FuncImguiDraw = [](FImGuiComponentDrawContext const& Ctx) {
					ImGuiCDrawNoReflectionAndDraw(Ctx, entt::type_id<C>());
				};
			}
		}
	}
	{ // ImguiDrawPriority
		Cell.ImguiDrawPriority = ImGuiCDrawGetPri<C>();
	}

	{ // FuncUnpack
		Cell.FuncUnpack = [](FEnttHandle TemplateHandle, FInstancedStruct const& Data) {
			auto Typed = Data.GetPtr<C>();
			if (!Typed)
			{
				LOG();
				return;
			}
			TemplateHandle.emplace<C>(*Typed);
		};
	}

	{ // FuncHaveComp
		Cell.FuncHaveComp = [](FRegistry const& Reg, FEntity Entity) {
			return Reg.has<C>(Entity);
		};
	}

	{ // FuncCopy
		if constexpr (!std::is_empty_v<C>)
		{
			Cell.FuncCopy = [](FEnttHandle BreedHandle, FEnttHandle EntityHandle) {
				EntityHandle.emplace_or_replace<C>(BreedHandle.get<C>());
			};
		}
		else
		{
			Cell.FuncCopy = [](FEnttHandle BreedHandle, FEnttHandle EntityHandle) {
				EntityHandle.emplace<C>();
			};
		}
	}

	Factory.AddType(entt::type_id<C>(), MoveTemp(Cell));
}

#define REG_COMPONENT(C) RegComp<C>(*this, #C)

FFactoryComponents::FFactoryComponents()
{
	REG_COMPONENT(FCompBreedId);
	REG_COMPONENT(FCompBuildQueue);
	REG_COMPONENT(FCompBuilding);
	REG_COMPONENT(FCompBuildingConsume);
	REG_COMPONENT(FCompBuildingCost);
	REG_COMPONENT(FCompBuildingHouse);
	REG_COMPONENT(FCompBuildingProduct);
	REG_COMPONENT(FCompBuildingType);
	REG_COMPONENT(FCompBuildingWorkers);
	REG_COMPONENT(FCompColony);
	REG_COMPONENT(FCompCurrentTimePos);
	REG_COMPONENT(FCompEmpire);
	REG_COMPONENT(FCompEmpireSpawnInfo);
	REG_COMPONENT(FCompMesh);
	REG_COMPONENT(FCompOrbit);
	REG_COMPONENT(FCompOwner);
	REG_COMPONENT(FCompPlanet);
	REG_COMPONENT(FCompPrPlanet);
	REG_COMPONENT(FCompPrStar);
	REG_COMPONENT(FCompPrSun);
	REG_COMPONENT(FCompResourceType);
	REG_COMPONENT(FCompSelectTag);
	REG_COMPONENT(FCompShip);
	REG_COMPONENT(FCompShipModule);
	REG_COMPONENT(FCompShipModuleArmor);
	REG_COMPONENT(FCompShipModuleEngine);
	REG_COMPONENT(FCompShipModuleHull);
	REG_COMPONENT(FCompShipModuleHullStation);
	REG_COMPONENT(FCompShipModuleShield);
	REG_COMPONENT(FCompShipModuleShipyard);
	REG_COMPONENT(FCompShipModuleWeapon);
	REG_COMPONENT(FCompShipTemplate);
	REG_COMPONENT(FCompSpaceObject);
	REG_COMPONENT(FCompStar);
	REG_COMPONENT(FCompTransform);
	REG_COMPONENT(FCompMeshWeapon);
}

#undef REG_COMPONENT