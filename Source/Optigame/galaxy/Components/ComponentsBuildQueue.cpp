#include "ComponentsBuildQueue.h"
#include "Optigame/Galaxy/Helpers/HelperBuildingQueue.h"

double FCompBuildQueue::AddBuild(FEnttHandle Object, double BuildToApply)
{
	if (BuildToApply < 0)
	{
		LOG();
		return 0;
	}

	bool IsSomethingEnd = false;

	for (auto& BuildOp : Queue)
	{
		if (BuildOp.BuildReady + BuildToApply < BuildOp.BuildFull)
		{
			BuildOp.BuildReady += BuildToApply;
			BuildToApply = 0;
			break;
		}
		BuildToApply = FMath::Max(0.0f, BuildToApply - (BuildOp.BuildFull - BuildOp.BuildReady));
		BuildOp.BuildReady = BuildOp.BuildFull;
		IsSomethingEnd = true;
	}

	if (!IsSomethingEnd)
		return FMath::Max(0.0f, BuildToApply);

	for (auto It = Queue.CreateIterator(); It; ++It)
	{
		if (It->BuildReady != It->BuildFull)
			break;

		UHelperBuildingQueue::ExecBuildOperation(Object, *It);

		It.RemoveCurrent();
	}

	return FMath::Max(0.0f, BuildToApply); // Fpoint math is hard
}