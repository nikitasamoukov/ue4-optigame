#pragma once

#include "Optigame/OptigamePch.h"
#include "ComponentsBasic.h"

#include "ComponentsShip.generated.h"

USTRUCT(BlueprintType)
struct FHitStat
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Regen = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Val = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Max = 0;

	void Update();
	void Refresh(FHitStat const& NewStat);
	void SetFull();
};

UENUM(BlueprintType)
enum class EWeaponSize : uint8
{
	Small,
	Medium,
	Large,
};

extern TMap<EWeaponSize, double> GMapWeaponSize;

inline int WeaponSizeToInt(EWeaponSize WeaponSize)
{
	return GMapWeaponSize[WeaponSize];
}

inline auto CalcGMapWeaponSize()
{
	return TMap<EWeaponSize, double>{
		{ EWeaponSize::Small, GOptigameVars.Ships.WeaponScaleSmall },
		{ EWeaponSize::Medium, GOptigameVars.Ships.WeaponScaleMedium },
		{ EWeaponSize::Large, GOptigameVars.Ships.WeaponScaleLarge }
	};
}

USTRUCT(BlueprintType)
struct FShipWeapon
{
	GENERATED_BODY()
public:
	// Type info

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName BreedId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int BreedTier = 1;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EWeaponSize Size = EWeaponSize::Small;

	// Instance info

	// Ship local
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FQuat Rotate = {};

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double ReloadTime = 1;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Range = 0;
};

USTRUCT(BlueprintType)
struct FCompSpaceObject
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FHitStat Hp;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FHitStat Ap;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FHitStat Sp;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FShipWeapon> Weapons;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName HullBreedId;

	void Refresh(FCompSpaceObject const& CSpaceObject);
	// Used to make full Hp/Reload setup on new ship create.
	void SetFull();
};

USTRUCT(BlueprintType)
struct FCompPhys
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector Vel = FVector::ZeroVector;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Size = 0;

	void Update(FCompTransform& CTransform);
};

USTRUCT(BlueprintType)
struct FCompShip
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double SpeedForwardMax = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double SpeedSideMax = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double SpeedRotateMax = 0;
};

USTRUCT(BlueprintType)
struct FCompStation
{
	GENERATED_BODY()

	FEntity Planet = entt::null;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Build = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double BuildPlanet = 0;
};

USTRUCT(BlueprintType)
struct FShipTemplateModule
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName BreedId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Size = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Tier = 1;
};

USTRUCT(BlueprintType)
struct FCompShipTemplate
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Name;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FShipTemplateModule> Modules;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FShipTemplateModule> Weapons;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FShipTemplateModule Hull;
};

UENUM(BlueprintType)
enum class EShipModuleType : uint8
{
	Hull,
	Weapon,
	Shield,
	Armor,
	Reactor,
	Engine,
	HullStation,
	Shipyard,
};

USTRUCT(BlueprintType)
struct FCompShipModule
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName NameId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName ImageId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EShipModuleType Type;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FResPile Cost;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Mass = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Energy = 0;

	FCompShipModule GetScaled(double Scale, int Tier);
};

USTRUCT(BlueprintType)
struct FShipWeaponSlot
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EWeaponSize Size = EWeaponSize::Small;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bFixed = true;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName SocketName;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FTransform SocketTransform = FTransform::Identity;
};

USTRUCT(BlueprintType)
struct FCompShipModuleHull
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Hp = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Volume = 100;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FShipWeaponSlot> Weapons;
};

USTRUCT(BlueprintType)
struct FCompShipModuleShield
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Sp = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Regen = 0;

	FCompShipModuleShield GetScaled(double Scale, int Tier);
};

USTRUCT(BlueprintType)
struct FCompShipModuleArmor
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Ap = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Regen = 0;

	FCompShipModuleArmor GetScaled(double Scale, int Tier);
};

USTRUCT(BlueprintType)
struct FCompShipModuleWeapon
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Cooldown = 1;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Range = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Dps = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName ShotId;

	FCompShipModuleWeapon GetScaled(double Scale, int Tier);
};

USTRUCT(BlueprintType)
struct FCompMeshWeapon
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Small;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Medium;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Large;
};

USTRUCT(BlueprintType)
struct FCompShipModuleEngine
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Thrust = 0;

	FCompShipModuleEngine GetScaled(double Scale, int Tier);
};

USTRUCT(BlueprintType)
struct FCompShipModuleHullStation
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Hp = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Volume = 100;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Modules = 1;
};

USTRUCT(BlueprintType)
struct FCompShipModuleShipyard
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Build = 0;
};

USTRUCT(BlueprintType)
struct FCompSelectTag
{
	GENERATED_BODY()
public:
};

USTRUCT(BlueprintType)
struct FCompShipWarpOffset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector Offset{ 0 }; // Sphere with size 1
};

struct FGamePos
{
	FEntity Star;
	FVector Pos{ 0 };
};

struct FActionBase
{
	FGamePos		GamePos;
	TArray<FEntity> Path; // CurrentStar-Waypoint-Waypoint-Waypoint-Goal
	FEntity			WarpZone = entt::null;
};

struct FActionMove : FActionBase
{
	void UpdateBase(FRegistry& Reg, FGamePos const& PrevGamePos);
};

struct FActionAttack : FActionBase
{
	FEntity Target;

	void UpdateBase(FRegistry& Reg, FGamePos const& PrevGamePos);
};

using FShipAction = TVariant<FActionMove, FActionAttack>;

USTRUCT(BlueprintType)
struct FCompShipController
{
	GENERATED_BODY()
public:
	TArray<FShipAction> Actions;
};

USTRUCT(BlueprintType)
struct FCompShipWarpExec
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEntity Star = entt::null;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEntity WarpZone = entt::null;
};