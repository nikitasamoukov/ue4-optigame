#pragma once

#include "Optigame/OptigamePch.h"
#include "Optigame/Misc/FRegistry.h"
#include "ComponentsBasic.generated.h"

USTRUCT(BlueprintType)
struct FCompTransform
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector Pos{ 0 };
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FQuat Quat{ FQuat::Identity };
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector PrevPos{ 0 };
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FQuat PrevQuat{ FQuat::Identity };

	FVector CalcMixPos(double ProgressToCurrentFrame) const;
	FQuat CalcMixQuat(double ProgressToCurrentFrame) const;
	void CopyToPrev();
};

USTRUCT(BlueprintType)
struct FCompCurrentTimePos
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector Pos{ 0 };
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FQuat Quat{ FQuat::Identity };

	void FromTrs(FCompTransform const& CTransform, double ProgressToCurrentFrame);
};

USTRUCT(BlueprintType)
struct FCompVelocity
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector Vel;
};

USTRUCT(BlueprintType)
struct FCompOwner
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEntity Empire = entt::null;
};

USTRUCT(BlueprintType)
struct FCompPosStar
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEntity Star = entt::null;
};

USTRUCT(BlueprintType)
struct FStarConnect
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEntity Star = entt::null;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEntity WarpZone = entt::null;
};

UENUM(BlueprintType)
enum class EWarpZoneType : uint8
{
	Star,
	Gate,
};

USTRUCT(BlueprintType)
struct FCompWarpZone
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEntity Star = entt::null;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EWarpZoneType Type = EWarpZoneType::Star;
};

UENUM(BlueprintType)
enum class EStarType : uint8
{
	Sun,
	Blackhole,
	Pulsar,
};

USTRUCT(BlueprintType)
struct FCompStar
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Size = 70;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EStarType Type = EStarType::Sun;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FStarConnect> Connects;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FEntity> Objects;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FEntity> Ships;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Name = "<Unnamed>";
};

USTRUCT(BlueprintType)
struct FCompOrbit
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEntity Body = entt::null; // For moons

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double Radius = 0;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double OrbitalAngularSpeed = 1;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	double CurrentAngle = 0; // Radians

	void CalcAngularSpeed();

	void Update();
	FVector CalculatePosition() const;
};

USTRUCT(BlueprintType)
struct FCompResourceType
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName NameId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName ImageId;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool Local = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool Additive = false;
};

USTRUCT(BlueprintType)
struct FCompBreedId
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Id;
};

struct FCtxPlayerInfo
{
	FEntity Empire = entt::null;
};

USTRUCT(BlueprintType)
struct FResPile
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FName, double> Resources;
	void Add(FResPile const& Cost);
	void Add(FName Name, double Count);
};

USTRUCT(BlueprintType)
struct FCompMesh
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Name;
};