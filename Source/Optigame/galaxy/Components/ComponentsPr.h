#pragma once
#include "ComponentsPr.generated.h"

class APrWarpZone;
class APrSun;
class APrStar;
class APrPlanet;
class APrStation;

USTRUCT(BlueprintType)
struct FCompPrStar
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	APrStar* Actor = nullptr;
};

USTRUCT(BlueprintType)
struct FCompPrSun
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	APrSun* Actor = nullptr;
};

USTRUCT(BlueprintType)
struct FCompPrPlanet
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	APrPlanet* Actor = nullptr;
};

USTRUCT(BlueprintType)
struct FCompPrStation
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	APrStation* Actor = nullptr;
};

USTRUCT(BlueprintType)
struct FCompPrWarpZone
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	APrWarpZone* Actor = nullptr;
};