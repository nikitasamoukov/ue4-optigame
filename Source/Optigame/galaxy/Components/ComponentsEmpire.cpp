#include "ComponentsEmpire.h"
#include "ComponentsBasic.h"

bool FCompEmpire::IsHaveResources(FResPile const& ResCost)
{
	for (auto& Pair : ResCost.Resources)
	{
		if (Pair.Key == "res_build")
		{
			continue;
		}
		if (!Resources.Contains(Pair.Key))
		{
			LOG("No res in empire:" + Pair.Key.ToString());
			continue;
		}
		if (*Resources.Find(Pair.Key) < Pair.Value)
			return false;
	}
	return true;
}

void FCompEmpire::ResourcesCostPay(FResPile const& ResCost)
{
	for (auto& Pair : ResCost.Resources)
	{
		if (Pair.Key == "res_build")
		{
			continue;
		}
		if (!Resources.Contains(Pair.Key))
		{
			LOG("No res in empire:" + Pair.Key.ToString());
			continue;
		}
		*Resources.Find(Pair.Key) -= Pair.Value;
	}
}