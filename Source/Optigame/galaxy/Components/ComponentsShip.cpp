#include "ComponentsShip.h"

#include "Optigame/Galaxy/FGalaxy.h"

TMap<EWeaponSize, double> GMapWeaponSize;

void FHitStat::Update()
{
	Val = FMath::Clamp(Val + Regen * GOptigameVars.FrameTime, 0.0f, Max);
}

void FHitStat::Refresh(FHitStat const& NewStat)
{
	double Percent = Val / FMath::Max(0.001f, Max);
	if (Max == 0)
		Percent = 1;

	Regen = NewStat.Regen;
	Max = NewStat.Max;
	Val = Max * Percent;
	Val = FMath::Clamp(Val, 0.0f, Max);
}

void FHitStat::SetFull()
{
	Val = Max;
}

void FCompSpaceObject::Refresh(FCompSpaceObject const& CSpaceObject)
{
	Hp.Refresh(CSpaceObject.Hp);
	Ap.Refresh(CSpaceObject.Ap);
	Sp.Refresh(CSpaceObject.Sp);

	Weapons = CSpaceObject.Weapons;
}

void FCompSpaceObject::SetFull()
{
	Hp.SetFull();
	Ap.SetFull();
	Sp.SetFull();
}

void FCompPhys::Update(FCompTransform& CTransform)
{
	CTransform.Pos += Vel;
}

FCompShipModule FCompShipModule::GetScaled(double Scale, int Tier)
{
	auto Res = *this;

	Tier = FMath::Clamp(Tier, 1, 5);

	// Tier

	for (auto& Pair : Res.Cost.Resources)
	{
		Pair.Value *= FMath::Pow(1.2, Tier - 1);
	}

	switch (Type)
	{
		case EShipModuleType::Hull:
		case EShipModuleType::HullStation:
		{
		}
		break;
		case EShipModuleType::Armor:
		{
			Res.Energy *= FMath::Pow(1.2, Tier - 1);
		}
		break;
		case EShipModuleType::Engine:
		{
			Res.Energy *= FMath::Pow(1.2, Tier - 1);
			Res.Mass *= FMath::Pow(0.9, Tier - 1);
		}
		break;
		case EShipModuleType::Reactor:
		{
			Res.Energy *= FMath::Pow(1.6, Tier - 1);
			Res.Mass *= FMath::Pow(1.1, Tier - 1);
		}
		break;
		case EShipModuleType::Shield:
		{
			Res.Energy *= FMath::Pow(1.2, Tier - 1);
			Res.Mass *= FMath::Pow(0.9, Tier - 1);
		}
		break;
		case EShipModuleType::Weapon:
		{
			Res.Energy *= FMath::Pow(1.2, Tier - 1);
			Res.Mass *= FMath::Pow(0.9, Tier - 1);
		}
		break;
		case EShipModuleType::Shipyard:
		{
			Res.Energy *= FMath::Pow(1.2, Tier - 1);
			Res.Mass *= FMath::Pow(0.9, Tier - 1);
		}
		break;
		default:
			LOG();
	}

	// Scale

	switch (Type)
	{
		case EShipModuleType::Hull:
		case EShipModuleType::HullStation:
		{
		}
		break;
		case EShipModuleType::Reactor:
			for (auto& Pair : Res.Cost.Resources)
			{
				Pair.Value *= Scale;
			}

			Res.Energy *= FMath::Pow(2.4, FMath::Log2(Scale));
			Res.Mass *= Scale;
			break;
		case EShipModuleType::Armor:
		case EShipModuleType::Engine:
		case EShipModuleType::Shield:
		case EShipModuleType::Weapon:
		case EShipModuleType::Shipyard:
		{
			for (auto& Pair : Res.Cost.Resources)
			{
				Pair.Value *= Scale;
			}
			Res.Energy *= Scale;
			Res.Mass *= Scale;
		}
		break;
		default:
			LOG();
	}

	return Res;
}

FCompShipModuleShield FCompShipModuleShield::GetScaled(double Scale, int Tier)
{
	auto Res = *this;

	// Tier

	Res.Sp *= FMath::Pow(1.3, Tier - 1);
	Res.Regen *= FMath::Pow(1.3, Tier - 1);

	// Scale

	Res.Sp *= FMath::Pow(2.1, FMath::Log2(Scale));
	Res.Regen *= FMath::Pow(2.1, FMath::Log2(Scale));

	return Res;
}

FCompShipModuleArmor FCompShipModuleArmor::GetScaled(double Scale, int Tier)
{
	auto Res = *this;

	// Tier

	Res.Ap *= FMath::Pow(1.3, Tier - 1);
	Res.Regen *= FMath::Pow(1.3, Tier - 1);

	// Scale

	Res.Ap *= FMath::Pow(2.1, FMath::Log2(Scale));
	Res.Regen *= FMath::Pow(2.1, FMath::Log2(Scale));

	return Res;
}

FCompShipModuleWeapon FCompShipModuleWeapon::GetScaled(double Scale, int Tier)
{
	auto Res = *this;

	// Tier

	Res.Range *= FMath::Pow(1.1, Tier - 1);
	Res.Cooldown *= FMath::Pow(1.2, Tier - 1);
	Res.Dps *= FMath::Pow(1.3, Tier - 1);

	// Scale

	Res.Range *= FMath::Pow(1.3, FMath::Log2(Scale));
	Res.Cooldown *= FMath::Pow(1.3, FMath::Log2(Scale));
	Res.Dps *= FMath::Pow(2.1, FMath::Log2(Scale));

	return Res;
}

FCompShipModuleEngine FCompShipModuleEngine::GetScaled(double Scale, int Tier)
{
	auto Res = *this;

	// Tier

	Res.Thrust *= FMath::Pow(1.2, Tier - 1);

	// Scale

	Res.Thrust *= FMath::Pow(2.1, FMath::Log2(Scale));

	return Res;
}

void FActionMove::UpdateBase(FRegistry& Reg, FGamePos const& PrevGamePos)
{
	bool bCorrectPath =
		Path.Num() != 0 && Path[Path.Num() - 1] == GamePos.Star && Path[0] == PrevGamePos.Star;

	if (!bCorrectPath)
	{
		auto NewPath = Reg.ctx().get<FGalaxy*>()->Pathfinder.Find(Reg, PrevGamePos.Star, GamePos.Star);
		Path = NewPath;
	}

	if (Path.Num() < 2)
	{
		WarpZone = entt::null;
		return;
	}

	// if some target exist
	if (WarpZone != entt::null)
	{
		auto CWarpZone = Reg.try_get<FCompWarpZone>(WarpZone);
		if (CWarpZone->Star == Path[1])
			return; // all ok
		WarpZone = entt::null;
	}

	// search new zone
	auto& CStar = Reg.get<FCompStar>(PrevGamePos.Star);
	auto  StarConnect = CStar.Connects.FindByPredicate([&](FStarConnect& E) { return E.Star == Path[1]; });
	if (!StarConnect)
	{
		LOG();
		return;
	}
	WarpZone = StarConnect->WarpZone;
}

void FActionAttack::UpdateBase(FRegistry& Reg, FGamePos const& PrevGamePos)
{
	// TODO
}