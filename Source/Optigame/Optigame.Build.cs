// Copyright Epic Games, Inc. All Rights Reserved.

using System.IO;
using UnrealBuildTool;

public class Optigame : ModuleRules
{
    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private string ProjPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "../")); }
    }

    public Optigame(ReadOnlyTargetRules Target) : base(Target)
    {
        //bUseUnity = false;
        //PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        PCHUsage = PCHUsageMode.NoSharedPCHs;
        PrivatePCHHeaderFile = "OptigamePch.h";
        CppStandard = CppStandardVersion.Cpp20;

        PublicDependencyModuleNames.AddRange(new string[] { "RenderCore", "Core", "CoreUObject", "Engine", "InputCore" });
        PublicDependencyModuleNames.AddRange(new string[] { "RHI", "RenderCore" });


        PublicDependencyModuleNames.Add("ImGui");

        PublicDependencyModuleNames.AddRange(new string[] { "UMG", "Slate", "SlateCore" });

        if (Target.Type == TargetType.Editor)
            PublicDependencyModuleNames.AddRange(new string[] { "UnrealEd", "EditorSubsystem" });

        PublicDependencyModuleNames.AddRange(new string[] { "XmlParser" });

        PublicDependencyModuleNames.AddRange(new string[] { "StructUtils" });

        string includePath = Path.Combine(ProjPath, "include");
        PublicIncludePaths.Add(includePath);
        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
