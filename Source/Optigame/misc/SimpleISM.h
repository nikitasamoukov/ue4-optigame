#pragma once
#include "StaticMeshVertexData.h"

#include "Rendering/StaticMeshVertexDataInterface.h"

// #include "SimpleISM.generated.h"

/*-----------------------------------------------------------------------------
	FPerInstanceRenderData
	Holds render data that can persist between scene proxy reconstruction
-----------------------------------------------------------------------------*/

class FSimpleISMInstancesData // FStaticMeshInstanceData
{
public:
	FSimpleISMInstancesData()
	{
		InstanceOriginData = MakeUnique<TStaticMeshVertexData<FVector4>>();
		InstanceTransformData = MakeUnique<TStaticMeshVertexData<FMatrix>>();
		InstanceCustomData = MakeUnique<TStaticMeshVertexData<double>>();

		InstanceOriginDataPtr = InstanceOriginData->GetDataPointer();
		InstanceTransformDataPtr = InstanceTransformData->GetDataPointer();
		InstanceCustomDataPtr = InstanceCustomData->GetDataPointer();
	}

	TUniquePtr<FStaticMeshVertexDataInterface> InstanceOriginData;
	uint8*									   InstanceOriginDataPtr = nullptr;

	TUniquePtr<FStaticMeshVertexDataInterface> InstanceTransformData;
	uint8*									   InstanceTransformDataPtr = nullptr;

	TUniquePtr<FStaticMeshVertexDataInterface> InstanceCustomData;
	uint8*									   InstanceCustomDataPtr = nullptr;

	int32 NumInstances = 0;
	int32 NumCustomDatadoubles = 0;
};

//// A vertex buffer of positions.
// class FSimpleISMInstanceBuffer : public FRenderResource // FStaticMeshInstanceBuffer
//{
// public:
//
//	/** Default constructor. */
//	FSimpleISMInstanceBuffer(ERHIFeatureLevel::Type InFeatureLevel, bool InRequireCPUAccess);
//
//	/** Destructor. */
//	~FSimpleISMInstanceBuffer();
//
//	/**
//	 * Initializes the buffer with the component's data.
//	 * @param Other - instance data, this call assumes the memory, so this will be empty after the call
//	 */
//	ENGINE_API void InitFromPreallocatedData(FSimpleISMInstancesData& Other);
//
//	/**
//	 * Specialized assignment operator, only used when importing LOD's.
//	 */
//	void operator=(const FSimpleISMInstanceBuffer& Other);
//
//	// Other accessors.
//	FORCEINLINE uint32 GetNumInstances() const
//	{
//		return InstanceData->GetNumInstances();
//	}
//
//	FORCEINLINE  void GetInstanceTransform(int32 InstanceIndex, FMatrix& Transform) const
//	{
//		InstanceData->GetInstanceTransform(InstanceIndex, Transform);
//	}
//
//	FORCEINLINE  void GetInstanceShaderValues(int32 InstanceIndex, FVector4(&InstanceTransform)[3], FVector4& InstanceLightmapAndShadowMapUVBias, FVector4& InstanceOrigin) const
//	{
//		InstanceData->GetInstanceShaderValues(InstanceIndex, InstanceTransform, InstanceLightmapAndShadowMapUVBias, InstanceOrigin);
//	}
//
//	FORCEINLINE  void GetInstanceCustomDataValues(int32 InstanceIndex, TArray<double>& InstanceCustomData) const
//	{
//		InstanceData->GetInstanceShaderCustomDataValues(InstanceIndex, InstanceCustomData);
//	}
//
//	FORCEINLINE FSimpleISMInstancesData* GetInstanceData() const
//	{
//		return InstanceData.Get();
//	}
//
//	// FRenderResource interface.
//	virtual void InitRHI() override;
//	virtual void ReleaseRHI() override;
//	virtual void InitResource() override;
//	virtual void ReleaseResource() override;
//	virtual FString GetFriendlyName() const override { return TEXT("Static-mesh instances"); }
//	SIZE_T GetResourceSize() const;
//
//	void BindInstanceVertexBuffer(const class FVertexFactory* VertexFactory, struct FInstancedStaticMeshDataType& InstancedStaticMeshData) const;
//
// public:
//	/** The vertex data storage type */
//	TSharedPtr<FSimpleISMInstancesData, ESPMode::ThreadSafe> InstanceData;
//
//	/** Keep CPU copy of instance data*/
//	bool RequireCPUAccess;
//
//	FVertexBufferRHIRef GetInstanceOriginBuffer()
//	{
//		return InstanceOriginBuffer.VertexBufferRHI;
//	}
//
//	FVertexBufferRHIRef GetInstanceTransformBuffer()
//	{
//		return InstanceTransformBuffer.VertexBufferRHI;
//	}
//
// private:
//	class FInstanceOriginBuffer : public FVertexBuffer
//	{
//		virtual FString GetFriendlyName() const override { return TEXT("FInstanceOriginBuffer"); }
//	} InstanceOriginBuffer;
//	FShaderResourceViewRHIRef InstanceOriginSRV;
//
//	class FInstanceTransformBuffer : public FVertexBuffer
//	{
//		virtual FString GetFriendlyName() const override { return TEXT("FInstanceTransformBuffer"); }
//	} InstanceTransformBuffer;
//	FShaderResourceViewRHIRef InstanceTransformSRV;
//
//	class FInstanceCustomDataBuffer : public FVertexBuffer
//	{
//		virtual FString GetFriendlyName() const override { return TEXT("FInstanceCustomDataBuffer"); }
//	} InstanceCustomDataBuffer;
//	FShaderResourceViewRHIRef InstanceCustomDataSRV;
//
//	/** Delete existing resources */
//	void CleanUp();
//
//	void CreateVertexBuffer(FResourceArrayInterface* InResourceArray, uint32 InUsage, uint32 InStride, uint8 InFormat, FVertexBufferRHIRef& OutVertexBufferRHI, FShaderResourceViewRHIRef& OutInstanceSRV);
//
// };
//
//
// struct FSimpleISMPerInstanceRenderData
//{
//	// Should be always constructed on main thread
//	FSimpleISMPerInstanceRenderData(FSimpleISMInstancesData& Other, ERHIFeatureLevel::Type InFeaureLevel);
//	~FSimpleISMPerInstanceRenderData();
//
//	ENGINE_API void Update(FSimpleISMInstancesData& InOther);
//
//	/** cached per-instance resource size*/
//	SIZE_T								ResourceSize;
//
//	/** Instance buffer */
//	FSimpleISMInstanceBuffer			InstanceBuffer;
//	TSharedPtr<FSimpleISMInstancesData, ESPMode::ThreadSafe> InstanceBuffer_GameThread;
// };