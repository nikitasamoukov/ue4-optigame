// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Optigame/OptigamePch.h"

class UTexture2D;

UStaticMesh*			   TryGetMesh(FString const& Name, FString const& AssetFolder = "/Game");
TMap<FName, UTexture2D*>   GetAllTextures(FString const& AssetFolder = "/Game");
TMap<FName, UStringTable*> GetAllStringTables(FString const& AssetFolder = "/Game");