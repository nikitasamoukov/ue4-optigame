// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "NumFmt.generated.h"

/**
 *
 */
UCLASS()
class OPTIGAME_API UNumFmt : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	static FString NumFmt(double Num, int Digits, ERoundingMode RoundingMode);
	UFUNCTION(BlueprintCallable)
	static FText NumFmtStats(double Num);
	UFUNCTION(BlueprintCallable)
	static FText NumFmtCost(double Num);
	UFUNCTION(BlueprintCallable)
	static FText NumFmtRes(double Num);
};