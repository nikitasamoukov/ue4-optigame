// Fill out your copyright notice in the Description page of Project Settings.

#include "DPIScalingRule.h"

float UFullHdDPIScalingRule::GetDPIScaleBasedOnSize(FIntPoint Size) const
{
	auto SidesDpi = FVector2D(Size) / FVector2D(1920, 1080);
	auto Dpi = FMath::Max(FMath::Min(SidesDpi.X, SidesDpi.Y), 0.1f);
	if (Dpi > 0.9 && Dpi < 1.1)
		Dpi = 1;
	return Dpi;
}

float UUhdDPIScalingRule::GetDPIScaleBasedOnSize(FIntPoint Size) const
{
	auto SidesDpi = FVector2D(Size) / FVector2D(1920 * 2, 1080 * 2);
	auto Dpi = FMath::Max(FMath::Min(SidesDpi.X, SidesDpi.Y), 0.1f);
	if (Dpi > 0.9 && Dpi < 1.1)
		Dpi = 1;
	return Dpi;
}
