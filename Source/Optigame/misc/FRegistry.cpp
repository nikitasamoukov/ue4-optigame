// Fill out your copyright notice in the Description page of Project Settings.

#include "FRegistry.h"

bool FEnttHandle::IsValid() const
{
	return !!(*this);
}

bool operator==(FEnttHandle const& A, FEnttHandle const& B)
{
	// LOG("E1: " + FString::FromInt((int)A.Entity) + ",E2: " + FString::FromInt((int)B.Entity) + " Compare res:" + FString::FromInt(A.Entity == B.Entity));
	// LOG("R1: " + FString::FromInt(*(int*)&A.Reg) + ",R2: " + FString::FromInt(*(int*)&B.Reg) + " Compare res:" + FString::FromInt(A.Reg == B.Reg));
	return A.entity() == B.entity() && A.registry() == B.registry();
}