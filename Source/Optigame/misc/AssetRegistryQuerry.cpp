// Fill out your copyright notice in the Description page of Project Settings.

#include "AssetRegistryQuerry.h"

#include "AssetRegistry/AssetData.h"
#include "AssetRegistry/AssetRegistryModule.h"
#include "Engine/StaticMesh.h"
#include "Engine/Texture2D.h"
#include "Internationalization/StringTable.h"

template <typename T>
TMap<FName, T*> GetAssetsFromFolder(FString const& Name = "", FString const& AssetFolder = "/Game")
{
	FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");

	TArray<FAssetData> AssetsData;
	FARFilter		   Filter;
	Filter.ClassPaths.Add(FTopLevelAssetPath(T::StaticClass()->GetPathName()));
	Filter.PackagePaths.Add(*AssetFolder);
	Filter.bRecursivePaths = true;
	bool GetRes = AssetRegistryModule.Get().GetAssets(Filter, AssetsData);

	if (Name != "")
	{
		FName NameId(*Name, FNAME_Find);
		if (NameId == NAME_None)
			return {};
		AssetsData.RemoveAll([NameId](auto& e) {
			return e.AssetName != NameId;
		});
	}

	TMap<FName, T*> Res;

	if (AssetsData.Num() == 0)
	{
		LOG("AssetsData.Num()==0");
	}

	for (auto& AssetData : AssetsData)
	{
		UObject* Object = AssetData.GetAsset();

		if (Object)
		{
			T* MeshPtr = Cast<T>(Object);
			if (MeshPtr)
			{
				Res.Add(AssetData.AssetName, MeshPtr);
			}
			else
				LOG("GetAssetsFromFolder Bad ptr get");
		}
		else
			LOG("GetAssetsFromFolder Bad object get");
	}

	return Res;
}

UStaticMesh* TryGetMesh(FString const& Name, FString const& AssetFolder)
{
	auto Assets = GetAssetsFromFolder<UStaticMesh>(Name, AssetFolder);
	if (Assets.Num() == 0)
	{
		LOG("No mesh");
		return nullptr;
	}
	return (*Assets.begin()).Value;
}

TMap<FName, UTexture2D*> GetAllTextures(FString const& AssetFolder)
{
	auto AssetsMap = GetAssetsFromFolder<UTexture2D>("", AssetFolder);

	return AssetsMap;
}

TMap<FName, UStringTable*> GetAllStringTables(FString const& AssetFolder)
{
	auto AssetsMap = GetAssetsFromFolder<UStringTable>("", AssetFolder);

	return AssetsMap;
}