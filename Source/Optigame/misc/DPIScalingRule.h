// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DPICustomScalingRule.h"
#include "DPIScalingRule.generated.h"

UCLASS()
class OPTIGAME_API UFullHdDPIScalingRule : public UDPICustomScalingRule
{
	GENERATED_BODY()

	virtual float GetDPIScaleBasedOnSize(FIntPoint Size) const override;
};

UCLASS()
class OPTIGAME_API UUhdDPIScalingRule : public UDPICustomScalingRule
{
	GENERATED_BODY()

	virtual float GetDPIScaleBasedOnSize(FIntPoint Size) const override;
};