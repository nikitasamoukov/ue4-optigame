// Fill out your copyright notice in the Description page of Project Settings.

#include "NumFmt.h"
#include "Kismet/KismetTextLibrary.h"

double FractionalRoundNumber(double Value, ERoundingMode InRoundingMode)
{
	double Res = -1234567890;

	switch (InRoundingMode)
	{
		case ERoundingMode::HalfToEven:
			// Rounds to the nearest place, equidistant ties go to the value which is closest to an even value: 1.5 becomes 2, 0.5 becomes 0
			Res = FMath::RoundHalfToEven(Value);
			break;

		case ERoundingMode::HalfFromZero:
			// Rounds to nearest place, equidistant ties go to the value which is further from zero: -0.5 becomes -1.0, 0.5 becomes 1.0
			Res = FMath::RoundHalfFromZero(Value);
			break;

		case ERoundingMode::HalfToZero:
			// Rounds to nearest place, equidistant ties go to the value which is closer to zero: -0.5 becomes 0, 0.5 becomes 0
			Res = FMath::RoundHalfToZero(Value);
			break;

		case ERoundingMode::FromZero:
			// Rounds to the value which is further from zero, "larger" in absolute value: 0.1 becomes 1, -0.1 becomes -1
			Res = FMath::RoundFromZero(Value);
			break;

		case ERoundingMode::ToZero:
			// Rounds to the value which is closer to zero, "smaller" in absolute value: 0.1 becomes 0, -0.1 becomes 0
			Res = FMath::RoundToZero(Value);
			break;

		case ERoundingMode::ToNegativeInfinity:
			// Rounds to the value which is more negative: 0.1 becomes 0, -0.1 becomes -1
			Res = FMath::RoundToNegativeInfinity(Value);
			break;

		case ERoundingMode::ToPositiveInfinity:
			// Rounds to the value which is more positive: 0.1 becomes 1, -0.1 becomes 0
			Res = FMath::RoundToPositiveInfinity(Value);
			break;

		default:
			break;
	}

	return Res;
}

FString ConvNumToString(int Digits, int PointPos, int DigitsCount)
{
	auto Res = FString::FromInt(Digits);

	if (Res.Len() != DigitsCount)
	{
		LOG();
		if (Res.Len() > DigitsCount)
			Res.LeftInline(DigitsCount);
		for (int i = 0; i < 100 && Res.Len() < DigitsCount; i++)
		{
			Res.AppendChar('0');
		}
	}

	if (PointPos < DigitsCount)
	{
		Res.InsertAt(PointPos, '.');
	}

	return Res;
}

FString UNumFmt::NumFmt(double Num, int Digits, ERoundingMode RoundingMode)
{
	if (Num == 0)
		return "0";

	int	   Exp = FMath::Floor(log10(FMath::Abs(Num)));
	double Mant = FMath::Abs(Num) / FMath::Pow(10.0, Exp);
	if (Mant >= 10)
	{
		Mant /= 10;
		Exp++;
	}
	if (Mant < 1)
	{
		Mant *= 10;
		Exp--;
	}
	if (Mant >= 10 || Mant < 1)
	{
		LOG();
	}
	// return FString::FromInt(Mant) + "*10^" + FString::FromInt(Exp);

	int MultToInt = FMath::RoundHalfFromZero(FMath::Pow(10.0, Digits - 1));
	int NumDigits = FractionalRoundNumber(Mant * MultToInt, RoundingMode);
	// return FString::FromInt(NumDigits) + "<" + FString::FromInt(MultToInt);

	if (NumDigits >= MultToInt * 10)
	{
		NumDigits = FractionalRoundNumber(NumDigits / 10.0f, RoundingMode);
		Exp++;
	}
	if (NumDigits >= MultToInt * 10)
	{
		LOG();
	}

	if (Exp < 0)
	{
		return UKismetTextLibrary::Conv_DoubleToText(
			Num,
			RoundingMode,
			false,
			false,
			1,
			324,
			Digits - 1,
			Digits - 1)
			.ToString();
	}
	// return FString::FromInt(NumDigits) + "*10^" + FString::FromInt(Exp);

	// 123 2(123) 3
	// 123 1(12.3) 2
	// 123 0(1.23) 1
	//
	//
	//
	FString Pref;
	if (Num < 0)
		Pref = "-";

	static FString NumPostfix = "kMGTPEZY";

	if (Exp < 3)
	{
		return Pref + ConvNumToString(NumDigits, 1 + Exp - 0, Digits);
	}
	int IdxPostfix = Exp / 3 - 1;
	if (IdxPostfix < 0)
	{
		LOG();
		return "X";
	}
	if (IdxPostfix >= NumPostfix.Len())
	{
		return "XXX";
	}

	return Pref + ConvNumToString(NumDigits, 1 + Exp - 3 * (1 + IdxPostfix), Digits) + NumPostfix[IdxPostfix];
}

FText UNumFmt::NumFmtStats(double Num)
{
	return FText::FromString(NumFmt(Num, 3, HalfFromZero));
}

FText UNumFmt::NumFmtCost(double Num)
{
	return FText::FromString(NumFmt(Num, 3, HalfFromZero));
}

FText UNumFmt::NumFmtRes(double Num)
{
	return FText::FromString(NumFmt(Num, 3, HalfFromZero));
}