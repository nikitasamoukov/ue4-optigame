// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <entt/entt.hpp>
#include "FRegistry.generated.h"

USTRUCT(BlueprintType)
struct FEntity { // entt-3.12.2\test\example\custom_identifier.cpp
	GENERATED_BODY()
	using entity_type = uint64_t;
	static constexpr auto null = entt::null;

	constexpr FEntity(entity_type value = null) noexcept
		: entt(value) {}

	constexpr FEntity(const FEntity &other) noexcept
		: entt(other.entt) {}

	constexpr operator entity_type() const noexcept {
		return entt;
	}
	
	friend uint32 GetTypeHash(FEntity Val)
	{
		auto i = (uint64_t)Val;
		return GetTypeHash(i);
	}
	auto operator<=>(const FEntity&) const = default;

private:
	entity_type entt;
};

USTRUCT()
struct FRegistry
#if CPP
	: public entt::basic_registry<FEntity>
#endif
{
	GENERATED_BODY()
public:
	template <typename... Component>
	[[nodiscard]] bool has(const entity_type entity) const
	{
		return all_of<Component...>(entity);
	}
};

template <>
struct TIsPODType<FRegistry>
{
	enum
	{
		Value = false
	};
};
template <>
struct TStructOpsTypeTraits<FRegistry> : public TStructOpsTypeTraitsBase2<FRegistry>
{
	enum
	{
		WithCopy = false,
	};
};

USTRUCT(BlueprintType)
struct FEnttHandle
#if CPP
	: public entt::basic_handle<FRegistry>
#endif
{
	GENERATED_BODY()
public:
	FEnttHandle() {}
	FEnttHandle(FRegistry& Reg, FEntity Entity)
		: entt::basic_handle<FRegistry>(Reg, Entity) {}
	~FEnttHandle() {}
	FRegistry* registry() const { return (FRegistry*)entt::basic_handle<FRegistry>::registry(); }
	FEntity entity() const { return (FEntity)entt::basic_handle<FRegistry>::entity(); }

	bool IsValid() const;

	template <typename... Component>
	[[nodiscard]] decltype(auto) has() const
	{
		return all_of<Component...>();
	}

	bool friend operator==(FEnttHandle const& A, FEnttHandle const& B);
};

template <>
struct TStructOpsTypeTraits<FEnttHandle> : public TStructOpsTypeTraitsBase2<FEnttHandle>
{
	enum
	{
		WithIdenticalViaEquality = true,
	};
};