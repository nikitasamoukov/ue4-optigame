#pragma once

#include "NewGameSettings.generated.h"

USTRUCT(BlueprintType)
struct FNewGameGalaxySettings
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Static")
	FString Seed = "";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Static")
	int GalaxyGenStarsCount = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Static")
	int GalaxyGenLinkPercent = 18;
};

USTRUCT(BlueprintType)
struct FNewGameEnemySettings
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Static")
	int Count = 6;
};

USTRUCT(BlueprintType)
struct FNewGameSettings
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Static")
	FNewGameGalaxySettings Galaxy;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Static")
	FNewGameEnemySettings Enemy;
};