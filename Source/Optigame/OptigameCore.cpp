// Fill out your copyright notice in the Description page of Project Settings.

#include "OptigameCore.h"
#include "AssetRegistry/AssetRegistryModule.h"
#include "EngineUtils.h"
#include "Projection/PrStar.h"
#include "ImGuiModule.h"
#include "Engine/Engine.h"
#include "OptigameData.h"
#include "Optigame/common/ImGuiFonts.h"

#include "ImGuiWindows/ImGuiWindows.h"

// {,,UE4Editor-Core}::PrintScriptCallstack()

// Sets default values
AOptigameCore::AOptigameCore()
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PrePhysics;
}

// Called when the game starts or when spawned
void AOptigameCore::BeginPlay()
{
	Super::BeginPlay();
	LOG("AOptigameCore initializing");

	{
		auto& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");
		AssetRegistryModule.Get().SearchAllAssets(true);
	}

	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	OptigameData->Init();
	GalaxyProjector->Init(this);
}

// Called every frame
void AOptigameCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FImGuiModule::Get().GetProperties().SetInputEnabled(EnableImGui);

	static auto VarNonsense = []() -> bool {
		if (TSharedPtr<ImFontConfig> FAFontConfig = MakeShareable(new ImFontConfig()))
		{
			static const ImWchar IconRange[] = { 0, 10000, 0 };

			FAFontConfig->FontDataOwnedByAtlas = false;									// Global font data lifetime
			FAFontConfig->FontData = (void*)Consolas_data;								// Declared in binary C .h file
			FAFontConfig->FontDataSize = Consolas_size;									// Declared in binary C .h file
			FAFontConfig->SizePixels = 30;												// Size
			FAFontConfig->MergeMode = false;											// Forces ImGui to place this font into the same atlas as the previous font
			FAFontConfig->GlyphRanges = ImGui::GetIO().Fonts->GetGlyphRangesCyrillic(); // Required; instructs ImGui to use these glyphs
			FAFontConfig->GlyphMinAdvanceX = 16.f;										// Use for monospaced icons
			FAFontConfig->PixelSnapH = true;											// Better rendering (align to pixel grid)
			FAFontConfig->GlyphOffset = { 0, 3 };										// Moves icons around, for alignment with general typesets

			FImGuiModule::Get().GetProperties().AddCustomFont("Consolas", FAFontConfig);
			FImGuiModule::Get().RebuildFontAtlas();

			auto& Fonts = ImGui::GetIO().Fonts->Fonts;
			ImGui::GetIO().FontDefault = Fonts[1];
		}

		return true;
	}();

	if (EnableImGui)
	{
		static bool IsShowGalaxy = false;
		static bool IsShowData = false;

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiCond_Always, ImVec2(0, 0));
		if (ImGui::Begin("Control", nullptr,
				ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav))
		{
			if (ImGui::Button("Close"))
				EnableImGui = false;
			if (Galaxy)
				ImGui::Checkbox("Galaxy", &IsShowGalaxy);
			ImGui::Checkbox("Data", &IsShowData);
			ImGui::End();
		}

		if (Galaxy)
			if (IsShowGalaxy)
				ImGuiWindowGalaxy(this);
		if (IsShowData)
			ImGuiWindowGameData(this);
	}

	if (!Galaxy)
	{
		return;
	}

	Galaxy->Tick(DeltaTime);

	GalaxyProjector->Update();

	// if (!InstanceDrawer)
	//{
	//	UStaticMesh* MeshPtr = TryGetMesh("cargo_spaceship_test");
	//	if (MeshPtr)
	//	{
	//		//UE_LOG(LogTemp, Warning, TEXT("ships spam"));
	//		AActor* NewActor = GetWorld()->SpawnActor(AActor::StaticClass());

	//		USceneComponent* CompRoot = NewObject<USceneComponent>(NewActor);
	//		CompRoot->RegisterComponent();
	//		CompRoot->SetFlags(RF_Transient);
	//		NewActor->AddInstanceComponent(CompRoot);
	//		NewActor->SetRootComponent(CompRoot);

	//		UInstancedStaticMeshComponent* ISMComp = NewObject<UInstancedStaticMeshComponent>(NewActor);
	//		ISMComp->RegisterComponent();
	//		ISMComp->SetStaticMesh(MeshPtr);
	//		ISMComp->SetFlags(RF_Transactional);
	//		NewActor->AddInstanceComponent(ISMComp);
	//		ISMComp->AttachToComponent(CompRoot, FAttachmentTransformRules({}, {}, {}, {}));

	//		//for (int i = 0; i < 100; i++)
	//		//	ISMComp->AddInstance(FTransform(FVector(rand() % 4000 - 2000, rand() % 4000 - 2000, rand() % 4000 - 2000)));

	//		InstanceDrawer = NewActor;
	//	}
	//}

	// if (InstanceDrawer)
	//	if (UInstancedStaticMeshComponent* comp = InstanceDrawer->FindComponentByClass<UInstancedStaticMeshComponent>(); comp)
	//	{
	//		//comp->ClearInstances();
	//		//for (int i = 0; i < 100; i++)
	//		//	comp->AddInstance(FTransform(FVector(rand() % 4000 - 2000, rand() % 4000 - 2000, rand() % 4000 - 2000)));
	//	}
}

void AOptigameCore::StartNewGame(FNewGameSettings const& NewGameSettings)
{
	Galaxy = MakeShared<FGalaxy>();

	Galaxy->Init(this);
	Galaxy->Clear();
	Galaxy->Generate(NewGameSettings);

	GalaxyProjector->SetProjectGalaxy();
}

FEnttGalaxyHandle AOptigameCore::GetGalaxy()
{
	return FEnttGalaxyHandle(Galaxy.Get());
}

void AOptigameCore::SetViewStarSystem(APrStar* PrStar)
{
	if (!PrStar || !PrStar->EnttHandle.IsValid())
	{
		LOG("View Bad set star");
		return;
	}

	LOG("View Set star");

	GalaxyProjector->SetProjectStar(PrStar->EnttHandle);
}

void AOptigameCore::SetViewGalaxy()
{
	LOG("View Set Galaxy");

	GalaxyProjector->SetProjectGalaxy();
}

AOptigameCore* UOptigameCoreGetGlobal::GetOptigameCore(UObject* ContextObject)
{
	if (!ContextObject)
		return nullptr;
	UWorld* World = GEngine->GetWorldFromContextObject(ContextObject, EGetWorldErrorMode::ReturnNull);
	if (!World)
		return nullptr;
	TActorIterator<AOptigameCore> ActorIterator(World);
	return *ActorIterator;
}

// void URunnerSubsystem::Initialize(FSubsystemCollectionBase& Collection)
//{
//	FSlateApplication::Get().SetApplicationScale(1.5f);
// }
//
// void URunnerSubsystem::Deinitialize()
//{
// }