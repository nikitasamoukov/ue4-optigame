#pragma once

#include "CoreMinimal.h"

#include "Galaxy/FGalaxy.h"
#include "Galaxy/Handles/FEnttGalaxyHandle.h"
#include "GameFramework/Actor.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Projection/GalaxyProjector.h"
#include "StartGame/NewGameSettings.h"

#include "OptigameCore.generated.h"

class APrSun;
class APrStar;
class APrPlanet;

UCLASS()
class OPTIGAME_API AOptigameCore : public AActor
{
	GENERATED_BODY()

public:
	AOptigameCore();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void StartNewGame(FNewGameSettings const& NewGameSettings);

	UFUNCTION(BlueprintCallable)
	FEnttGalaxyHandle GetGalaxy();

	UFUNCTION(BlueprintCallable)
	void SetViewStarSystem(APrStar* PrStar);

	UFUNCTION(BlueprintCallable)
	void SetViewGalaxy();

	TSharedPtr<FGalaxy> Galaxy;

	// variables controll ---------------------------------------------------------------

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool EnableImGui = true;

	// variables from editor ---------------------------------------------------------------

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Actors Ref")
	AGalaxyProjector* GalaxyProjector = nullptr;
};

UCLASS()
class OPTIGAME_API UOptigameCoreGetGlobal : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintPure, meta = (HidePin = "ContextObject", DefaultToSelf = "ContextObject", DisplayName = "Get Optigame Core", CompactNodeTitle = "Optigame Core", Keywords = "blueprint"), Category = "Optigame")
	static AOptigameCore* GetOptigameCore(UObject* ContextObject);
};

// UCLASS()
// class URunnerSubsystem : public UEditorSubsystem
//{
//	GENERATED_BODY()
// public:
//	// Begin USubsystem
//	virtual void Initialize(FSubsystemCollectionBase& Collection);
//	virtual void Deinitialize();
//	// End USubsystem
//
// private:
//	// All my variables
//};