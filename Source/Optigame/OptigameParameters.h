#pragma once

#include "OptigameParameters.generated.h"

USTRUCT(BlueprintType)
struct FGalaxyPathColors
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FVector4 Move = FVector4(0.3, 1, 0.3, 0.1);
};

USTRUCT(BlueprintType)
struct FSceneUiParam
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double GalaxyStarCircleRange = 50;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double GalaxyStarCircleWidth = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double GalaxyStarConnectWidth = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double GalaxyPathWidth = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FGalaxyPathColors GalaxyPathColors;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double GalaxyPathSideDist = 20;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double GalaxyPathConeWidth = 20;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double GalaxyPathConeHeight = 20;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FName MeshNameLine = "SceneUiCylCustom";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FName MeshNameCone = "SceneUiConeCustom";
};

USTRUCT(BlueprintType)
struct FTurretTypeInfo
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FName MeshPart1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double WeaponOffsetX = 0;
};

USTRUCT(BlueprintType)
struct FShipsParam
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double SideSpeedMul = 0.25;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double AccelerateMul = 0.2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double SpeedMul = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double RotateMul = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double WeaponScaleSmall = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double WeaponScaleMedium = 5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double WeaponScaleLarge = 40;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FTurretTypeInfo TurretInfoSmall = { "Turret_small_part1", 101.141 };
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FTurretTypeInfo TurretInfoMedium = { "Turret_medium_part1", 181.907 };
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FTurretTypeInfo TurretInfoLarge = { "Turret_large_part1", 426.332 };
};

USTRUCT(BlueprintType)
struct FOptigameVars
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double Ups = 20;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Variable")
	double FrameTime = 1 / Ups; // Dont assign its manually
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double DayTime = 5;		   // 1 game day - X sec
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	int DaysInMonth = 30;	   // resources period

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double PlanetOrbitMin = 200;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double PlanetOrbitMax = 1000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double PlanetOrbitMinPeriod = 10000;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double PlanetDistMin = 50;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FName BasicShipHullId = "ship_m_hull_small";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double StationModuleCostScale = 0.1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double GamePlaneHeight = 50;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double WarpZoneDistance = 1300;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	double WarpZoneRadius = 150;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FSceneUiParam SceneUi;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variable")
	FShipsParam Ships;

	// Called on data init
	void Assign(FOptigameVars const& Vars);
};

extern FOptigameVars GOptigameVars;