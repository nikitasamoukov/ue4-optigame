// Copyright Epic Games, Inc. All Rights Reserved.

#include "Optigame.h"

#include "Modules/ModuleManager.h"

class FOptigameGameModule : public IModuleInterface
{
public:
	virtual void StartupModule() override;
	virtual void ShutdownModule() override {}
	virtual bool IsGameModule() const override
	{
		return true;
	}
};

void FOptigameGameModule::StartupModule()
{
	FString ShaderDirectory = FPaths::Combine(FPaths::ProjectDir(), TEXT("Shaders"));
	// AddShaderSourceDirectoryMapping("/Shaders", ShaderDirectory);
}

IMPLEMENT_PRIMARY_GAME_MODULE(FOptigameGameModule, Optigame, "Optigame");

/*
 *
{,,UE4Editor-Core}::PrintScriptCallstack()

 */