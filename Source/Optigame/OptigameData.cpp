// Fill out your copyright notice in the Description page of Project Settings.

#include "OptigameData.h"

#include "Engine/Texture2D.h"
#include "Misc/AssetRegistryQuerry.h"
#include "Internationalization/StringTable.h"
#include "Internationalization/StringTableCore.h"

TMap<FName, FString> CollectStringTables(TMap<FName, UStringTable*> TablesMap)
{
	TMap<FName, FString> Res;

	for (auto& Pair : TablesMap)
	{
		Pair.Value->GetStringTable()->EnumerateSourceStrings([&](FString const& Key, FString const& Value) {
			Res.Add(*Key, Value);
			return true;
		});
	}

	return Res;
}

void FEmpireIcons::Init()
{
	// Icons.Empty();
	// for (auto& Pair : GetAllTextures("/Game/Galaxy/Del/UI/Textures/EmpireIcons"))
	// {
	// 	Icons.Add(Pair.Value);
	// }

	IconsMap.Empty();

	for (auto Tex : Icons)
	{
		IconsMap.FindOrAdd(FName(Tex->GetName())) = Tex;
	}
}
void UOptigameDataAsset::Init()
{
	EmpireIcons.Init();
	GOptigameVars.Assign(OptigameVars);

	TexturesMap = GetAllTextures("/Game");

	StringsMap = CollectStringTables(GetAllStringTables("/Game/Galaxy/Del/Data"));

	for (auto ObjectsDataAsset : BuildingsData)
		if (ObjectsDataAsset)
			BreedRegistry.LoadBreed(ObjectsDataAsset, LBreedRegistry::EBreedType::Building);

	for (auto ObjectsDataAsset : ResourcesData)
		if (ObjectsDataAsset)
			BreedRegistry.LoadBreed(ObjectsDataAsset, LBreedRegistry::EBreedType::Resource);

	for (auto ObjectsDataAsset : ShipModulesData)
		if (ObjectsDataAsset)
			BreedRegistry.LoadBreed(ObjectsDataAsset, LBreedRegistry::EBreedType::ShipModule);

	for (auto ObjectsDataAsset : EmpireSpawnData)
		if (ObjectsDataAsset)
			BreedRegistry.LoadBreed(ObjectsDataAsset, LBreedRegistry::EBreedType::EmpireSpawn);

	IsLoaded = true;
}

FText UOptigameDataAsset::GetFTextFromId(FName Id)
{
	FText TextStr;
	if (StringsMap.Contains(Id))
	{
		TextStr = FText::FromString(StringsMap[Id]);
	}
	else
	{
		LOG("GetFTextFromId no entry " + Id.ToString());
		TextStr = FText::FromString("<unknown " + Id.ToString() + ">");
	}
	return TextStr;
}

FString UOptigameDataAsset::GetFStringFromId(FName Id)
{
	FString TextStr;
	if (StringsMap.Contains(Id))
	{
		TextStr = StringsMap[Id];
	}
	else
	{
		LOG("GetFStringFromId no entry:" + Id.ToString());
		TextStr = "<unknown " + Id.ToString() + ">";
	}
	return TextStr;
}

FEnttHandle UOptigameDataAsset::GetBreedFromId(FName Id)
{
	auto H = BreedRegistry.GetBreed(Id);
	return { *H.registry(), H.entity() };
}

UOptigameDataAsset* UOptigameProjectSettings::GetOptigameData()
{
	return GetDefault<UOptigameProjectSettings>()->OptigameDataAsset.LoadSynchronous();
}