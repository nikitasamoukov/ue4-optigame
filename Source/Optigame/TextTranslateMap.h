// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataAsset.h"
#include "TextTranslateMap.generated.h"

UCLASS(BlueprintType)
class UTextTranslateMap : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FName, FText> Map;
};