// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Optigame/galaxy/handles/FEnttStarHandle.h"
#include "PrStar.generated.h"

UCLASS()
class OPTIGAME_API APrStar : public AActor
{
	GENERATED_BODY()

public:
	APrStar();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent)
	void ForceRenderUpdate();

	UFUNCTION(BlueprintCallable)
	TArray<APrStar*> GetPositiveConnections();

	UFUNCTION(BlueprintCallable)
	FEnttStarHandle GetHandle();

	// Internal

	void	ProjSet(FEnttStarHandle Handle);
	FVector GetPosition();

	UPROPERTY(VisibleAnywhere)
	USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere)
	FEnttStarHandle EnttHandle;
};