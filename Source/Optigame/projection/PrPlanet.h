// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Optigame/Galaxy/Handles/FEnttPlanetHandle.h"
#include "PrPlanet.generated.h"

UCLASS()
class OPTIGAME_API APrPlanet : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APrPlanet();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	double GetSize();
	UFUNCTION(BlueprintCallable)
	EPlanetType GetType();

	// Internal

	void	ProjSet(FEnttPlanetHandle Handle);
	FVector GetPosition();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEnttPlanetHandle EnttHandle;
};