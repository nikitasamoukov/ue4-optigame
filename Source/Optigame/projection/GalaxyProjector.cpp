// Fill out your copyright notice in the Description page of Project Settings.

#include "GalaxyProjector.h"
#include "PrStar.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "PrSun.h"
#include "Optigame/Galaxy/FGalaxy.h"
#include "Optigame/OptigameCore.h"
#include "PrPlanet.h"
#include "Optigame/Galaxy/Handles/FEnttPlanetHandle.h"
#include "Optigame/Common/common.h"
#include "Optigame/Misc/AssetRegistryQuerry.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Components/ComponentsPr.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"
#include "PrStation.h"
#include "Optigame/Galaxy/Components/ComponentsShip.h"
#include "Optigame/Galaxy/Helpers/HelperActions.h"
#include "Components/InstancedStaticMeshComponent.h"

AGalaxyProjector::AGalaxyProjector()
{
}

AGalaxyProjector::~AGalaxyProjector()
{
}

void AGalaxyProjector::SetProjectStar(FEnttHandle Star)
{
	if (CurrentViewStar == Star)
		return;

	auto& Reg = OptigameCore->Galaxy->Reg;

	ClearProjection();

	CurrentViewStar = Star;

	CreatePrSun(Star);

	if (Star.IsValid() && Star.all_of<FCompStar>())
	{
		for (auto& E : Star.get<FCompStar>().Objects)
		{
			CreatePrPlanet({ Reg, E });
		}
	}

	auto CStar = Star.try_get<FCompStar>();
	if (!CStar)
	{
		LOG();
		return;
	}

	for (auto& Connect : CStar->Connects)
	{
		auto WarpZone = Connect.WarpZone;
		CreatePrWarpZone({ Reg, WarpZone });
	}
}

void AGalaxyProjector::SetProjectGalaxy()
{
	auto& Reg = OptigameCore->Galaxy->Reg;

	ClearProjection();
	for (auto [E, CStar] : Reg.view<FCompStar>().each())
	{
		FEnttHandle Handle(Reg, E);
		CreatePrStar(Handle);
	}

	for (auto [E, CPrStar] : Reg.view<FCompPrStar>().each())
	{
		CPrStar.Actor->ForceRenderUpdate();
	}
}

void ActorDestroyFull(AActor* Actor)
{
	auto Children = Actor->Children;
	for (auto A : Children)
		ActorDestroyFull(A);

	{
		TArray<class AActor*> Arr;
		Actor->GetAttachedActors(Arr);
		for (auto A : Arr)
			ActorDestroyFull(A);
	}

	Actor->Destroy();
}

void AGalaxyProjector::ClearProjection()
{
	auto& Reg = OptigameCore->Galaxy->Reg;

	for (auto [E, CPrStar] : Reg.view<FCompPrStar>().each())
	{
		ActorDestroyFull(CPrStar.Actor);
		Reg.remove<FCompPrStar>(E);
		Reg.remove<FCompCurrentTimePos>(E);
	}

	for (auto [E, CPrSun] : Reg.view<FCompPrSun>().each())
	{
		ActorDestroyFull(CPrSun.Actor);
		Reg.remove<FCompPrSun>(E);
		Reg.remove<FCompCurrentTimePos>(E);
	}

	for (auto [E, CPrPlanet] : Reg.view<FCompPrPlanet>().each())
	{
		ActorDestroyFull(CPrPlanet.Actor);
		Reg.remove<FCompPrPlanet>(E);
		Reg.remove<FCompCurrentTimePos>(E);
	}

	for (auto [E, CPrStation] : Reg.view<FCompPrStation>().each())
	{
		ActorDestroyFull(CPrStation.Actor);
		Reg.remove<FCompPrStation>(E);
		Reg.remove<FCompCurrentTimePos>(E);
	}

	for (auto [E, CPrWarpZone] : Reg.view<FCompPrWarpZone>().each())
	{
		ActorDestroyFull(CPrWarpZone.Actor);
		Reg.remove<FCompPrWarpZone>(E);
		Reg.remove<FCompCurrentTimePos>(E);
	}

	for (auto [E, CShip] : Reg.view<FCompShip>().each())
	{
		Reg.remove<FCompCurrentTimePos>(E);
	}

	for (auto& Pair : ComponentsMap)
	{
		UpdRenderComp(Pair.Value, {});
	}

	CurrentViewStar = {};
}

void AGalaxyProjector::Update()
{
	if (CurrentViewStar.IsValid())
	{
		UpdateSystem();
	}
	else
	{
		UpdateGalaxy();
	}
}

void AGalaxyProjector::UpdateSystem()
{
	UpdateSystemMeshes();
	UpdateSystemPath();
}

void AGalaxyProjector::UpdateSystemMeshes()
{
	// LOG("R1: " + FString::FromInt((long long)(void*)CurrentViewStar.registry()) +" R2: " + FString::FromInt((int)CurrentViewStar.entity()) );

	auto& Reg = OptigameCore->Galaxy->Reg;

	double ProgressToCurrentFrame = OptigameCore->Galaxy->ProgressToCurrentFrame;

	for (auto [E, CTransform, CCurrentTimePos] : Reg.view<FCompTransform, FCompCurrentTimePos>().each())
	{
		CCurrentTimePos.FromTrs(CTransform, ProgressToCurrentFrame);
	}

	auto& CStar = CurrentViewStar.get<FCompStar>();

	for (auto& E : CStar.Objects)
	{
		auto CColony = Reg.try_get<FCompColony>(E);
		if (!CColony)
			continue;
		if (!Reg.valid(CColony->Station))
			continue;

		if (Reg.all_of<FCompPrStation>(CColony->Station))
			continue;

		FCompPrStation CPrStation;
		CPrStation.Actor = CreatePrStationActor({ Reg, CColony->Station });
		Reg.emplace<FCompPrStation>(CColony->Station, CPrStation);
	}

	TMap<FName, TArray<FTransform>> ShipsDraw;

	// Ships render Calc
	for (auto& E : CStar.Ships)
	{
		auto CMesh = Reg.try_get<FCompMesh>(E);
		auto CSpaceObject = Reg.try_get<FCompSpaceObject>(E);
		if (!CMesh && !CSpaceObject)
		{
			LOG();
			continue;
		}

		auto OptigameData = UOptigameProjectSettings::GetOptigameData();
		auto HullBreed = OptigameData->BreedRegistry.GetBreed(CSpaceObject->HullBreedId);
		if (!HullBreed.IsValid())
		{
			LOG();
			continue;
		}
		auto CShipModuleHull = HullBreed.try_get<FCompShipModuleHull>();
		if (!CShipModuleHull)
		{
			LOG();
			continue;
		}

		if (!Reg.has<FCompCurrentTimePos>(E))
		{
			auto CTransform = Reg.try_get<FCompTransform>(E);
			if (!CTransform)
			{
				LOG();
				continue;
			}
			Reg.emplace<FCompCurrentTimePos>(E).FromTrs(*CTransform, ProgressToCurrentFrame);
		}

		// Hull
		auto& CCurrentTimePos = Reg.get<FCompCurrentTimePos>(E);
		auto  Pos = CCurrentTimePos.Pos;
		auto  Quat = CCurrentTimePos.Quat;

		FTransform Trs;
		Trs.SetLocation(Pos);
		Trs.SetRotation(Quat);
		Trs.SetScale3D({ 0.01f, 0.01f, 0.01f });

		ShipsDraw.FindOrAdd(CMesh->Name).Add(Trs);

		// Turrets
		if (CSpaceObject->Weapons.Num() > CShipModuleHull->Weapons.Num())
		{
			LOG();
			continue;
		}

		for (int i = 0; i < CSpaceObject->Weapons.Num(); i++)
		{
			auto&		WeaponSlot = CShipModuleHull->Weapons[i];
			auto const& TurretTransform = WeaponSlot.SocketTransform;

			auto& WeaponSlotInfo = CSpaceObject->Weapons[i];

			auto TurretTransformLocal = TurretTransform;

			// TurretTransformLocal.MultiplyScale3D(FVector(0.1f));

			FTransform TurretTransformGlobal = Trs;
			TurretTransformGlobal = TurretTransformLocal * TurretTransformGlobal;

			if (!WeaponSlot.bFixed)
			{
				FTurretTypeInfo TurretTypeInfo;
				if (WeaponSlot.Size == EWeaponSize::Small)
					TurretTypeInfo = GOptigameVars.Ships.TurretInfoSmall;
				if (WeaponSlot.Size == EWeaponSize::Medium)
					TurretTypeInfo = GOptigameVars.Ships.TurretInfoMedium;
				if (WeaponSlot.Size == EWeaponSize::Large)
					TurretTypeInfo = GOptigameVars.Ships.TurretInfoLarge;

				ShipsDraw.FindOrAdd(TurretTypeInfo.MeshPart1).Add(TurretTransformGlobal);
			}
			else
			{
				FName WeaponMeshName;

				auto WeaponBreed = OptigameData->BreedRegistry.GetBreed(WeaponSlotInfo.BreedId);
				if (!WeaponBreed.IsValid())
				{
					LOG();
					continue;
				}
				auto CMeshWeapon = WeaponBreed.try_get<FCompMeshWeapon>();
				if (!CMeshWeapon)
				{
					LOG();
					continue;
				}

				// TODO switch
				WeaponMeshName = CMeshWeapon->Small;

				ShipsDraw.FindOrAdd(WeaponMeshName).Add(TurretTransformGlobal);
			}
		}
	}

	// Ships render upd
	for (auto& Pair : ComponentsMap)
	{
		if (Pair.Key == GOptigameVars.SceneUi.MeshNameLine || Pair.Key == GOptigameVars.SceneUi.MeshNameCone || Pair.Key == "SelectMesh")
			continue;

		auto Comp = Pair.Value;
		if (!ShipsDraw.Contains(Pair.Key))
		{
			UpdRenderComp(Comp, {});
			continue;
		}
		UpdRenderComp(Comp, ShipsDraw[Pair.Key]);
	}

	// Ships select
	if (ComponentsMap.Contains("SelectMesh"))
	{
		TArray<FTransform> Transforms;

		for (auto& E : CStar.Ships)
		{
			if (!Reg.all_of<FCompSelectTag>(E))
				continue;
			if (!Reg.all_of<FCompCurrentTimePos>(E))
			{
				LOG();
				continue;
			}
			auto& CCurrentTimePos = Reg.get<FCompCurrentTimePos>(E);

			auto  Pos = CCurrentTimePos.Pos;
			auto  Quat = CCurrentTimePos.Quat;
			double Scl = 0.06;

			FTransform Trs;
			Trs.SetLocation(Pos);
			Trs.SetRotation(Quat);
			Trs.SetScale3D({ Scl, Scl, Scl });
			Transforms.Add(Trs);
		}

		auto Comp = ComponentsMap["SelectMesh"];
		UpdRenderComp(Comp, Transforms);
	}
}

struct FLinesDrawInfo
{
	FVector	 P1{ 0 };
	FVector	 P2{ 0 };
	FVector4 Col{ 0 };
	double	 Width{ 0 };
};

void AGalaxyProjector::UpdateSystemPath()
{
	auto& Reg = OptigameCore->Galaxy->Reg;

	auto				   CompLine = ComponentsMap[GOptigameVars.SceneUi.MeshNameLine];
	auto				   CompCone = ComponentsMap[GOptigameVars.SceneUi.MeshNameCone];
	TArray<FLinesDrawInfo> Lines;
	TArray<FLinesDrawInfo> Cones;

	// Not work because double presicion to point 2 so low.
	auto MakeTransformLine = [&](FVector const& P1, FVector const& P2, double Width) {
		FQuat	Rotation = (P2 - P1).ToOrientationQuat();
		FVector Translation = P1;
		FVector Scale3D(FVector::Distance(P1, P2) * 0.01, Width / 2 * 0.01, Width / 2 * 0.01);

		return FTransform(Rotation, Translation, Scale3D);
	};

	auto DrawArrow = [&](FVector const& P1, FVector const& P2, FVector4 const& Col) {
		auto ConeLength = GOptigameVars.SceneUi.GalaxyPathWidth * 4;
		auto ArrowLength = FVector::Dist(P1, P2);

		if (ArrowLength < 0.001)
			return;

		auto Dir = P2 - P1;
		Dir /= ArrowLength;

		if (ArrowLength > ConeLength)
			Lines.Add({ P1, P1 + Dir * (ArrowLength - ConeLength), Col, GOptigameVars.SceneUi.GalaxyPathWidth });
		Cones.Add({ P2 - Dir * ConeLength, P2, Col, GOptigameVars.SceneUi.GalaxyPathWidth * 2 });
	};

	auto DrawActionPath = [&](FEntity const& Ship, FGamePos const& CurrentGamePos, FActionBase const& Action) {
		if (Action.Path.Num() <= 1)
		{
			// local
			if (CurrentGamePos.Star == CurrentViewStar)
				DrawArrow(CurrentGamePos.Pos, Action.GamePos.Pos, GOptigameVars.SceneUi.GalaxyPathColors.Move);
			return;
		}

		FVector PrevPos = CurrentGamePos.Pos;

		for (int i = 0; i < Action.Path.Num(); i++)
		{
			FVector EndPos(0);
			FVector NextPos(0);

			auto CurrentStar = Action.Path[i];

			if (i + 1 >= Action.Path.Num()) // last path point
			{
				EndPos = Action.GamePos.Pos;
				NextPos = EndPos;
			}
			else // some point in path
			{
				auto& CurrentCStar = Reg.get<FCompStar>(CurrentStar);
				auto  NextStar = Action.Path[i + 1];
				auto  StarConnect = CurrentCStar.Connects.FindByPredicate([&](FStarConnect& E) { return E.Star == NextStar; });
				if (!StarConnect)
				{
					LOG();
					return;
				}
				auto ZoneCTransform = Reg.try_get<FCompTransform>(StarConnect->WarpZone);
				if (!ZoneCTransform)
				{
					LOG();
					return;
				}
				auto CShipWarpOffset = Reg.try_get<FCompShipWarpOffset>(Ship);
				if (!CShipWarpOffset)
				{
					LOG();
					return;
				}
				EndPos = ZoneCTransform->Pos + CShipWarpOffset->Offset;

				auto& NextCStar = Reg.get<FCompStar>(NextStar);
				auto  BackStarConnect = NextCStar.Connects.FindByPredicate([&](FStarConnect& E) { return E.Star == CurrentStar; });
				if (!BackStarConnect)
				{
					LOG();
					return;
				}
				auto BackZoneCTransform = Reg.try_get<FCompTransform>(BackStarConnect->WarpZone);
				if (!BackZoneCTransform)
				{
					LOG();
					return;
				}
				NextPos = BackZoneCTransform->Pos + CShipWarpOffset->Offset;
			}

			if (CurrentStar == CurrentViewStar)
				DrawArrow(PrevPos, EndPos, GOptigameVars.SceneUi.GalaxyPathColors.Move);

			PrevPos = NextPos;
		}
	};

	for (auto [Star, CStar] : Reg.view<FCompStar>().each())
	{
		for (auto& Ship : CStar.Ships)
		{
			if (!Reg.has<FCompSelectTag>(Ship))
				continue;
			auto CShipController = Reg.try_get<FCompShipController>(Ship);
			if (!CShipController)
			{
				LOG();
				continue;
			}
			auto CTransform = Reg.try_get<FCompTransform>(Ship);
			if (!CTransform)
			{
				LOG();
				continue;
			}

			FGamePos PrevGamePos = { Star, CTransform->CalcMixPos(OptigameCore->Galaxy->ProgressToCurrentFrame) };
			for (auto& Action : CShipController->Actions)
			{

				FActionBase* ActionBase = nullptr;

				Visit([&]<typename Type>(Type& ValRaw) {
					ValRaw.UpdateBase(Reg, PrevGamePos);
					ActionBase = &ValRaw;
				},
					Action);

				DrawActionPath(Ship, PrevGamePos, *ActionBase);

				auto TargetGamePos = UHelperActions::GetActionTargetPos(Action);
				PrevGamePos = TargetGamePos;
			}
		}
	}

	auto FillCompData = [&](TArray<FLinesDrawInfo> const& Data, auto Comp) {
		TArray<FTransform> Transforms;
		for (int i = 0; i < Data.Num(); i++)
			Transforms.Add(FTransform::Identity);

		UpdRenderComp(Comp, Transforms);

		Comp->NumCustomDataFloats = 11; // Pos1, Pos2, RGBA, Width
		auto& CompData = Comp->PerInstanceSMCustomData;
		CompData.SetNum(11 * Data.Num());

		for (int i = 0; i < Data.Num(); i++)
		{
			CompData[i * 11 + 0] = Data[i].P1.X;
			CompData[i * 11 + 1] = Data[i].P1.Y;
			CompData[i * 11 + 2] = Data[i].P1.Z;
			CompData[i * 11 + 3] = Data[i].P2.X;
			CompData[i * 11 + 4] = Data[i].P2.Y;
			CompData[i * 11 + 5] = Data[i].P2.Z;
			CompData[i * 11 + 6] = Data[i].Col.X;
			CompData[i * 11 + 7] = Data[i].Col.Y;
			CompData[i * 11 + 8] = Data[i].Col.Z;
			CompData[i * 11 + 9] = Data[i].Col.W;
			CompData[i * 11 + 10] = Data[i].Width;
		}
	};

	FillCompData(Lines, CompLine);
	FillCompData(Cones, CompCone);
	{
		TArray<FTransform> Transforms;
		for (int i = 0; i < Lines.Num(); i++)
			Transforms.Add(FTransform::Identity);

		UpdRenderComp(CompCone, Transforms);
	}
}

void AGalaxyProjector::UpdateGalaxy()
{
	UpdateGalaxyMeshes();
	UpdateGalaxyPath();
}

void AGalaxyProjector::UpdateGalaxyMeshes()
{
}

void AGalaxyProjector::UpdateGalaxyPath()
{
}

void AGalaxyProjector::Init(AOptigameCore* OptigameCorePtr)
{
	OptigameCore = OptigameCorePtr;

	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	auto ShipComponents = OptigameData->BreedRegistry.GetBreeds(LBreedRegistry::EBreedType::ShipModule);
	for (auto& Pair : ShipComponents)
	{
		auto& H = Pair.Value;
		auto  CMesh = H.try_get<FCompMesh>();
		if (!CMesh)
			continue;

		AddMeshRenderComp(CMesh->Name);
	}

	for (auto& Pair : ShipComponents)
	{
		auto& H = Pair.Value;
		auto  CMeshWeapon = H.try_get<FCompMeshWeapon>();
		if (!CMeshWeapon)
			continue;

		AddMeshRenderComp(CMeshWeapon->Small);
		AddMeshRenderComp(CMeshWeapon->Medium);
		AddMeshRenderComp(CMeshWeapon->Large);
	}

	AddMeshRenderComp("SelectMesh");
	AddMeshRenderComp(GOptigameVars.SceneUi.MeshNameLine);
	AddMeshRenderComp(GOptigameVars.SceneUi.MeshNameCone);

	AddMeshRenderComp(GOptigameVars.Ships.TurretInfoSmall.MeshPart1);
	AddMeshRenderComp(GOptigameVars.Ships.TurretInfoMedium.MeshPart1);
	AddMeshRenderComp(GOptigameVars.Ships.TurretInfoLarge.MeshPart1);
}

APrStar* AGalaxyProjector::CreatePrStarActor(FEnttHandle Star)
{
	auto Mesh = TryGetMesh("Sun_Bb_00", "/Game/Galaxy/Del/Models/Suns");

	if (!Mesh)
	{
		LOG("BP Star need mesh");
		return nullptr;
	}
	if (!PrStarBp)
	{
		LOG("BP Star Actor is false");
		return nullptr;
	}

	APrStar* PrStar = OptigameCore->GetWorld()->SpawnActor<APrStar>(PrStarBp);
	PrStar->EnttHandle = FEnttStarHandle(Star);

	{
		UStaticMeshComponent* Comp = NewObject<UStaticMeshComponent>(PrStar);
		Comp->RegisterComponent();
		Comp->SetStaticMesh(Mesh);
		Comp->SetFlags(RF_Transient);
		Comp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		PrStar->AddInstanceComponent(Comp);
		Comp->AttachToComponent(PrStar->GetRootComponent(), FAttachmentTransformRules({}, {}, {}, {}));
	}

	PrStar->SetActorLocation(PrStar->GetPosition());
	// FTransform ActorTrs(Pos);
	// UE_LOG(LogTemp, Warning, TEXT("%f %f"), Pos.X, Pos.Y);
	// UE_LOG(LogTemp, Warning, TEXT("%f %f"), ActorTrs.GetTranslation().X, ActorTrs.GetTranslation().Y);

	// auto TestT = NewActor->GetActorTransform();
	// UE_LOG(LogTemp, Warning, TEXT("%f %f"), TestT.GetTranslation().X, TestT.GetTranslation().Y);
	return PrStar;
}

APrSun* AGalaxyProjector::CreatePrSunActor(FEnttHandle Star)
{
	UE_LOG(LogTemp, Warning, TEXT("CreatePrSunActor"));

	if (!PrSunBp)
	{
		UE_LOG(LogTemp, Warning, TEXT("BP Star Actor is false"));
		return nullptr;
	}

	APrSun* PrSun = OptigameCore->GetWorld()->SpawnActor<APrSun>(PrSunBp);
	PrSun->EnttHandle = FEnttStarHandle(Star);
	PrSun->SetActorLocation(FVector(0));

	FCompStar* CStar = Star.try_get<FCompStar>();
	if (!CStar)
	{
		LOG();
		return nullptr;
	}

	{
		AActor* SunRenderingActor = OptigameCore->GetWorld()->SpawnActor(TemplateYellowSun);

		if (SunRenderingActor)
		{
			SunRenderingActor->SetActorLocation(FVector(0));

			UStaticMeshComponent* Comp = Cast<UStaticMeshComponent>(SunRenderingActor->GetComponentByClass(UStaticMeshComponent::StaticClass()));

			if (!Comp)
				UE_LOG(LogTemp, Warning, TEXT("BAD CAST"));

			if (Comp)
				Comp->SetVisibility(true, true);

			PrSun->Children.Add(SunRenderingActor);

			SunRenderingActor->AttachToComponent(PrSun->GetRootComponent(), FAttachmentTransformRules({}, {}, {}, {}));

			SunRenderingActor->SetActorScale3D(FVector(0.01 * PrSun->GetSize()));
		}
		else
		{
			LOG("BAD SPAWN");
		}
	}

	PrSun->ForceRenderUpdate();

	return PrSun;
}

APrPlanet* AGalaxyProjector::CreatePrPlanetActor(FEnttHandle Planet)
{
	UE_LOG(LogTemp, Warning, TEXT("CreatePrPlanetActor"));

	if (!PrSunBp)
	{
		UE_LOG(LogTemp, Warning, TEXT("BP Star Actor is false"));
		return nullptr;
	}

	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	if (!OptigameData)
	{
		UE_LOG(LogTemp, Warning, TEXT("OptigameData = OptigameCore->OptigameData; not true"));
		return nullptr;
	}

	APrPlanet* PrPlanet = OptigameCore->GetWorld()->SpawnActor<APrPlanet>(PrPlanetBp);
	PrPlanet->EnttHandle = FEnttPlanetHandle(Planet);
	PrPlanet->SetActorLocation(FVector(0));

	{
		AActor* PlanetRenderingActor = OptigameCore->GetWorld()->SpawnActor<AActor>();

		if (PlanetRenderingActor)
		{
			PlanetRenderingActor->SetActorLocation(FVector(0));

			{
				USceneComponent* Comp = NewObject<USceneComponent>(PlanetRenderingActor);
				Comp->RegisterComponent();
				Comp->SetFlags(RF_Transient);
				PlanetRenderingActor->AddInstanceComponent(Comp);
				PlanetRenderingActor->SetRootComponent(Comp);
			}

			if (OptigameData->MeshesMap.Find("Planet"))
			{
				UStaticMeshComponent* Comp = NewObject<UStaticMeshComponent>(PlanetRenderingActor);
				Comp->RegisterComponent();
				Comp->SetStaticMesh(OptigameData->MeshesMap["Planet"]);
				auto PlanetType = PrPlanet->EnttHandle.GetType();
				if (auto Mat = OptigameData->PlanetMaterialsMap.Find(*GetEnumValueAsString(PlanetType)); Mat)
				{
					if (*Mat)
						Comp->SetMaterial(0, *Mat);
					else
						LOG("No mat:" + GetEnumValueAsString(PlanetType));
				}
				else
				{
					LOG("No mat:" + GetEnumValueAsString(PlanetType));
				}
				Comp->SetFlags(RF_Transient);
				Comp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				PlanetRenderingActor->AddInstanceComponent(Comp);
				Comp->AttachToComponent(PlanetRenderingActor->GetRootComponent(), FAttachmentTransformRules({}, {}, {}, {}));
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("MeshesMap no Planet"));
			}

			PrPlanet->Children.Add(PlanetRenderingActor);
			PlanetRenderingActor->AttachToComponent(PrPlanet->GetRootComponent(), FAttachmentTransformRules({}, {}, {}, {}));
			PlanetRenderingActor->SetActorScale3D(FVector(0.005 * PrPlanet->GetSize()));
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("BAD SPAWN"));
		}
	}

	PrPlanet->SetActorLocation(PrPlanet->GetPosition());

	return PrPlanet;
}

APrStation* AGalaxyProjector::CreatePrStationActor(FEnttHandle Station)
{
	APrStation* PrStation = OptigameCore->GetWorld()->SpawnActor<APrStation>(PrStationBp);
	PrStation->ProjSet(FEnttStationHandle(Station));
	PrStation->Update();

	return PrStation;
}

APrWarpZone* AGalaxyProjector::CreatePrWarpZoneActor(FEnttHandle WarpZone)
{
	APrWarpZone* PrWarpZone = OptigameCore->GetWorld()->SpawnActor<APrWarpZone>(PrWarpZoneBp);
	PrWarpZone->ProjSet(WarpZone);

	return PrWarpZone;
}

void AGalaxyProjector::CreatePrStar(FEnttHandle Star)
{
	if (!Star.all_of<FCompTransform>())
	{
		LOG("transform?");
		return;
	}
	(void)Star.emplace_or_replace<FCompCurrentTimePos>().FromTrs(Star.get<FCompTransform>(), OptigameCore->Galaxy->ProgressToCurrentFrame);

	FCompPrStar CPrStar;
	CPrStar.Actor = CreatePrStarActor(Star);
	if (!CPrStar.Actor)
	{
		LOG("Actor bad spawn");
		return;
	}
	Star.emplace_or_replace<FCompPrStar>(CPrStar);
}

void AGalaxyProjector::CreatePrSun(FEnttHandle Star)
{
	if (!Star.all_of<FCompTransform>())
	{
		LOG("transform?");
		return;
	}
	(void)Star.emplace_or_replace<FCompCurrentTimePos>().FromTrs(Star.get<FCompTransform>(), OptigameCore->Galaxy->ProgressToCurrentFrame);

	FCompPrSun CPrSun;
	CPrSun.Actor = CreatePrSunActor(Star);
	if (!CPrSun.Actor)
	{
		LOG("Actor bad spawn");
		return;
	}
	Star.emplace_or_replace<FCompPrSun>(CPrSun);
}

void AGalaxyProjector::CreatePrPlanet(FEnttHandle Planet)
{
	if (!Planet.all_of<FCompTransform>())
	{
		LOG("transform?");
		return;
	}
	Planet.emplace_or_replace<FCompCurrentTimePos>().FromTrs(Planet.get<FCompTransform>(), OptigameCore->Galaxy->ProgressToCurrentFrame);

	FCompPrPlanet CPrPlanet;
	CPrPlanet.Actor = CreatePrPlanetActor(Planet);
	if (!CPrPlanet.Actor)
	{
		LOG("Actor bad spawn");
		return;
	}
	Planet.emplace_or_replace<FCompPrPlanet>(CPrPlanet);
}

void AGalaxyProjector::CreatePrWarpZone(FEnttHandle Planet)
{
	if (!Planet.all_of<FCompTransform>())
	{
		LOG("transform?");
		return;
	}
	Planet.emplace_or_replace<FCompCurrentTimePos>().FromTrs(Planet.get<FCompTransform>(), OptigameCore->Galaxy->ProgressToCurrentFrame);

	FCompPrWarpZone CPrWarpZone;
	CPrWarpZone.Actor = CreatePrWarpZoneActor(Planet);
	if (!CPrWarpZone.Actor)
	{
		LOG("Actor bad spawn");
		return;
	}
	Planet.emplace_or_replace<FCompPrWarpZone>(CPrWarpZone);
}

void AGalaxyProjector::AddMeshRenderComp(FName MeshName)
{
	auto OptigameData = UOptigameProjectSettings::GetOptigameData();
	if (ComponentsMap.Contains(MeshName))
		return;
	auto EPtr = OptigameData->MeshesMap.Find(MeshName);
	if (!EPtr)
	{
		LOG();
		return;
	}
	auto						   MeshPtr = *EPtr;
	UInstancedStaticMeshComponent* IsmComp = NewObject<UInstancedStaticMeshComponent>(this);
	IsmComp->RegisterComponent();
	IsmComp->SetStaticMesh(MeshPtr);
	IsmComp->SetFlags(RF_Transactional);
	IsmComp->SetSimulatePhysics(false);
	IsmComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	IsmComp->SetGenerateOverlapEvents(false);

	AddInstanceComponent(IsmComp);
	// ISMComp->AttachToComponent(CompRoot, FAttachmentTransformRules({}, {}, {}, {}));
	ComponentsMap.Add(MeshName, IsmComp);
}

void AGalaxyProjector::UpdRenderComp(UInstancedStaticMeshComponent* Comp, TArray<FTransform> const& Transforms)
{
	if (Comp == nullptr)
	{
		LOG();
		return;
	}
	auto NumComp = Comp->GetInstanceCount();
	auto NumReq = Transforms.Num();
	if (NumComp < NumReq)
	{
		TArray<FTransform> Tmp;
		Tmp.SetNum(NumReq - NumComp);
		Comp->AddInstances(Tmp, false);
	}
	else if (NumComp > NumReq)
	{
		int32 LastInstance = NumComp - 1;
		int32 RemoveCount = NumComp - NumReq;
		for (int32 I = 0; I < RemoveCount; I++)
		{
			Comp->RemoveInstance(LastInstance);
			LastInstance--;
		}
	}
	Comp->BatchUpdateInstancesTransforms(0, Transforms, true, true, true);
}