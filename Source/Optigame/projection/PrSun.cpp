// Fill out your copyright notice in the Description page of Project Settings.

#include "PrSun.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"

APrSun::APrSun()
{
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);
}

void APrSun::BeginPlay()
{
	Super::BeginPlay();
}

void APrSun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

double APrSun::GetSize()
{
	if (!EnttHandle.IsValid())
		return {};

	return EnttHandle.get<FCompStar>().Size;
}

void APrSun::ProjSet(FEnttStarHandle Handle)
{
	EnttHandle = Handle;
}