// Fill out your copyright notice in the Description page of Project Settings.

#include "PrStation.h"
#include "Components/StaticMeshComponent.h"
#include "Optigame/OptigameData.h"
#include "Optigame/Galaxy/Helpers/HelperStation.h"
#include "Optigame/galaxy/handles/FEnttPlanetHandle.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"

APrStation::APrStation()
{
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules({}, {}, {}, {}));
}

void APrStation::BeginPlay()
{
	Super::BeginPlay();
}

void APrStation::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Update();
}

void APrStation::ProjSet(FEnttStationHandle Handle)
{
	EnttHandle = Handle;
}

void APrStation::Update()
{
	UpdateMesh();

	if (EnttHandle.IsValid())
	{
		auto&			  CStation = EnttHandle.get<FCompStation>();
		FEnttPlanetHandle Planet(*EnttHandle.registry(), CStation.Planet);
		if (Planet.IsValid())
		{
			auto Pos = Planet.GetPosition();
			Pos.Z += Planet.get<FCompPlanet>().Size / 2;
			FTransform Trs(Pos);

			SetActorTransform(Trs);
		}
	}
}

void APrStation::UpdateMesh()
{
	auto HullMeshId = EnttHandle.GetHullMeshId();
	if (HullMeshId == CurrentHullMeshId)
		return;

	if (HullMeshId != "None")
	{
		if (CurrentHullMeshId == "None")
			ShowMesh();
		auto OptigameData = UOptigameProjectSettings::GetOptigameData();
		auto MeshPtr = OptigameData->MeshesMap.Find(*HullMeshId.ToString());
		if (MeshPtr)
		{
			MeshComponent->SetStaticMesh(*MeshPtr);
		}
		else
		{
			LOG();
		}
	}
	CurrentHullMeshId = HullMeshId;
}

void APrStation::HideMesh()
{
	MeshComponent->SetVisibility(false);
}

void APrStation::ShowMesh()
{
	MeshComponent->SetVisibility(true);
	double Scl = 0.012;
	MeshComponent->SetWorldScale3D(FVector(Scl, Scl, Scl));
}