// Fill out your copyright notice in the Description page of Project Settings.

#include "PrWarpZone.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"

// Sets default values
APrWarpZone::APrWarpZone()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);
}

// Called every frame
void APrWarpZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!EnttHandle.IsValid())
		return;
	auto CTransform = EnttHandle.try_get<FCompTransform>();

	if (!CTransform)
	{
		LOG();
		return;
	}

	FVector Pos = CTransform->Pos;
	SetActorLocation(Pos);
}

void APrWarpZone::ProjSet(FEnttHandle Handle)
{
	EnttHandle = Handle;
}