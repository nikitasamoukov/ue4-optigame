// Fill out your copyright notice in the Description page of Project Settings.

#include "PrStar.h"
#include "Optigame/Galaxy/Components/ComponentsPr.h"

APrStar::APrStar()
{
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);
}

void APrStar::ProjSet(FEnttStarHandle Handle)
{
	EnttHandle = Handle;
}

TArray<APrStar*> APrStar::GetPositiveConnections()
{
	TArray<APrStar*> Res;

	for (auto& OtherStarHandle : EnttHandle.GetPositiveConnections())
	{
		if (OtherStarHandle.all_of<FCompPrStar>())
		{
			Res.Add(OtherStarHandle.get<FCompPrStar>().Actor);
		}
	}

	return Res;
}

FEnttStarHandle APrStar::GetHandle()
{
	return EnttHandle;
}

FVector APrStar::GetPosition()
{
	return EnttHandle.GetPosition();
}

void APrStar::BeginPlay()
{
	Super::BeginPlay();
}

void APrStar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FTransform Trs(GetPosition());

	if (EnttHandle.IsValid())
	{
		SetActorTransform(Trs);
	}
}