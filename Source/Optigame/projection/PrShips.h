// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PrShips.generated.h"

UCLASS()
class OPTIGAME_API APrShips : public AActor
{
	GENERATED_BODY()

public:
	APrShips();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	// Internal

	void SetStar(FEntity E);

	UPROPERTY(VisibleAnywhere)
	USceneComponent* SceneComponent;
};