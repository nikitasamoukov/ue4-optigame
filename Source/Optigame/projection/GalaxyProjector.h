// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PrWarpZone.h"
#include "GameFramework/Actor.h"
#include "Optigame/OptigamePch.h"
#include "GalaxyProjector.generated.h"

class UInstancedStaticMeshComponent;
class AOptigameCore;
class FGalaxy;
class APrStar;
class APrSun;
class APrPlanet;
class APrStation;
class APrWarpZone;

UCLASS()
class AGalaxyProjector : public AActor
{
	GENERATED_BODY()
public:
	AGalaxyProjector();
	virtual ~AGalaxyProjector() override;
	void Init(AOptigameCore* OptigameCorePtr);

	void SetProjectStar(FEnttHandle Star);
	void SetProjectGalaxy();
	void ClearProjection();
	void Update();

protected:
	void UpdateSystem();
	void UpdateSystemMeshes();
	void UpdateSystemPath();

	void UpdateGalaxy();
	void UpdateGalaxyMeshes();
	void UpdateGalaxyPath();

	APrStar*	 CreatePrStarActor(FEnttHandle Star);
	APrSun*		 CreatePrSunActor(FEnttHandle Star);
	APrPlanet*	 CreatePrPlanetActor(FEnttHandle Planet);
	APrStation*	 CreatePrStationActor(FEnttHandle Station);
	APrWarpZone* CreatePrWarpZoneActor(FEnttHandle WarpZone);

	void CreatePrStar(FEnttHandle Star);
	void CreatePrSun(FEnttHandle Star);
	void CreatePrPlanet(FEnttHandle Planet);
	void CreatePrWarpZone(FEnttHandle Planet);

	void		AddMeshRenderComp(FName MeshName);
	static void UpdRenderComp(UInstancedStaticMeshComponent* Comp, TArray<FTransform> const& Transforms);

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Render")
	TMap<FName, UInstancedStaticMeshComponent*> ComponentsMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Render")
	FEnttHandle CurrentViewStar;

	UPROPERTY(EditAnywhere, Category = "Templates Actors")
	TSubclassOf<AActor> TemplateYellowSun;
	UPROPERTY(EditAnywhere, Category = "Templates Actors")
	TSubclassOf<AActor> TemplateWarpZone;

	UPROPERTY(EditAnywhere, Category = "Projections Bp")
	TSubclassOf<APrStar> PrStarBp;

	UPROPERTY(EditAnywhere, Category = "Projections Bp")
	TSubclassOf<APrSun> PrSunBp;

	UPROPERTY(EditAnywhere, Category = "Projections Bp")
	TSubclassOf<APrPlanet> PrPlanetBp;

	UPROPERTY(EditAnywhere, Category = "Projections Bp")
	TSubclassOf<APrStation> PrStationBp;

	UPROPERTY(EditAnywhere, Category = "Projections Bp")
	TSubclassOf<APrWarpZone> PrWarpZoneBp;

protected:
	UPROPERTY()
	AOptigameCore*		OptigameCore = nullptr;
};