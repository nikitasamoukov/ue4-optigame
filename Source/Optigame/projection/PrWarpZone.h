// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PrWarpZone.generated.h"

UCLASS()
class OPTIGAME_API APrWarpZone : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APrWarpZone();

protected:
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Internal

	void ProjSet(FEnttHandle Handle);

	UPROPERTY(VisibleAnywhere)
	USceneComponent* SceneComponent;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FEnttHandle EnttHandle;
};