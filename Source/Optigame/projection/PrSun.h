// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Optigame/galaxy/handles/FEnttStarHandle.h"
#include "PrSun.generated.h"

// Projection of Star when view system
UCLASS()
class OPTIGAME_API APrSun : public AActor
{
	GENERATED_BODY()

public:
	APrSun();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent)
	void ForceRenderUpdate();

	UFUNCTION(BlueprintCallable)
	double GetSize();

	// Internal

	void ProjSet(FEnttStarHandle Handle);

	UPROPERTY(VisibleAnywhere)
	USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere)
	FEnttStarHandle EnttHandle;
};