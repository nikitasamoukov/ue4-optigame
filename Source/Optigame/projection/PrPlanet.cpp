// Fill out your copyright notice in the Description page of Project Settings.

#include "PrPlanet.h"
#include "Optigame/Galaxy/Components/ComponentsBasic.h"
#include "Optigame/Galaxy/Components/ComponentsPlanet.h"
#include "DrawDebugHelpers.h"

// Sets default values
APrPlanet::APrPlanet()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void APrPlanet::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APrPlanet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector Pos = GetPosition();
	SetActorLocation(Pos);

	DrawDebugLine(GetWorld(), EnttHandle.get<FCompTransform>().PrevPos, EnttHandle.get<FCompTransform>().Pos, FColor(100, 255, 100), false, -1, 1);
}

double APrPlanet::GetSize()
{
	if (!EnttHandle.IsValid())
		return {};

	return EnttHandle.get<FCompPlanet>().Size;
}

EPlanetType APrPlanet::GetType()
{
	return EnttHandle.GetType();
}

void APrPlanet::ProjSet(FEnttPlanetHandle Handle)
{
	EnttHandle = Handle;
}

FVector APrPlanet::GetPosition()
{
	return EnttHandle.GetPosition();
}