// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Optigame/Galaxy/Handles/FEnttStationHandle.h"
#include "PrStation.generated.h"

UCLASS()
class OPTIGAME_API APrStation : public AActor
{
	GENERATED_BODY()

public:
	APrStation();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	// Internal

	void ProjSet(FEnttStationHandle Handle);

	void Update();

protected:
	FName CurrentHullMeshId;

	void UpdateMesh();
	void HideMesh();
	void ShowMesh();

public:
	UPROPERTY(VisibleAnywhere)
	USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* MeshComponent;
	UPROPERTY(VisibleAnywhere)
	FEnttStationHandle EnttHandle;
};