#pragma once

#pragma warning(disable : 5054)
#pragma warning(disable : 5055)

#include "CoreMinimal.h"
#include "UObject/StrongObjectPtr.h"
#include "Engine/World.h"
#include "Misc/TVariant.h"

#define ENTT_NO_ETO 1
#include <entt/entt.hpp>
#include "Misc/FRegistry.h"

#include "Misc/LibExtend.h"
#include "Common/common.h"
#include "Common/Log.h"

#include "OptigameParameters.h"