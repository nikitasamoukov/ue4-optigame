#pragma once

#include "Engine/DataAsset.h"
#include "InstancedStruct.h"
#include "ObjectsDataAsset.generated.h"

USTRUCT(BlueprintType)
struct FObjectData
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Id;
	UPROPERTY(EditAnywhere)
	TArray<FInstancedStruct> Components;
};

UCLASS(BlueprintType)
class UObjectsDataAsset : public UDataAsset
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FObjectData> Objects;
};