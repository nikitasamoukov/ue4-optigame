// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class OptigameTarget : TargetRules
{
	public OptigameTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "Optigame" } );
        IncludeOrderVersion = EngineIncludeOrderVersion.Unreal5_2;
    }
}
