// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class OptigameEditorTarget : TargetRules
{
	public OptigameEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "Optigame" } );
		IncludeOrderVersion = EngineIncludeOrderVersion.Unreal5_2;
	}
}
